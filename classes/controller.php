<?php

class controller
{
	var $css;
	var $js;
	var $dta;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();
		//MongoSession::init();
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		
		if(!isset($_SESSION['userid']))
		{
			header( 'Location: '.SSO_URL ) ;
			exit;
		}
		
		/*$db = Db::init();
		$usr = $db->emails;
		$d = $usr->findone(array('email' => $_SESSION['email']));
		if(!isset($d['_id']))
		{
			header( 'Location: /error/err401' ) ;
			exit;
		}*/
	}
	
	protected function before()
	{
		
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
            
    protected function getListDevice($page=1,$limit=20,$url='')
	{
		$db = Db::init();
		$devTbl = $db->devices;

        	$skip = (int)($limit * ($page - 1));
		
		$count = $devTbl->count();

		$devList = $devTbl->find()->limit($limit)->skip($skip);
		$pg = new Pagination();
		$pg->pag_url = $url;
		$pg->calculate_pages($count, $limit, $page);
		$var = array(
			'deviceList' => $devList,
			'pagination' => $pg->Show()
		);
		return $var;
	}
	
	protected function redirect($page)
	{
		header( 'Location: '.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{
		//$css[] = "http://edge.telin.swiftserve.com/digitama/dboxid/flat/css/bootstrap.min.css";
		
		$css[] = "/public/css/styles.css";
		$css[] = "/public/css/jquery.tagit.css";
		$css[] = "/public/css/tagit.ui-zendesk.css";
		
		$js[] = "/public/js/vendors/jquery/jquery.min.js"; 
		$js[] = "/public/js/vendors/jquery/jquery-ui.min.js";
		$js[] = "/public/js/vendors/easing/jquery.easing.1.3.min.js"; 
		$js[] = "/public/js/vendors/easypie/jquery.easypiechart.min.js";
		$js[] = "/public/js/vendors/fullscreen/screenfull.min.js";
		$js[] = "/public/js/vendors/nanoscroller/jquery.nanoscroller.min.js";
		$js[] = "/public/js/vendors/sparkline/jquery.sparkline.min.js";
		$js[] = "/public/js/vendors/horisontal/cbpHorizontalSlideOutMenu.js"; 
		$js[] = "/public/js/vendors/classie/classie.js";
		$js[] = "/public/js/vendors/powerwidgets/powerwidgets.min.js";
		$js[] = "/public/js/vendors/raphael/raphael-min.js"; 
		$js[] = "/public/js/vendors/morris/morris.min.js";
		//$js[] = "/public/js/vendors/flotchart/jquery.flot.min.js"; 
		//$js[] = "/public/js/vendors/flotchart/jquery.flot.resize.min.js"; 
		//$js[] = "/public/js/vendors/flotchart/jquery.flot.axislabels.js";
		//$js[] = "/public/js/vendors/chartjs/chart.min.js";
		//$js[] = "/public/js/vendors/fullcalendar/fullcalendar.min.js"; 
		//$js[] = "/public/js/vendors/fullcalendar/gcal.js";
		$js[] = "/public/js/vendors/bootstrap/bootstrap.min.js";
		//$js[] = "/public/js/vendors/vector-map/jquery.vmap.min.js"; 
		//$js[] = "/public/js/vendors/vector-map/jquery.vmap.sampledata.js"; 
		//$js[] = "/public/js/vendors/vector-map/jquery.vmap.world.js";
		$js[] = "/public/js/vendors/todos/todos.js";
		$js[] = "/public/js/scripts.js";			
		
		
		$topmenu = $this->getView(DOCVIEW."template/topmenu.php", array('controller' => $group));
		$shorcut = $this->getView(DOCVIEW."template/shortcut.php", array());
		
		$a["shorcut"] = $shorcut;
		$var = array_merge($var, $a);
		//$var = array_merge($var, $this->dta);
		
		$content = $this->getView(DOCVIEW.$view, $var);
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);
		include(DOCVIEW."template/template.php");
	}
}
?>