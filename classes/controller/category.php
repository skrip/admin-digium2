<?php

class category_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function test()
	{
		echo session_id();
	}
	
	
	public function index()
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('name' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$db = Db::init();
		$cat = $db->categories;
		$c = $cat->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
		$count = $cat->count($quer);
		//echo $count.'</br>';
		//echo $skip.'</br>';
		//die;
		
		$pg = new Pagination();
		$pg -> pag_url = "/category/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "category/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$cat = $db->categories;
		
		$parent = '0';
		$menu = 'Tafaqquh';
		$nomor = '';
		$color = '#FFFFFF';
		$name = '';
		$description = '';
		$status = 'active';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$name = $_POST['name'];
			$parent = $_POST['parent'];
			$menu = $_POST['menu'];
			$color = $_POST['color'];
			$nomor = $_POST['nomor'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
               		'parent' => $parent,
               		'menu' => $menu,
               		'color' => $color,
               		'nomor' => $nomor,
					'description' => $description,
					'name' => $name,
					'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'menu' => $menu,
					'color' => $color,
					'nomor' => intval($nomor),
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				$cat->insert($var);
				$this->redirect('/category/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'menu' => $menu,
			'nomor' => $nomor,
			'color' => $color,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/category/add'
		);
		$this->render("", "category/add.php", $var);
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->categories;
		$dcat = $cat->findone(array('_id' => new MongoId($id)));
		
		$parent = $dcat['parent'];
		$menu = $dcat['menu'];
		$color = '#FFFFFF';
		if (isset($dcat['color']))
			$color = $dcat['color'];
		$nomor = $dcat['nomor'];
		$status = 'active';
		if(isset($dcat['status']))
			$status = $dcat['status'];
		$name = $dcat['name'];
		$description = $dcat['description'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$parent = $_POST['parent'];
			$menu = $_POST['menu'];
			$nomor = $_POST['nomor'];
			$color = $_POST['color'];
			$name = $_POST['name'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
				  'description' => $description,
				  'parent' => $parent,
				  'menu' => $menu,
				  'color' => $color,
				  'nomor' => $nomor,
				  'status' => $status,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'menu' => $menu,
					'color' => $color,
					'nomor' => intval($nomor),
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				$cat->update(array("_id" => new MongoId($id)), array('$set' => $var));
				$this->redirect('/category/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'menu' => $menu,
			'nomor' => $nomor,
			'color' => $color,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/category/edit?id='.$id
		);
		$this->render("", "category/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->categories;
		
		$cat->remove(array('_id' => new MongoId($id)));
		$this->redirect('/category/index');
		exit;
	}
}