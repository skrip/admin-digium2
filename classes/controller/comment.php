<?php

class comment_controller extends controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() 
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('title' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$db = Db::init();
		$comment = $db->comments;
		$mcomment = $comment->find($quer)->limit($docs_per_page)-> skip($skip)-> sort(array("title" => -1));
		$count = $comment->count($quer);
		
		$pg = new Pagination();
		$pg -> pag_url = "/comment/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $mcomment,
			'pagination' => $pg->Show(),
			'search' => $search,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "comment/index.php", $var);
	}
	
	public function delete()
	{
		$db = Db::init();
		$dbcomment = $db->comments;
		$dbcomment_reply = $db->comment_replies;
			
		if (isset($_POST['delcomment'])){
			foreach($_POST['delcomment'] as $k){
				$commentid = $k;
				$comment->remove(array('_id' => new MongoId($commentid) ));
			}
			
		}
		if (isset($_GET)){
			$id = $_GET['id'];
			$reply = "";
			if (isset($_GET['reply']))
				$reply = $_GET['reply'];
			
			$comment = "";
			if (isset($_GET['comment']))
				$comment = $_GET['comment'];
			
			if ($comment=="yes")
				$dbcomment->remove(array('_id' => new MongoId($id) ));
				
			if ($reply=="yes")
				$dbcomment_reply->remove(array('_id' => new MongoId($id) ));
			
		}
		$this->redirect('/comment/index');
		exit;
	}
}
