<?php

class slideshow_controller extends controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$db = Db::init();
		$cat = $db->slide_shows;
		$c = $cat->find();
		
		$var = array(
			'data' => $c
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "slide/index.php", $var);
	}
	
	public function delete()
	{
		$id = $_GET['id'];
		
		$db = Db::init();		
		$slide = $db->slide_shows;
		$col = $slide->findOne(array('_id' => new MongoId($id)));
		
		$var = array(
			'slide_show' => 'not'
		);
		
		$slide->remove(array('_id' => new MongoId($id)));
		
		$content = $db->contents;
		$content->update(array('_id' => new MongoId($col['content_id'])), array('$set' => $var));
		
		$this->redirect('/slideshow/index');
		exit;
	}
}
