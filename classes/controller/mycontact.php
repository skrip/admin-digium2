<?php

class mycontact_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('name' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$dboxes = DBDeboxs::init();
		$em = $dboxes->user_contacts;
		$c = $em->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
		$count = $em->count($quer);
		//echo $count.'</br>';
		//echo $skip.'</br>';
		//die;
		
		$pg = new Pagination();
		$pg -> pag_url = "/mycontact/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "mycontact/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$cat = $db->categories;
		
		$parent = '0';
		$name = '';
		$description = '';
		$status = 'active';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$name = $_POST['name'];
			$parent = $_POST['parent'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
               		'parent' => $parent,
					'description' => $description,
					'name' => $name,
					'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				$cat->insert($var);
				$this->redirect('/category/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/category/add'
		);
		$this->render("", "category/add.php", $var);
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->categories;
		$dcat = $cat->findone(array('_id' => new MongoId($id)));
		
		$parent = $dcat['parent'];
		$status = 'active';
		if(isset($dcat['status']))
			$status = $dcat['status'];
		$name = $dcat['name'];
		$description = $dcat['description'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$parent = $_POST['parent'];
			$name = $_POST['name'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
				  'description' => $description,
				  'parent' => $parent,
				  'status' => $status,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				$cat->update(array("_id" => new MongoId($id)), array('$set' => $var));
				$this->redirect('/category/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/category/edit?id='.$id
		);
		$this->render("", "category/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->categories;
		
		$cat->remove(array('_id' => new MongoId($id)));
		$this->redirect('/category/index');
		exit;
	}
}