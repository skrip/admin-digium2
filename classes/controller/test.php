<?php

class test_controller extends controller 
{
	public function index() {
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		
		$mcat = $cat -> find(array('status' => 'not'));
		
		//---------------------terbaru----------------//
		
		$quer = array();
		$query = array();
		foreach ($mcat as $key) {
			$q = array('category.category_id' => array('$ne' => trim($key['_id'])));
			$quer[] = $q;
		}

		$query = array('$and' => $quer);

		$mcontent = $cont -> find(array('$and' => array( array('publish' => 'yes'), $query))) -> limit(10) -> sort(array("time_created" => -1));
		//------------------------------ End Terbaru ---------------//
		
		
		//------------------------------ Terpopuler ---------------//
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array('category.category_id' => array('$ne' => trim($keypop['_id'])));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpop = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop))) -> limit(10) -> sort(array("count" => -1));
		
		
		//------------------------------ End Terpopuler ---------------//
		
		
		//-------------------------------- Di Komentari ---------------- //
		$querkom = array();
		$querykom = array();
		
		foreach ($mcat as $keykom) {
			$qkom = array('category.category_id' => array('$ne' => trim($keykom['_id'])));
			$querkom[] = $qkom;
		}
		$querykom = array('$and' => $querkom);

		$mcontentkom = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querykom)));
			
			
			
		//-------------------------------- End Di Komentari -------------//
		
		
		//------------------------------ USER Baru ----------------------//
			$userbaru = $db->users_digiumm;
			$muserbaru = $userbaru -> find() -> limit(10) -> sort(array("time_created" => -1));
		
		//------------------------------ END USer Baru ------------------//
		
		$var = array(
			'userbaru' => $muserbaru,
			'komentar' => $mcontentkom,
			'terbaru' => $mcontent,
			'terpopuler' => $mcontentpop
		);
		$this->render("test", "test/index.php", $var);
	}
}
