<?php

class forumcategory_controller extends controller
{
	public function __construct() {
		parent::__construct();
	}
	
	public function index() 
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('title' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$katserver = new kategoriserver();
		$data = $katserver->getAll($skip, $limit);
		
		//$db = Db::init();
		//$comment = $db->comments;
		//$mcomment = $comment->find($quer)->limit($docs_per_page)-> skip($skip)-> sort(array("title" => -1));
		//$count = $comment->count($quer);
		$count = $katserver->getCount();
		
		$pg = new Pagination();
		$pg -> pag_url = "/comment/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $data,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "forumcategory/index.php", $var);
	}

	public function add()
	{
		$parent = '0';
		$name = '';
		$description = '';
		$status = 'active';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'enable';
			$name = $_POST['name'];
			$parent = $_POST['parent'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
               		'parent' => $parent,
					'description' => $description,
					'name' => $name,
					'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				
				
				$katserver = new kategoriserver();
				$katserver->insert($var);
				$this->redirect('/forumcategory/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/forumcategory/add'
		);
		$this->render("", "forumcategory/add.php", $var);
	}

	public function edit()
	{
		$id = $_GET['id'];
		$katserver = new kategoriserver();
		$dcat = $katserver->findone($id);
		
		$parent = $dcat['parent'];
		$status = 'active';
		if(isset($dcat['status']))
		{
			if($dcat['status'] != '1')
				$status = 'no';
		}
		$name = $dcat['name'];
		$description = $dcat['description'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$parent = $_POST['parent'];
			$name = $_POST['name'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('parent', array('require'));
			
			$validator->setData(
                array(
				  'description' => $description,
				  'parent' => $parent,
				  'status' => $status,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'parent' => $parent,
					'name' => $name,
					'status' => $status,
					'description' => $description,
				);
				
				$katserver->update($id, $var);
				$this->redirect('/forumcategory/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'parent' => $parent,
			'status' => $status,
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/forumcategory/edit?id='.$id
		);
		$this->render("", "forumcategory/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$katserver->delete($id);
		$this->redirect('/forumcategory/index');
		exit;
	}
}
