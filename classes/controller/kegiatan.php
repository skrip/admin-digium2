<?php
	class kegiatan_controller extends controller
	{
		public function __construct()
		{
			parent::__construct();
		}
		
		public function index()
		{
			$page = "";
			if (isset($_GET['page'])) 
				$page = $_GET['page'];
			
			$search = "";
			
			if(isset($_POST['search']))
				$search = $_POST['search'];
			
			$quer = array();
			if(strlen(trim($search)) > 0)
			{
				$regex = new MongoRegex("/".$search."/i"); 
				$quer = array('title' => $regex);
			}
			
			if(strlen(trim($page)) > 0)
				$page = intval($page);
			else
				$page = 1;
			
			$docs_per_page = 10;
			$skip = (int)($docs_per_page * ($page - 1));
			
			$db = Db::init();
			$kegiatan = $db->kegiatan;
			$k = $kegiatan->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created"  => -1));
			$count = $kegiatan->count($quer);
			
			$pg = new Pagination();
			$pg -> pag_url = "/kegiatan/index?page=";
			$pg -> calculate_pages($count, $docs_per_page, $page);
			
			$var = array(
				'data' => $k,
				'pagination' => $pg -> Show(),
				'search' => $search,
				'idx' => (($page-1)*$docs_per_page)+1
			);
			
			$this->js[] = "/public/js/delete.js";
			$this->js[] = "/public/js/vendors/animation/animation.js";
			$this->render("", "kegiatan/index.php", $var);
			
		}
		
		public function add()
		{
			$db = Db::init();
			$kegiatan = $db->kegiatan;
			
			$title = '';
			$date_start = '';
			$date_finish = '';
			$time_start = '00:00:00';
			$time_finish = '00:00:00';
			$description = '';
			$kuota = '';
			$tag = array();
			$category = array();
			$publish = "yes";
			$daftar = 'yes';
			$slide = 'not';
			
			$fileimage = '';
			$error = array();
			$info = array();
			
			if (!empty($_POST)) {
				$title = $_POST['title'];
				$date_start = strtotime($_POST['date_start']);
				$date_finish = strtotime($_POST['date_finish']);
				$time_start = strtotime($_POST['time_start']);
				$time_finish =  strtotime($_POST['time_finish']);
				$description = $_POST['description'];
				$kuota = $_POST['kuota'];
				
				$validator = new Validator();
				$validator->addRule('title', array('require', 'minlength' => 3));
				$validator->addRule('description', array('require', 'minlength' => 3));
				
				$seo = helper::string_to_underscore_name($title);
				
				$tag = $_POST['tag'];
				$dtag = $db->tags;
				$tagcat = array();
				$ex = explode(',', $tag);										
				for($x=0; $x<count($ex); $x++)
				{
					$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
					if ($mcat){
						$acat = array('tag_id' => new MongoId($mcat['_id']));
						$tagcat[] = $acat;
					}else{
						$var = array(
							'name' => trim($ex[$x])
						);
						$dtag->insert($var);
						$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
						$acat = array('tag_id' => new MongoId($mcat['_id']));
						$tagcat[] = $acat;
					}
				}
				$cat = $db->categories;
					$category = array();
					foreach ($_POST['category'] as $dct) {
						$col = $cat->findOne(array('_id' => new MongoId($dct)));
						$seo .= helper::string_to_underscore_name($col['name']);
						
						$c = array('category_id' => $dct);
						$category[] = $c;
					}
					
					if (!isset($_POST['publish'])) 
						$publish = 'not';
					if (!isset($_POST['daftar']))
						$daftar = 'not';
					if (isset($_POST['slide'])) 
						$slide = 'yes';	
						
					$validator->setData(
		                array(
						  'description' => $description,
						  'title' => $title,				  
		            	)
					);
					
					if ($validator->isValid()) {
						$namafileimage = '';
						$namafileshaimage = '';
						
						if (isset($_FILES['fileimage']['name'])) {
							$namafileimage = $_FILES['fileimage']['name'];
							if (strlen(trim($namafileimage)) > 0) {
								$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
							}
						}
					$var = array(
						'title' => $title,
						'date_start' => $date_start,
						'date_finish' => $date_finish,
						'time_start' => $time_start,
						'time_finish' => $time_finish,
						'description' => $description,
						'kuota' => intval($kuota),
						'daftar' => $daftar,
						'publish' => $publish,
						'tag' => $tagcat,
	  				    'slide_show' => $slide,
					    'category' => $category,
					    'seo' => $seo,
						'image' => $namafileshaimage,
						'time_created' => time(),
						'created_by' => $_SESSION['userid']
					);	
					$kegiatan->insert($var);
					if (strlen(trim($namafileshaimage)) > 0) {
						$ftp = new upload();
						$ftp -> upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
					}
					
					if (trim($slide) == 'yes') {
						$col = $kegiatan->findOne(array('title' => trim($title)));
						$sld = $db->slide_shows;
						$vvar = array(
							'content_id' => trim($col['_id']),
							'title' => $title,
							'description' => $description,
						 	'image' => $namafileshaimage,
						 	'time_created' => time()
						);
						$sld->insert($vvar);
					}
						$this->redirect('/kegiatan/index');
						exit;
					}
					else {
						$error = $validator->getErrors();
					}
			}
			
			$dtag = $db->tags;
			$mtag = $dtag->find();	
			$var = array(
						'title' => $title,
						'date_start' => $date_start,
						'date_finish' => $date_finish,
						'description' => $description,
						'time_start' => '00:00:00',
						'time_finish' => '00:00:00',
						'kuota' => $kuota,
						'daftar' => $daftar,
						'publish' => $publish,
						'tag' => $tag,
	  				    'slide' => $slide,
					    'category' => $category,
						'fileimage' => $fileimage,
						'error' => $error,
						'info' => $info,
						'atag' => $mtag,
						'link' => '/kegiatan/add'
			);
					
			$this->js[] = "/public/js/tag-it.min.js";		
			$this->js[] = "/public/js/tinymce/tinymce.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
			$this->js[] = "/public/js/controller/content.js";
			$this->render("", "kegiatan/add.php", $var);
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$kegiatan = $db->kegiatan;
		$mkat = $kegiatan->findOne(array('_id' => new MongoId($id)));
		
		$title = $mkat['title'];
		$date_start = date("d.m.Y", $mkat['date_start']);
		$date_finish = date("d.m.Y", $mkat['date_finish']);
		
		$time_start = date("H:i:s", $mkat['time_start']);
		$time_finish = date("H:i:s", $mkat['time_finish']);
		
		$kuota = $mkat['kuota'];
		$description = $mkat['description'];
		$tag = $mkat['tag'];
		$category = $mkat['category'];
		$publish = $mkat['publish'];
		$daftar = $mkat['daftar'];
		$slide = $mkat['slide_show'];
		$fileimage = $mkat['image'];
		$time_created = $mkat['time_created'];
		$error = array();
		$info = array();
		
		if (!empty($_POST)) {
			$title = $_POST['title'];
				$date_start = strtotime($_POST['date_start']);
				$date_finish = strtotime($_POST['date_finish']);
				$time_start = strtotime($_POST['time_start']);
				$time_finish =  strtotime($_POST['time_finish']);
				$description = $_POST['description'];
				$kuota = $_POST['kuota'];
				
				$validator = new Validator();
				$validator->addRule('title', array('require', 'minlength' => 3));
				$validator->addRule('description', array('require', 'minlength' => 3));
				
				$seo = helper::string_to_underscore_name($title);
				
				$tag = $_POST['tag'];
				$dtag = $db->tags;
				$tagcat = array();
				$ex = explode(',', $tag);										
				for($x=0; $x<count($ex); $x++)
				{
					$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
					if ($mcat){
						$acat = array('tag_id' => new MongoId($mcat['_id']));
						$tagcat[] = $acat;
					}else{
						$var = array(
							'name' => trim($ex[$x])
						);
						$dtag->insert($var);
						$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
						$acat = array('tag_id' => new MongoId($mcat['_id']));
						$tagcat[] = $acat;
					}
				}
				$cat = $db->categories;
					$category = array();
					foreach ($_POST['category'] as $dct) {
						$col = $cat->findOne(array('_id' => new MongoId($dct)));
						$seo .= helper::string_to_underscore_name($col['name']);
						
						$c = array('category_id' => $dct);
						$category[] = $c;
					}
				
				if(isset($_POST['daftar'])){
				$daftar = 'yes';
				} elseif (!isset($_POST['daftar'])) {
					$daftar = 'not';
				}
					
				if(isset($_POST['publish'])){
				$publish = 'yes';
				} elseif (!isset($_POST['publish'])) {
					$publish = 'not';
				}
				
				if(isset($_POST['slide'])){
					$slide = 'yes';
				} elseif (!isset($_POST['slide'])) {
					$slide = 'not';
				}
				
				$validator->setData(
					 array(
					  'description' => $description,
					  'title' => $title,				  
	            	)
				);
				
				if ($validator->isValid()) {
					$namafileimage = '';
					$namafileshaimage = $fileimage;
					if(isset($_FILES['fileimage']['name']))
					{
						$namafileimage = $_FILES['fileimage']['name'];
						if(strlen(trim($namafileimage)) > 0)
						{
							$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
						}
					}
					
					$var = array(
						'title' => $title,
						'date_start' => $date_start,
						'date_finish' => $date_finish,
						'time_start' => $time_start,
						'time_finish' => $time_finish,
						'description' => $description,
						'kuota' => intval($kuota),
						'daftar' => $daftar,
						'publish' => $publish,
						'tag' => $tagcat,
	  				    'slide_show' => $slide,
					    'category' => $category,
					    'seo' => $seo,
						'image' => $namafileshaimage,
						'time_updated' => time(),
						'updated_by' => $_SESSION['userid']
					);
					
					$kegiatan->update(array('_id' => new MongoId($id)), array('$set' => $var));
					
					if(strlen(trim($namafileshaimage)) > 0)
					{					
						if(trim($fileimage) != trim($namafileshaimage))
						{
							$ftp = new upload();
							$ftp->delete($fileimage, 'image_content');					
							$ftp->upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
						}						
					}
					
					if (isset($_POST['slide'])) {
						$sld = $db->slide_shows;
						$msld = $sld->findOne(array('content_id' => trim($id)));
						
						
							$vvar = array(
							'title' => $title,
							'description' => $description,
						 	'image' => $namafileshaimage,
						 	'publish' => $publish,
						 	'time_created' => $time_created,
						 	'time_updated' => time()
						);
						$sld->update(array('content_id' => trim($id)), array('$set' => $vvar));
						
					}
					
					$this->redirect('/kegiatan/index');
					exit;
				}
				else {
					$error = $validator->getErrors();
				}	
		}
			$dtag = $db->tags;
			$mtag = $dtag->find();
			
			$var = array(
						'title' => $title,
						'date_start' => $date_start,
						'date_finish' => $date_finish,
						'description' => $description,
						'time_start' => $time_start,
						'time_finish' => $time_finish,
						'kuota' => $kuota,
						'daftar' => $daftar,
						'publish' => $publish,
						'tag' => $tag,
	  				    'slide' => $slide,
					    'category' => $category,
						'fileimage' => $fileimage,
						'error' => $error,
						'info' => $info,
						'atag' => $mtag,
						'link' => '/kegiatan/edit?id='.$id
			);
			
			$this->js[] = "/public/js/tag-it.min.js";		
			$this->js[] = "/public/js/tinymce/tinymce.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
			$this->js[] = "/public/js/controller/content.js";
			$this->render("", "kegiatan/add.php", $var);
	}
	
	
	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$kegiatan = $db->kegiatan;
		$mkat = $kegiatan->findOne(array('_id' => new MongoId($id)));
		$slide = $db->slide_shows;
		
		if(isset($mkat['image']))
		{
			if(strlen(trim($mkat['image'])) > 0)
			{
				$ftp = new upload();
				$ftp->delete($mkat['image'], 'image_content');
			}			
		}
		
		$kegiatan->remove(array('_id' => new MongoId($id)));
		$slide->remove(array('content_id' => $id));
		$this->redirect('/kegiatan/index');
		exit;
	}
		
	private function findexts ($filename) { 
		$path_parts = pathinfo($filename);

		return $path_parts['extension'];
	}	
		
	}
?>