<?php

class contact_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('title' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$db = Db::init();
		$cont = $db->contacts;
		$c = $cont->find($quer)->limit($docs_per_page)-> skip($skip)-> sort(array("title" => -1));
		$count = $cont->count($quer);
		
		$pg = new Pagination();
		$pg -> pag_url = "/contact/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "contact/index.php", $var);
	}
	
	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cont = $db->contacts;
		
		$cont->remove(array('_id' => new MongoId($id)));
		$this->redirect('/contact/index');
		exit;
	}
}