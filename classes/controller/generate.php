<?php

class generate_controller {
	
	public function content() {
		$url = "http://deboxs.com:8055/generate";
		
		$db = Db::init();
		$content = $db->contents;
		$c = $content->find(array('publish' => 'yes'));
		
		$data = array();
		foreach($c as $pp)
		{
			if(strlen($pp['description']) > 500)
				$tmp = helper::limitString($pp['description'], 500);
			else {
				$tmp = $pp['description'];
			}

			$p = array(
				'title' => trim($pp['title']),
				'seo' => trim($pp['seo']),
				'sid' => trim($pp['_id'])
			);
			$data[] = $p;
		}
		
		$field = array();
		$p = array("name" => 'title', 'type' => 2);
		$field[] = $p;
		$p = array("name" => 'seo', 'type' => 2);
		$field[] = $p;
		$p = array("name" => 'sid', 'type' => 2);
		$field[] = $p;
		//$p = array("name" => 'description', 'type' => 2);
		//$field[] = $p;
				
		$p = array(
			'name' => 'digiumm_content',
			'field' => $field,
			'data' => $data,
			'clear' => 1
		);
		
		$curl = new Curl();
		//$curl->setOpt(CURLOPT_RETURNTRANSFER, 1);
		//$curl->setOpt(CURLOPT_POST, 1);
		//$curl->setOpt(CURLOPT_VERBOSE, true);
		$curl->setHeader('Content-Type', 'application/json');
		$curl->post($url, json_encode($p));
		
		if ($curl->error) {
			var_dump($curl->response_headers);
		    echo 'Error: ' . $curl->error_code . ': ' . $curl->error_message;
		}
		else {
			$array = json_decode(json_encode($curl->response), true);
		    print_r( $array );
		}
	}

	public function search() {
		$search = $_GET['search'];
		$field = $_GET['field'];
		$url = "http://deboxs.com:8055/search";
		
		//$src =  "title: ".$search." or description:".$search.' or type:'.$search.' or artist:'.$search;
		$p = array(
			'name' => 'digiumm_content',
			'fieldname' => $field,
			'search' => $search,
			'skip' => 0,
			'limit' => 10
		);
			
		$curl = new Curl();
		$curl->setHeader('Content-Type', 'application/json');
		//echo json_encode($p)."<br />";
		$curl->post($url, json_encode($p));
		
		if ($curl->error) {
		    echo 'Error: ' . $curl->error_code . ': ' . $curl->error_message;
		}
		else {
		   $array = json_decode(json_encode($curl->response), true);
		    print_r( $array );
		}
	}
}