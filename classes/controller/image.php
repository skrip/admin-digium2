<?php

class image_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$db = Db::init();
		$cat = $db->images;
		$c = $cat->find();
		
		$var = array(
			'data' => $c
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "image/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$content = $db->images;
		
		$title = '';
		$fileimage = '';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$title = $_POST['title'];
			$validator = new Validator();
            $validator->addRule('title', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'title' => $title
            	)
			);
                    
            if ($validator->isValid())
			{
				$namafileimage = '';
				$namafileshaimage = '';
				if(isset($_FILES['fileimage']['name']))
				{
					$namafileimage = $_FILES['fileimage']['name'];
					if(strlen(trim($namafileimage)) > 0)
					{
						$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
					}
				}

				$var = array(
					'title' => $title,
					'image' => $namafileshaimage,
					'time_created' => time(),
					'created_by' => $_SESSION['userid']
				);
				$content->insert($var);
				if(strlen(trim($namafileshaimage)) > 0)
				{
					$ftp = new upload();
					$ftp->upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
				}
				
				$this->redirect('/image/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'title' => $title,
			'fileimage' => $fileimage,
			'error' => $error,
			'info' => $info,
			'link' => '/image/add'
		);
		
		$this->render("", "image/add.php", $var);
	}

	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db->images;
		$mcon = $content->findOne(array('_id' => new MongoId($id)));
		
		$title = $mcon['title'];
		$fileimage = $mcon['image'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$title = $_POST['title'];
			
			$validator = new Validator();
            $validator->addRule('title', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'title' => $title,				  
            	)
			);
                    
            if ($validator->isValid())
			{
				$namafileimage = '';
				$namafileshaimage = $fileimage;
				if(isset($_FILES['fileimage']['name']))
				{
					$namafileimage = $_FILES['fileimage']['name'];
					if(strlen(trim($namafileimage)) > 0)
					{
						$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
					}
				}

				$var = array(
					'title' => $title,
					'image' => $namafileshaimage,
					'time_created' => time(),
				);
				$content->update(array('_id' => new MongoId($id)), array('$set' => $var));
								
				if(strlen(trim($namafileshaimage)) > 0)
				{					
					if(trim($fileimage) != trim($namafileshaimage))
					{
						$ftp = new upload();
						$ftp->delete($fileimage, 'image_content');					
						$ftp->upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
					}						
				}
				
				$this->redirect('/image/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'title' => $title,
			'fileimage' => $fileimage,
			'error' => $error,
			'info' => $info,
			'link' => '/image/edit?id='.$id
		);
		
		$this->render("", "image/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db->images;
		$mcon = $content->findOne(array('_id' => new MongoId($id)));
		
		if(isset($mcon['image']))
		{
			if(strlen(trim($mcon['image'])) > 0)
			{
				$ftp = new upload();
				$ftp->delete($mcon['image'], 'image_content');
			}			
		}
		
		$content->remove(array('_id' => new MongoId($id)));
		$this->redirect('/image/index');
		exit;
	}

	private function findexts ($filename) { 
		$path_parts = pathinfo($filename);

		return $path_parts['extension'];
	}
}