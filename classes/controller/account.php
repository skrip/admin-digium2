<?php

class account_controller
{
	
	public function returnLogin()
	{
		$sid = $_GET['sid'];
		$userid = $_GET['userid'];
		$cookie = $_GET['cookie'];
		$name = $_GET['name'];
		$email = $_GET['email'];
		
		$usr = $this->verifikasi($email);
		if(!$usr)
		{
			header( 'Location: /error/err401' ) ;
			exit;
			return;
		}
			
		session_regenerate_id(true);
		$_SESSION['name'] = $name;
		$_SESSION['email'] = $email;
		$_SESSION['cookie'] = $cookie;
		$_SESSION['userid'] = trim($usr['_id']);
		$_SESSION['sid'] = $sid;
	}
	
	private function verifikasi($email)
	{
		$db = Db::init();
		$usr = $db->users;
		$role = $db->roles;
		$d = $usr->findone(array('email' => $email));
		if(isset($d['_id']))
		{
			$c = $role->findone(array('_id' => new MongoId($d['role'])));
			if(isset($c['_id']))
			{
				if(trim($c['_id']) == '53cc57a77e3af488848b456a') // admin
					return $d;
			}
		}
		return false;
	}
	
	public function logintwitter()
	{
		$type = $_GET['type'];
		if($type == 'twitter')
		{
			$twitterid = $_GET['sid'];
			$userid = $_GET['userid'];
			$cookie = $_GET['cookie'];
			$name = $_GET['name'];
			$email = $_GET['email'];
			
			$usr = $this->verifikasi($email);
			if(!$usr)
			{
				header( 'Location: /error/err401' ) ;
				exit;
				return;
			}
			
			session_regenerate_id(true);
			$_SESSION['name'] = $name;
			$_SESSION['email'] = $email;
			$_SESSION['cookie'] = $cookie;
			$_SESSION['userid'] = trim($usr['_id']);
			$_SESSION['sid'] = $twitterid;
			$_SESSION['type'] = $type;
			
			echo '<html><meta http-equiv="refresh" content="0; url=/"></html>';
			//header( 'Location: /' ) ;
		}
	}
	
	public function logingoogle()
	{
		$type = $_GET['type'];
		if($type == 'google')
		{
			$googleid = $_GET['sid'];
			$userid = $_GET['userid'];
			$cookie = $_GET['cookie'];
			$name = $_GET['name'];
			$email = $_GET['email'];
			
			$usr = $this->verifikasi($email);
			if(!$usr)
			{
				header( 'Location: /error/err401' ) ;
				exit;
				return;
			}
			
			session_regenerate_id(true);
			$_SESSION['name'] = $name;
			$_SESSION['email'] = $email;
			$_SESSION['cookie'] = $cookie;
			$_SESSION['userid'] = trim($usr['_id']);
			$_SESSION['sid'] = $googleid;
			$_SESSION['type'] = $type;
			
			//header( 'Location: /' ) ;
			echo '<html><meta http-equiv="refresh" content="0; url=/"></html>';
		}
	}

	public function loginfb()
	{
		$type = $_GET['type'];
		if($type == 'facebook')
		{
			$facebookid = $_GET['sid'];
			$userid = $_GET['userid'];
			$cookie = $_GET['cookie'];
			$name = $_GET['name'];
			$email = $_GET['email'];
			
			$usr = $this->verifikasi($email);
			if(!$usr)
			{
				header( 'Location: /error/err401' ) ;
				exit;
				return;
			}
			
			session_regenerate_id(true);
			$_SESSION['name'] = $name;
			$_SESSION['email'] = $email;
			$_SESSION['cookie'] = $cookie;
			$_SESSION['userid'] = trim($usr['_id']);
			$_SESSION['sid'] = $facebookid;
			$_SESSION['type'] = $type;
			
			//echo "SESSION ID ".session_id();
			//header( 'Location: /' ) ;
			//echo session_id();
			//print_r($_SESSION);
			//die;
			echo '<html><meta http-equiv="refresh" content="0; url=/"></html>';
		}
	}
	
	public function logout()
	{
		session_destroy ();
		
		header( 'Location: /' ) ;
		exit();
	}
	
	public function test() {
		$db = Db::init();
		$con = $db->contents;
		$col = $con->find();
		
		foreach($col as $dat) {
			foreach($dat['tag'] as $det) {
				helper::addTagContent($det['tag_id'], $dat['_id']);
			}
		}
	}
}
