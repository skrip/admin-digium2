<?php

class forum_controller extends controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() 
	{
		$var = array(
			
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "forum/index2.php", $var);
	}
	
	public function detail() 
	{
		$forumcategory = "";
		if (isset($_GET['forumcategory']))
			$forumcategory = $_GET['forumcategory'];
		
		if(strlen($forumcategory) == 0)
			return;
		
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array('forumcategory' => $forumcategory);
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array(
				'title' => $regex,
				'forumcategory' => $forumcategory
			);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$db = Db::init();
		$forum = $db->forum_threads;
		$mcomment = $forum->find($quer)->limit($docs_per_page)-> skip($skip)-> sort(array("title" => -1));
		$count = $forum->count($quer);
		
		$pg = new Pagination();
		$pg -> pag_url = "/forum/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $mcomment,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "forum/index.php", $var);
	}
	public function add()
	{
		$db = Db::init();
		$cat = $db->forum_threads;
		
		$forumcategory = '0';
		$title = '';
		$status = 'active';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$title = $_POST['title'];
			$forumcategory = $_POST['forumcategory'];
			$validator = new Validator();
            $validator->addRule('title', array('require', 'minlength' => 3));
			$validator->addRule('forumcategory', array('require'));
			$validator->addRule('status', array('require'));
			
			$validator->setData(
                array(
               		'forumcategory' => $forumcategory,
					'title' => $title,
					'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'forumcategory' => $forumcategory,
					'title' => $title,
					'status' => $status,
					'description' => $description,
					'time_created' => time(),
					'created_by' => $_SESSION['userid']
				);
				$cat->insert($var);
				$this->redirect('/forum/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'forumcategory' => $forumcategory,
			'status' => $status,
			'title' => $title,
			'error' => $error,
			'info' => $info,
			'link' => '/forum/add'
		);
		$this->render("", "forum/add.php", $var);
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->forum_threads;
		$dcat = $cat->findone(array('_id' => new MongoId($id)));
		
		$forumcategory = $dcat['forumcategory'];
		$status = 'active';
		if(isset($dcat['status']))
			$status = $dcat['status'];
		$title = $dcat['title'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$forumcategory = $_POST['forumcategory'];
			$title = $_POST['title'];
			
			$validator = new Validator();
            $validator->addRule('title', array('require', 'minlength' => 3));
			$validator->addRule('forumcategory', array('require'));
			$validator->addRule('status', array('require'));
			
			$validator->setData(
                array(
				  'title' => $title,
				  'forumcategory' => $forumcategory,
				  'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'forumcategory' => $forumcategory,
					'title' => $title,
					'status' => $status,
				);
				$cat->update(array("_id" => new MongoId($id)), array('$set' => $var));
				$this->redirect('/forum/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'forumcategory' => $forumcategory,
			'status' => $status,
			'title' => $title,
			'error' => $error,
			'info' => $info,
			'link' => '/forum/edit?id='.$id
		);
		$this->render("", "forum/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->forum_threads;
		
		$cat->remove(array('_id' => new MongoId($id)));
		$this->redirect('/forum/index');
		exit;
	}
}
