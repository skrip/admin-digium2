<?php

class mainmenu_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$db = Db::init();
		$menu = $db->menus;
		$c = $menu->find();
		
		$var = array(
			'data' => $c
		);
		$this->render("", "mainmenu/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$menu = $db->menus;
		
		$status = 'active';
		$title = '';
		$link = '';
		$submenu = array();
		$sublink = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			
			$title = $_POST['title'];
			$link = $_POST['link'];

			$validator = new Validator();
            $validator->addRule('name', array('require'));
			
			$validator->setData(
                array(
               		'title' => $title,
               		'link' => $link,
            	)
			);
                    
            if ($validator->isValid())
			{
				if(isset($_POST['submenu']))
				{
					$no=0;
					foreach($_POST['submenu'] as $nm)
					{
						$name = array('no' => $no, 'name' => $nm);
						$submenu[] = $name;
						$no++;
					}						
				}
				
				if(isset($_POST['sublink']))
				{
					$no=0;
					foreach($_POST['sublink'] as $nm)
					{
						$name = array('no' => $no, 'name' => $nm);
						$sublink[] = $name;
						$no++;
					}						
				}
				
				$var = array(
					'title' => $title,
					'link' => $link,
					'submenu' => $submenu,
					'sublink' => $sublink,
					'status' => $status
				);
				$cat->insert($var);
				$this->redirect('/category/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		if(isset($_POST['submenu']))
			$submenu = $_POST['submenu'];
		else
			$submenu = array();
		
		if(isset($_POST['sublink']))
			$sublink = $_POST['sublink'];
		else
			$sublink = array();
			
		$var = array(
			'submenu' => $submenu,
			'sublink' => $sublink,
			'status' => $status,
			'title' => $title,
			'link' => $link,
			'error' => $error,
			'info' => $info,
			'action' => '/category/add'
		);
		$this->render("", "mainmenu/add.php", $var);
		
	}
}