<?php

class tag_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('name' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$db = Db::init();
		$cat = $db->tags;
		$c = $cat->find($quer)->limit($docs_per_page)-> skip($skip)-> sort(array("title" => -1));
		$count = $cat->count($quer);
		
		$pg = new Pagination();
		$pg -> pag_url = "/tag/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "tag/index.php", $var);
	}
	
	public function getTag()
	{
		if (!empty($_POST))
		{
			$db = Db::init();
			$tag = $db->tags;
			$mtag = $tag->find(array('name'=> array('$regex' => new MongoRegex("/".$_POST['search']."/i"))));
			
			$hasil = array();
			foreach($mtag as $dt)
			{
				$p = array(
					//"id" => trim($dt['_id']),
					"label" => trim($dt['name']),
					"value" => trim($dt['name']),
				);
				$hasil[] = $p;
			}
			$dta = array("data" => $hasil);
			echo json_encode($dta);
		}
	}
	
	public function add()
	{
		$db = Db::init();
		$cat = $db->tags;
		
		$name = '';
		$status = 'active';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$name = $_POST['name'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
					'name' => $name,
					'status' => $status,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'name' => $name,
					'status' => $status,
				);
				$cat->insert($var);
				$this->redirect('/tag/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'status' => $status,
			'name' => $name,
			'error' => $error,
			'info' => $info,
			'link' => '/tag/add'
		);
		$this->render("", "tag/add.php", $var);
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->tags;
		$dcat = $cat->findone(array('_id' => new MongoId($id)));
		
		$status = 'active';
		if(isset($dcat['status']))
			$status = $dcat['status'];
		$name = $dcat['name'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$status = 'not';
			if(isset($_POST['status']))
				$status = 'active';
			$name = $_POST['name'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'status' => $status,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'name' => $name,
					'status' => $status,
				);
				$cat->update(array("_id" => new MongoId($id)), array('$set' => $var));
				$this->redirect('/tag/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'status' => $status,
			'name' => $name,
			'error' => $error,
			'info' => $info,
			'link' => '/tag/edit?id='.$id
		);
		$this->render("", "tag/add.php", $var);
	}
	
	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$tag = $db->tags;
		
		$tag->remove(array('_id' => new MongoId($id)));
		$this->redirect('/tag/index');
		exit;
	}
	
}