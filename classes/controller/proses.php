<?php

//require_once DOCROOT.'lib/geoip2.phar';

//use GeoIp2\Database\Reader;

class proses_controller
{
	public function __construct()
	{
		
	}
	
	public function run()
	{		
		//require_once DOCROOT.'lib/geoip2.phar';

		//use GeoIp2\Database\Reader;
		
		//$reader = new GeoIp2\Database\Reader(DOCROOT.'lib/GeoIP2-City.mmdb');
		//die("run");
		$db = Db::init();
		$ct = $db->content_logs;
		$p = array (
  			'useragent' => 
  				array('$exists' => true)
		);
		$dbk = $ct->find($p);
		foreach($dbk as $dba)
		{
			$ua_info = parse_user_agent($dba['useragent']);
			$platform = $ua_info['platform'];
			$browser = $ua_info['browser'];
			$version = $ua_info['version'];
			
			$var = array(
				'platform' => $platform,
				'browser' => $browser,
				'version' => $version
			);
			$ct->update(array("_id" => new MongoId($dba['_id'])), array('$set' => $var));
		}
		
		$p = array (
  			'ipaddress' => 
  				array('$exists' => true)
		);
		$dbk = $ct->find($p);
		require_once DOCROOT.'lib/geoip2.phar';
		$reader = new GeoIp2\Database\Reader(DOCROOT.'lib/GeoIP2-City.mmdb');
		foreach($dbk as $dba)
		{
			$record = $reader->city($dba['ipaddress']);
			//print_r($record);
			//die;
			$isocountry = $record->country->isoCode;
			$continent = $record->continent->name;
			$country = $record->country->name;
			$subdivisi = $record->mostSpecificSubdivision->names['en'];
			$city = $record->city->name;
		
			$lat = $record->location->latitude;
			$long = $record->location->longitude;
			$var = array(
				'isocountry' => $isocountry,
				'continent' => $continent,
				'country' => $country,
				'subdivisi' => $subdivisi,
				'city' => $city,
				'lat' => $lat,
				'long' => $long,
			);
			$ct->update(array("_id" => new MongoId($dba['_id'])), array('$set' => $var));
		}
	}

	public function tag()
	{		
		//require_once DOCROOT.'lib/geoip2.phar';

		//use GeoIp2\Database\Reader;
		
		//$reader = new GeoIp2\Database\Reader(DOCROOT.'lib/GeoIP2-City.mmdb');
		//die("run");
		$db = Db::init();
		$ct = $db->tag_logs;
		$p = array (
  			'useragent' => 
  				array('$exists' => true)
		);
		$dbk = $ct->find($p);
		foreach($dbk as $dba)
		{
			$ua_info = parse_user_agent($dba['useragent']);
			$platform = $ua_info['platform'];
			$browser = $ua_info['browser'];
			$version = $ua_info['version'];
			
			$var = array(
				'platform' => $platform,
				'browser' => $browser,
				'version' => $version
			);
			$ct->update(array("_id" => new MongoId($dba['_id'])), array('$set' => $var));
		}
		
		$p = array (
  			'ipaddress' => 
  				array('$exists' => true)
		);
		$dbk = $ct->find($p);
		require_once DOCROOT.'lib/geoip2.phar';
		$reader = new GeoIp2\Database\Reader(DOCROOT.'lib/GeoIP2-City.mmdb');
		foreach($dbk as $dba)
		{
			$record = $reader->city($dba['ipaddress']);
			//print_r($record);
			//die;
			$isocountry = $record->country->isoCode;
			$continent = $record->continent->name;
			$country = $record->country->name;
			$subdivisi = $record->mostSpecificSubdivision->names['en'];
			$city = $record->city->name;
		
			$lat = $record->location->latitude;
			$long = $record->location->longitude;
			$var = array(
				'isocountry' => $isocountry,
				'continent' => $continent,
				'country' => $country,
				'subdivisi' => $subdivisi,
				'city' => $city,
				'lat' => $lat,
				'long' => $long,
			);
			$ct->update(array("_id" => new MongoId($dba['_id'])), array('$set' => $var));
		}
	}
}
