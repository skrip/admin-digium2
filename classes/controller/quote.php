<?php
	/**
	 * 
	 */
	class quote_controller extends controller {
		
		function __construct() {
			parent::__construct();
		}
		
		public function index()
		{
			$page = "";
			if (isset($_GET['page'])) 
				$page = $_GET['page'];
			
			$search = "";
			
			if(isset($_POST['search']))
				$search = $_POST['search'];
			
			$quer = array();
			
			if(strlen(trim($search)) > 0)
			{
				$regex = new MongoRegex("/".$search."/i"); 
				$quer = array('quote' => $regex);
			}
			
			if(strlen(trim($page)) > 0)
				$page = intval($page);
			else
				$page = 1;
			
			$docs_per_page = 10;
			$skip = (int)($docs_per_page * ($page - 1));
			
			$db = Db::init();
			$quote = $db->kutipan;
			$mquote = $quote -> find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
			
			$count = $quote->count($quer);
			
			$pg = new Pagination();
			$pg -> pag_url = "/quote/index?page=";
			$pg -> calculate_pages($count, $docs_per_page, $page);
			
			$var = array(
				'data' => $mquote,
				'pagination' => $pg->Show(),
				'search' => $search,
				'idx' => (($page-1)*$docs_per_page)+1
			);
			
			$this->js[] = "/public/js/delete.js";
			$this->js[] = "/public/js/vendors/animation/animation.js";
			$this->render("", "quote/index.php", $var);
		}
		
		public function add()
		{
			$db = Db::init();
			$kutipan = $db->kutipan;
			
			$quotearab = '';
			$quotelatin = '';
			$quotename = '';
			$publish = '';
			$publish_date = '';
			$publish_time = '';
			$time_created = '';
			$error = array();
			$info = array();
			
			if (!empty($_POST)) {
				$publish_date = strtotime($_POST['publish_date']);
				$publish_time = strtotime($_POST['publish_time']);
					
				$quotearab = $_POST['quotearab'];
				$quotelatin = $_POST['quotelatin'];
				$quotename = $_POST['quotename'];
				
				$validator = new Validator();
				$validator->addRule('quotearab', array('require', 'minlength' => 3));
				
				if (isset($_POST['publish'])){
					$publish = 'yes';
				}elseif (!isset($_POST['publish'])) {
					$publish = 'not';
				}

				$validator->setData(array('quotearab' => $quotearab));
				
				if ($validator->isValid()) {
					$var = array(
						'quotearab' => $quotearab,
						'quotelatin' => $quotelatin,
						'quotename' => $quotename,
						'publish' => $publish,
						'publish_date' => $publish_date,
						'publish_time' => $publish_time,
						'time_created' => $time_created,
						'created_by' => $_SESSION['userid']
					);
					
					$kutipan -> insert($var);
					
					$this->redirect('/quote/index');
					exit;
				}
				else {
					$error = $validator->getErrors();
				}
			}
			
			$var = array(
				'quotearab' => $quotearab,
				'quotelatin' => $quotelatin,
				'quotename' => $quotename,
				'publish' => $publish,
				'publish_date' => $publish_date,
				'publish_time' => $publish_time,
				'info' => $info,
				'link' => '/quote/add'
			);
			$this->js[] = "/public/js/tag-it.min.js";		
			
			$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
			
			$this->render("", "quote/add.php", $var);
		}
		
		public function edit()
		{
			$id = $_GET['id'];
			$db = Db::init();
			
			$kutipan = $db->kutipan;
			
			$mkut = $kutipan->findOne(array('_id' => new MongoId($id)));
			
			$quotearab  = $mkut['quotearab'];
			$quotelatin = $mkut['quotelatin'];
			$quotename = $mkut['quotename'];
			$publish = $mkut['publish'];
			$time_created = $mkut['time_created'];
			$error = array();
			$info = array();
			
			if (!empty($_POST)) {
				$quotearab = $_POST['quotearab'];
				$quotelatin = $_POST['quotelatin'];
				$quotename = $_POST['quotename'];
				
				$validator = new Validator();
				
				$validator->addRule('quotearab', array('require', 'minlength' => 3));
				
				if (isset($_POST['publish'])){
					$publish = 'yes';
				}elseif (!isset($_POST['publish'])) {
					$publish = 'not';
				}
				
				$validator->setData(array('quotearab' => $quotearab));
				
				if ($validator->isValid()) {
					$var = array(
						'quotearab' => $quotearab,
						'quotelatin' => $quotelatin,
						'quotename' => $quotename,
						'publish' => $publish,
						'time_updated' => time(),
						'updated_by' => $_SESSION['userid']
					);
					
					$kutipan->update(array('_id' => new MongoId($id)), array('$set' => $var));
					$this->redirect('/quote/index');
					exit;
				}
				else {
					$error = $validator->getErrors();
				}
			}
			
			$var = array(
				'quotearab' => $quotearab,
				'quotelatin' => $quotelatin,
				'quotename' => $quotename,
				'publish' => $publish,
				'info' => $info,
				'link' => '/quote/edit?id='.$id
			);
			
			$this->js[] = "/public/js/tag-it.min.js";		
			
			$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
			$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
			
			$this->render("", "quote/add.php", $var);
		}
		
		public function delete()
		{
			$id = $_GET['id'];
			$db = Db::init();
			
			$kutipan = $db->kutipan;
			$kutipan -> remove(array('id' => new MongoId($id)));
			$this->redirect('/quote/index');
			exit;
		}

	}
?>