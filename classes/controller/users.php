<?php

class users_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$db = Db::init();
		$usr = $db->users;
		$c = $usr->find();
		
		$var = array(
			'data' => $c
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "users/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$usr = $db->users;
		
		$name = '';
		$email = '';
		$active = 'no';
		$role = '';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$name = $_POST['name'];
			$email = $_POST['email'];
			if(isset($_POST['active']))
				$active = 'yes';
			$role = $_POST['role'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('email', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'email' => $email,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{				
				$var = array(
					'name' => $name,
					'email' => $email,
					'active' => $active,
					'role' => $role,
					'time_created' => time(),
					'author_id' => $_SESSION['userid']
				);
				$usr->insert($var);
				$this->saveEmail(trim($var['_id']), $email);
				$this->redirect('/users/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'name' => $name,
			'email' => $email,
			'active' => $active,
			'role' => $role,
			'error' => $error,
			'info' => $info,
			'link' => '/users/add'
		);
		$this->render("", "users/add.php", $var);
	}

	private function saveEmail($userid, $em)
	{
		$db = Db::init();
		$email = $db->emails;
		$email->remove(array('user_id' => $userid));
		$res = explode(",", $em);
		foreach($res as $r)
		{
			$e = $email->findone(array('user_id' => $userid, 'email' => trim($r)));
			if(!isset($e['_id']))
			{
				$var = array(
					'user_id' => $userid,
					'email' => trim($r),
				);
				$email->insert($var);
			}
		}
	}
	
	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$usr = $db->users;
		$u = $usr->findone(array('_id' => new MongoId($id)));
		
		$name = $u['name'];
		$email = $u['email'];
		$active = $u['active'];
		$role = $u['role'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$name = $_POST['name'];
			$email = $_POST['email'];
			if(isset($_POST['active']))
				$active = 'yes';
			$role = $_POST['role'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('email', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'email' => $email,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'name' => $name,
					'email' => $email,
					'active' => $active,
					'role' => $role,
					'author_id' => $_SESSION['userid']
				);
				$usr->update(array("_id" => new MongoId($id)), array('$set' => $var));
				$this->saveEmail($id, $email);
				$this->redirect('/users/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'name' => $name,
			'email' => $email,
			'active' => $active,
			'role' => $role,
			'error' => $error,
			'info' => $info,
			'link' => '/users/edit?id='.$id
		);
		$this->render("", "users/add.php", $var);
	}

	public function profile()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$usr = $db->users;
		$musr = $usr->findOne(array('_id' => new MongoId($id)));
		
		$name = $musr['name'];
		
		$birth = '';
		if(isset($musr['birthdate']))
			$birth = $musr['birthdate'];
		
		$from = '';
		if(isset($musr['from']))
			$from = $musr['from'];
		
		$bio = '';
		if(isset($musr['biografi']))
			$bio = $musr['biografi'];
		
		$foto = '';
		if(isset($musr['foto']))
			$foto = $musr['foto'];
		
		$active = $musr['active'];
		$created = $musr['time_created'];
				
		$info = array();
		$error = array();
				
		if(!empty($_POST))
		{
			$name = $_POST['name'];
			$bio = $_POST['bio'];									
			$from = $_POST['from'];
			
			$validator = new Validator();
			$validator->addRule('name', array('require', 'minlength' => 3));
			
			$validator->setData(array(
				'name' => $name
			));
			
			if($validator->isValid())
			{
				if(isset($_POST['birthdate']))
				{
					$dt = explode('-', $_POST['birthdate']);
					if((is_array($dt)) && (count($dt) > 1))
					{
						$birth = strtotime($dt[2]."-".$dt[1]."-".$dt[0]);
					}
				}
								
				$namafileimage = '';
				$namafileshaimage = $foto;
				if(isset($_FILES['foto']['name']))
				{
					$namafileimage = $_FILES['foto']['name'];
					if(strlen(trim($namafileimage)) > 0)
					{
						$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
					}
				}
				
				$var = array(
					'name' => $name,
					'biografi' => $bio,
					'foto' => $namafileshaimage,
					'birthdate' => $birth,
					'from' => $from,
					'author_id' => $_SESSION['userid']
				);
				
				$usr->update(array('_id' => new MongoId($id)), array('$set' => $var));
				
				if(strlen(trim($namafileshaimage)) > 0)
				{					
					if(trim($foto) != trim($namafileshaimage))
					{
						$ftp = new upload();
						if(strlen(trim($foto)) > 0)
							$ftp->delete($foto, 'image_user');					
						$ftp->upload($_FILES['foto']['tmp_name'], $namafileshaimage, 'image_user');
					}						
				}
				
				$this->redirect('/users/index');
				exit;
			}
			else {
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'name' => $name,
			'birthdate' => $birth,
			'bio' => $bio,
			'from' => $from,
			'info' => $info,
			'foto' => $foto,
			'active' => $active,
			'created' => $created,
			'error' => $error,
			'link' => '/users/profile?id='.$id
		);
		
		$this->render("", 'users/profile.php', $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$usr = $db->users;
		
		$usr->remove(array('_id' => new MongoId($id)));
		$this->redirect('/users/index');
		exit;
	}
	
	private function findexts ($filename) { 
		$path_parts = pathinfo($filename);

		return $path_parts['extension'];
	}
}