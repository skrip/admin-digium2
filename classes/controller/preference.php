<?php

class preference_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$quote = "";
		$video_url = "";
		$facebook = "";
		$twitter = "";
		$google = "";
		$meta_description = "";
		$meta_author = "";
		$meta_keywords = "";
		$description = "";
		$motto = "";
		$contact_description = "";
		$contact_email = "";
		$contact_phone = "";
		$contact_address = "";
		$contact_lat = "";
		$contact_long = "";
		$error = array();
		$info = array();
		
		$db = Db::init();
		$pref = $db->preference;
			
		if(!empty($_POST))
		{
			$quote = $_POST['quote'];
			$video_url = $_POST['video_url'];
			$facebook = $_POST['facebook'];
			$twitter = $_POST['twitter'];
			$google = $_POST['google'];
			$description = $_POST['description'];
			$motto = $_POST['motto'];
			
			$meta_description = $_POST['meta_description'];
			$meta_author = $_POST['meta_author'];
			$meta_keywords = $_POST['meta_keywords'];
			
			$contact_description = $_POST['contact_description'];
			$contact_email = $_POST['contact_email'];
			$contact_phone = $_POST['contact_phone'];
			$contact_address = $_POST['contact_address'];
			$contact_lat = $_POST['contact_lat'];
			$contact_long = $_POST['contact_long'];
			
			$validator = new Validator();
            $validator->addRule('description', array('require', 'minlength' => 3));
			$validator->addRule('facebook', array('require', 'minlength' => 3));
			$validator->addRule('twitter', array('require', 'minlength' => 3));
			$validator->addRule('google', array('require', 'minlength' => 3));
                    
            $validator->setData(
                array(
				  'description' => $description,
				  'facebook' => $facebook,
				  'twitter' => $twitter,
				  'google' => $google,
            	)
			);
                    
            if ($validator->isValid())
			{
				$p = $pref->findOne(array());
				if(isset($p['_id']))
				{
					$var = array(
						'quote' => $quote,
						'video_url' => $video_url,
						'facebook' => $facebook,
						'twitter' => $twitter,
						'google' => $google,
						'description' => $description,
						'motto' => $motto,
						'meta_description'=> $meta_description,
						'meta_author' => $meta_author,
						'meta_keywords' => $meta_keywords,
						'contact_description' => $contact_description,
						'contact_email' => $contact_email,
						'contact_phone' => $contact_phone,
						'contact_address' => $contact_address,
						'contact_lat' => $contact_lat,
						'contact_long' => $contact_long
					);
					$pref->update(array("_id" => new MongoId($p['_id'])), array('$set' => $var));
					$p = array(
						'title' => 'UPDATE SUKSES',
						'info' => 'data preference berhasil di update'
					);
					$info[] = $p;
				}
				else
				{
					$var = array(
						'quote' => $quote,
						'video_url' => $video_url,
						'facebook' => $facebook,
						'twitter' => $twitter,
						'google' => $google,
						'description' => $description,
						'motto' => $motto,
						'meta_description'=> $meta_description,
						'meta_author' => $meta_author,
						'meta_keywords' => $meta_keywords,
						'contact_description' => $contact_description,
						'contact_email' => $contact_email,
						'contact_phone' => $contact_phone,
						'contact_address' => $contact_address,
						'contact_lat' => $contact_lat,
						'contact_long' => $contact_long
					);
					$pref->insert($var);	
					$p = array(
						'title' => 'INSERT SUKSES',
						'info' => 'data preference berhasil di insert'
					);
					$info[] = $p;
				}
				
			}
			else
				$error = $validator->getErrors();
		}
		else {			
			$p = $pref->findOne(array());
			if(isset($p['_id']))
			{
				$quote = $p['quote'];
				$video_url = $p['video_url'];
				$facebook = $p['facebook'];
				$twitter = $p['twitter'];
				$google = $p['google'];
				$description = $p['description'];
				$motto = $p['motto'];
				$meta_description = $p['meta_description'];
				$meta_author = $p['meta_author'];
				$meta_keywords = $p['meta_keywords'];
				$contact_description = $p['contact_description'];
				$contact_email = $p['contact_email'];
				$contact_phone = $p['contact_phone'];
				$contact_address = $p['contact_address'];
				$contact_lat = $p['contact_lat'];
				$contact_long = $p['contact_long'];
			}
		}
		
		$var = array(
			'quote' => $quote,
			'video_url' => $video_url,
			'facebook' => $facebook,
			'twitter' => $twitter,
			'google' => $google,
			'description' => $description,
			'motto' => $motto,
			'meta_description'=> $meta_description,
			'meta_author' => $meta_author,
			'meta_keywords' => $meta_keywords,
			'contact_description' => $contact_description,
			'contact_email' => $contact_email,
			'contact_phone' => $contact_phone,
			'contact_address' => $contact_address,
			'contact_lat' => $contact_lat,
			'contact_long' => $contact_long,
			'error' => $error,
			'info' => $info
		);
		$this->js[] = "/public/js/tinymce/tinymce.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
		$this->js[] = "/public/js/controller/content.js";
		$this->render("", "preference/index.php", $var);
	}
}