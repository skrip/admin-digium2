<?php

class role_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$db = Db::init();
		$role = $db->roles;
		$c = $role->find();
		
		$var = array(
			'data' => $c
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "role/index.php", $var);
	}
	
	public function add()
	{
		$db = Db::init();
		$cat = $db->roles;
		
		$name = '';;
		$description = '';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$name = $_POST['name'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('description', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'description' => $description,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'name' => $name,
					'description' => $description,
				);
				$cat->insert($var);
				$this->redirect('/role/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/role/add'
		);
		$this->render("", "role/add.php", $var);
	}

	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$cat = $db->roles;
		$col = $cat->findOne(array('_id' => new MongoId($id)));
		
		$name = $col['name'];
		$description = $col['description'];
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$name = $_POST['name'];
			$description = $_POST['description'];
			$validator = new Validator();
            $validator->addRule('name', array('require', 'minlength' => 3));
			$validator->addRule('description', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'description' => $description,
				  'name' => $name,
            	)
			);
                    
            if ($validator->isValid())
			{
				$var = array(
					'name' => $name,
					'description' => $description,
				);
				$cat->update(array('_id' => new MongoId($id)),$var);
				$this->redirect('/role/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		
		$var = array(
			'name' => $name,
			'description' => $description,
			'error' => $error,
			'info' => $info,
			'link' => '/role/edit?id='.$id
		);
		$this->render("", "role/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$role = $db->roles;
		
		$content->remove(array('_id' => new MongoId($id)));
		$this->redirect('/role/index');
		exit;
	}
}