<?php

class error_controller
{
	public function err404()
	{
		include(DOCVIEW."welcome/404.php");
	}
	
	public function err401()
	{
		include(DOCVIEW."welcome/401.php");
	}
	
	public function err500()
	{
		include(DOCVIEW."welcome/500.php");
	}
}