<?php

class inbox_controller extends controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	/*public function kirimemail()
	{
		$mail = new maillib();
		$dem = $mail->periksa();
		if(!$dem)
		{
			$this->render("", "inbox/belumada.php", array());
			exit;
		}
		
		$mail             = new PHPMailer();
		$body             = 'test kirim email dari smpt sendiri';
		//$body             = eregi_replace("[\]",'',$body);

		$mail->IsSMTP(); // telling the class to use SMTP
		//$mail->Host       = "ssl://smtp.gmail.com"; // SMTP server
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = 'mail.dboxid.com'; //$dem['domain_digitama'];      // sets GMAIL as the SMTP server
		$mail->Port       = 25;                   // set the SMTP port for the GMAIL server
		$mail->Username   = 'contact@digishelf.dboxid.com';  // GMAIL username
		$mail->Password   = 'c1mah1';            // GMAIL password

		$mail->SetFrom('contact@digishelf.dboxid.com', 'dhani');

		//$mail->AddReplyTo("user2@gmail.com', 'First Last");

		$mail->Subject    = "new register";

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		$mail->MsgHTML($body);

		$mail->AddAddress($dem['email_digitama'], 'dhani kuswendha');

		//$mail->AddAttachment("images/phpmailer.gif");      // attachment
		//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
		
		if(!$mail->Send()) {
		  echo 'Message could not be sent.';
    		echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		  echo 'berhasil';
		}
	}*/
	
	public function outbox() 
	{
		$mail = new maillib();
		$dem = $mail->periksa();
		if(!$dem)
		{
			$this->render("", "inbox/belumada.php", array());
			exit;
		}

		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array(
			'email_src' => $_SESSION['email'],
		);
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array(
				'subject' => $regex,
				'email_src' => $_SESSION['email'],
			);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$dboxes = DBDeboxs::init();
		$em = $dboxes->user_outboxs;
		$c = $em->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
		$count = $em->count($quer);
		//echo $count.'</br>';
		//echo $skip.'</br>';
		//die;
		
		$pg = new Pagination();
		$pg -> pag_url = "/outbox/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "inbox/outbox.php", $var);
	}
	
	public function index() 
	{
		$mail = new maillib();
		$dem = $mail->periksa();
		if(!$dem)
		{
			$this->render("", "inbox/belumada.php", array());
			exit;
		}

		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array(
			'email_src' => $_SESSION['email'],
		);
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array(
				'subject' => $regex,
				'email_src' => $_SESSION['email'],
			);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$dboxes = DBDeboxs::init();
		$em = $dboxes->user_mailboxs;
		$c = $em->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
		$count = $em->count($quer);
		//echo $count.'</br>';
		//echo $skip.'</br>';
		//die;
		
		$pg = new Pagination();
		$pg -> pag_url = "/inbox/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "inbox/index.php", $var);
	}
	
	private function cariHuruf($str, $huruf)
	{
		for($i=0;$i<strlen($str);$i++)
		{
			if($str[$i] == $huruf)
				return true;
		}
		return false;
	}
	
	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}
	
	public function create() 
	{
		$mail = new maillib();
		$dem = $mail->periksa();
		if($dem)
		{
			$this->redirect('/inbox/index');
			exit;
		}
		
		if(!empty($_POST))
		{
			$domain = '';
			if(isset($_POST['domain']))
			{
				if(trim($_POST['domain']) == "0")
					$domain = "";
				else
					$domain = trim($_POST['domain']);
			}
			
			$email = '';
			if(isset($_POST['email']))
				$email = trim($_POST['email']);
			
			$error = array();
			$domainok = 0;
			$emailok = 0;
			if((strlen(trim($domain)) > 0) && (strlen(trim($email)) > 0))
			{
				$sql = dbsql::init();
				$sql->where ("name", $domain);
				$dm = $sql->getOne("virtual_domains");
				if(isset($dm['name']))
				{
					$domainok = 1;
				}
				else {
					//$p = array('domain tidak dipilih, silahkan pilih terlebih dahulu !');
					$error['domain'] = 'domain tidak dipilih, silahkan pilih terlebih dahulu !';
				}
				$erroremail = array();
				if ($this->cariHuruf($email, ' ')) {
				    $erroremail[] = 'email tidak boleh mengandung spasi!';
					//$erroremail[] = $p;
				}
				else {
					if(strlen($email) < 5)
					{
						$p = array('minimal nama email harus lebih dari 5 karakter!');
						$erroremail[] = $p;
					}
					else {
						if ($this->cariHuruf($email, '"')) {
						    //$p = array('email tidak boleh mengandung karakter \"!');
							$erroremail[] = 'email tidak boleh mengandung karakter \"!';
						}
						else
						{
							if (!filter_var($email.'@'.$domain, FILTER_VALIDATE_EMAIL)) {
							    //$p = array('email di ketik masih salah silahkan di isi yang lain!');
								$erroremail[] = 'email di ketik masih salah silahkan di isi yang lain!';
							}
							else {
								$sql->where ("email", $email.'@'.$domain);
								$em = $sql->getOne("virtual_users");
								if(!isset($em['email']))
									$emailok = 1;
								else {
									//$p = array('email '.$email.'@'.$domain.' sudah ada, silahkan pilih yang lain!');
									$erroremail[] = 'email '.$email.'@'.$domain.' sudah ada, silahkan pilih yang lain!';
								}	
							}
						}
					}
				}
				$error['email'] = $erroremail;
			}
			else {
				if(strlen(trim($email)) == 0)
				{
					$p = array('Email belum diisi !');
					$error['email'] = $p;
				}
				if(strlen(trim($domain)) == 0)
				{
					$p = array('Domain belum di pilih !');
					$error['domain'] = $p;
				}
			}
			//
			if($domainok && $emailok)
			{
				$sql = dbsql::init();
				$sql->where ("name", $domain);
				$dm = $sql->getOne("virtual_domains");
				$domainid = 0;
				if(isset($dm['name']))
				{
					$domainid = $dm['id'];
				}
				
				if($domainid > 0)
				{
					$defaultpassword = $this->generateRandomString();
					$data = array(
					    'domain_id' => $domainid,
					    'password' => $sql->func("ENCRYPT('".$defaultpassword."', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16)))", array()),
					    'email' => $email.'@'.$domain,
					);
					
					$id = $sql->insert ('virtual_users', $data);
					if ($id)
					{
					    $dboxes = DBDeboxs::init();
						$eml = $dboxes->user_emails;
						$var = array(
							'email_src' => $_SESSION['email'],
							'password_digitama' => $defaultpassword,
							'email_digitama' => $email.'@'.$domain,
							'domain_digitama' => $domain,
							'created_time' => time()
						);
						$eml->insert($var);
						$this->redirect('/inbox/index');
						exit;
					}
					else
					{
						$p = array('insert failed: ' . $sql->getLastError());
						$error['email'] = $p;
					}
				}
			}
			else {
				$p = array(
					'error' => $error,
					'email' => $email,
					'domain' => $domain,
					'link' => '/inbox/create'
				);
				
				$this->render("", "inbox/create.php", $p);
				return;
			}
		}
		
		$p = array(
			'email' => '',
			'domain' => '',
			'link' => '/inbox/create'
		);
		$this->render("", "inbox/create.php", $p);
	}
	
	public function compose() 
	{
		$mail = new maillib();
		$dem = $mail->periksa();
		if(!$dem)
		{
			$this->render("", "inbox/belumada.php", array());
			exit;
		}
		
		if(!empty($_POST))
		{
			$to = '';
			if(isset($_POST['to']))
				$to = $_POST['to'];
			$cc = '';
			if(isset($_POST['cc']))
				$cc = $_POST['cc'];
			$bcc = '';
			if(isset($_POST['bcc']))
				$bcc = $_POST['bcc'];
			$subject = '';
			if(isset($_POST['subject']))
				$subject = $_POST['subject'];
			$isi = '';
			if(isset($_POST['isi']))
				$isi = $_POST['isi'];
			
			$validator = new Validator();
			$validator->addRule('to', array('require', 'minlength' => 3));
			$validator->addRule('subject', array('require', 'minlength' => 3));
			
			$validator->setData(
                array(
				  'to' => $to,
				  'subject' => $subject,				  
            	)
			);
                    
            if ($validator->isValid())
			{
				$c = explode(',', $cc);
				$acc = array();
				foreach ($c as $cd) {
					$acc[] = trim($cd);
				}
				
				$b = explode(',', $bcc);
				$bcc = array();
				foreach ($b as $cd) {
					$bcc[] = trim($cd);
				}
				
				$p = array(
					'to' => $to,
					'toname' => '',
					'subject' => $subject,
					'isi' => $isi,
					'cc' => $acc,
					'bcc' => $bcc,
					'time_created' => time(),
					'from' => $dem['email_digitama'],
					'email_src' => $_SESSION['email']
				);
				$mail->kirimEmail($p);
				$this->redirect('/inbox/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}
		$p = array(
			
		);
		$this->js[]= '/public/js/vendors/summernote/summernote.min.js';
		$this->render("", "inbox/compose.php", $p);
	}
}