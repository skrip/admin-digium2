<?php

class welcome_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		
		$mcat = $cat -> find(array('status' => 'not'));
		
		//---------------------terbaru----------------//
		
		$quer = array();
		$query = array();
		foreach ($mcat as $key) {
			$q = array('category.category_id' => array('$ne' => trim($key['_id'])));
			$quer[] = $q;
		}

		$query = array('$and' => $quer);

		$mcontent = $cont -> find(array('$and' => array( array('publish' => 'yes'), $query))) -> limit(5) -> sort(array("time_created" => -1));
		$countnew = $mcontent->count();
		//------------------------------ End Terbaru ---------------//
		
		
		//------------------------------ Terpopuler ---------------//
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array('category.category_id' => array('$ne' => trim($keypop['_id'])));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpop = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop))) -> limit(5) -> sort(array("count" => -1));
		$countpop = $mcontentpop -> count();
		//------------------------------ End Terpopuler ---------------//
		
		
		//------------------------------ Terpopuler This week ---------------//
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array(
			'category.category_id' => array('$ne' => trim($keypop['_id'])), 
			'time_created' => array('$gt' => strtotime("-1 week"), '$lt' => time()));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpopweek = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop))) -> limit(5) -> sort(array("count" => -1));
		$countpopweek = $mcontentpopweek -> count();
		//------------------------------ End Terpopuler ---------------//
		
		
		//-------------------------------- Di Komentari ---------------- //
		$querkom = array();
		$querykom = array();
		
		foreach ($mcat as $keykom) {
			$qkom = array('category.category_id' => array('$ne' => trim($keykom['_id'])));
			$querkom[] = $qkom;
		}
		$querykom = array('$and' => $querkom);

		$mcontentkom = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querykom)));
			
			
			
		//-------------------------------- End Di Komentari -------------//
		
		
		//------------------------------ USER Baru ----------------------//
			$userbaru = $db->users_digiumm;
			$muserbaru = $userbaru -> find() -> limit(10) -> sort(array("time_created" => -1));
		
		//------------------------------ END USer Baru ------------------//
		
		$var = array(
			'userbaru' => $muserbaru,
			'komentar' => $mcontentkom,
			'terbaru' => $mcontent,
			'countnew' => ceil($countnew / 5),
			'terpopuler' => $mcontentpop,
			'countpop' => ceil($countpop / 5),
			'terpopulerweek' => $mcontentpopweek,
			'countpopweek' => ceil($countpopweek / 5)
		);
		$this->render("", "welcome/index.php", $var);
	}

	public function loadmorenew(){
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		$mcat = $cat -> find(array('status' => 'not'));
		
		$page = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if (strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 5;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$quer = array();
		$query = array();
		foreach ($mcat as $key) {
			$q = array('category.category_id' => array('$ne' => trim($key['_id'])));
			$quer[] = $q;
		}

		$query = array('$and' => $quer);

		$mcontent = $cont -> find(array('$and' => array( array('publish' => 'yes'), $query))) -> limit($docs_per_page)-> skip($skip) -> sort(array("time_created" => -1));
		
		$categbaru = $db->categories;
    	$n = $skip + 1;
		foreach ($mcontent as $keybaru) {
			echo '<tr>';
            echo '<td><span class="num">'.$n.'</span></td>';
			
			$catname = '';
			foreach ($keybaru['category'] as $catbaru) {										
				$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
				$catname = $mcategbaru['name'];
			}
												
            echo '<td><h5>'.$keybaru['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
				
			/*$status = '<span class="label label-danger">Not Publish</span>';
            if ($keybaru['publish'] == 'yes')
				$status = '<span class="label label-success">Publish </span><span class="badge">'.date("d F Y", $keybaru['time_created']).'</span>';*/						
            echo '<td><span class="badge">'.date("d F Y", $keybaru['time_created']).'</span></td>';
            //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
            //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
            //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
          	echo '</tr>';
			$n++;
		}
	}

	public function loadmorepopular(){
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		$mcat = $cat -> find(array('status' => 'not'));
		
		$page = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if (strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 5;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array('category.category_id' => array('$ne' => trim($keypop['_id'])));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpop = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop)))  -> limit($docs_per_page)-> skip($skip) -> sort(array("count" => -1));
		
		$categbaru = $db->categories;
		$nn = $skip + 1;
		foreach ($mcontentpop as $keypop) {
			echo '<tr>';
            echo '<td><span class="num">'.$nn.'</span></td>';
												
			$catname = '';
			foreach ($keypop['category'] as $catbaru) {										
				$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
				$catname = $mcategbaru['name'];
			}
												
            echo '<td><h5>'.$keypop['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
										
            echo '<td><span class="badge">'.number_format( $keypop['count'] , 0 , '' , '.' ).'</span></td>';
            //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
            //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
            //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
          	echo '</tr>';
			$nn++;
		}
	}

	public function loadmorepopularweek(){
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		$mcat = $cat -> find(array('status' => 'not'));
		
		$page = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if (strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 5;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array('category.category_id' => array('$ne' => trim($keypop['_id'])),
				'time_created' => array('$gt' => strtotime("-1 week"), '$lt' => time()));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpop = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop)))  -> limit($docs_per_page)-> skip($skip) -> sort(array("count" => -1));
		
		$categbaru = $db->categories;
		$nn = $skip + 1;
		foreach ($mcontentpop as $keypop) {
			echo '<tr>';
            echo '<td><span class="num">'.$nn.'</span></td>';
												
			$catname = '';
			foreach ($keypop['category'] as $catbaru) {										
				$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
				$catname = $mcategbaru['name'];
			}
												
            echo '<td><h5>'.$keypop['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
										
            echo '<td><span class="badge">'.number_format( $keypop['count'] , 0 , '' , '.' ).'</span></td>';
            //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
            //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
            //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
          	echo '</tr>';
			$nn++;
		}
	}

	public function loadmorepopularrange(){
		$db = Db::init();
		$cont = $db->contents;
		$cat = $db -> categories;
		$mcat = $cat -> find(array('status' => 'not'));
		$date_start = strtotime($_POST['start']);
		$date_finish = strtotime($_POST['finish']);
		
		$page = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if (strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 5;
		$skip = (int)($docs_per_page * ($page - 1));
		
		$querpop = array();
		$querypop = array();
		
		foreach ($mcat as $keypop) {
			$qpop = array('category.category_id' => array('$ne' => trim($keypop['_id'])),
				'time_created' => array('$gt' => $date_start, '$lt' => $date_finish));
			$querpop[] = $qpop;
		}
		$querypop = array('$and' => $querpop);

		$mcontentpop = $cont -> find(array('$and' => array( array('publish' => 'yes'), $querypop)))  -> limit($docs_per_page)-> skip($skip) -> sort(array("count" => -1));
		
		$categbaru = $db->categories;
		$nn = $skip + 1;
		foreach ($mcontentpop as $keypop) {
			echo '<tr>';
            echo '<td><span class="num">'.$nn.'</span></td>';
												
			$catname = '';
			foreach ($keypop['category'] as $catbaru) {										
				$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
				$catname = $mcategbaru['name'];
			}
												
            echo '<td><h5>'.$keypop['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
										
            echo '<td><span class="badge">'.number_format( $keypop['count'] , 0 , '' , '.' ).'</span></td>';
            //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
            //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
            //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
          	echo '</tr>';
			$nn++;
		}
	}

}