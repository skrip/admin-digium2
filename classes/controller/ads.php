<?php

class ads_controller extends controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];

		$search = "";
		if (isset($_GET['search']))
			$page = $_GET['search'];

		$quer = array();
		if (strlen(trim($search)) > 0) {
			$regex = new MongoRegex("/" . $search . "/i");
			$quer = array('title' => $regex);
		}

		if (strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;

		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));

		$db = Db::init();
		$content = $db -> ads;
		$c = $content -> find($quer) -> limit($docs_per_page) -> skip($skip) -> sort(array("time_created" => -1));
		$count = $content -> count($quer);

		$pg = new Pagination();
		$pg -> pag_url = "/ads/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);

		$var = array('data' => $c, 'pagination' => $pg -> Show(), 'search' => $search, 'idx' => (($page - 1) * $docs_per_page) + 1);

		$this -> js[] = "/public/js/delete.js";
		$this -> js[] = "/public/js/vendors/animation/animation.js";
		$this -> render("", "ads/index.php", $var);
	}

	public function add() {
		$db = Db::init();
		$content = $db -> ads;

		$title = '';
		$menu = 'Home';
		$description = '';
		$publish = 'yes';
		$error = array();
		$info = array();

		if (!empty($_POST)) {
			$title = $_POST['title'];
			$menu = $_POST['menu'];
			$description = $_POST['description'];

			$validator = new Validator();
			$validator -> addRule('title', array('require', 'minlength' => 3));
			$validator -> addRule('description', array('require', 'minlength' => 3));

			$seo = helper::string_to_underscore_name($title);

			if (!isset($_POST['publish']))
				$publish = 'not';

			$validator -> setData(array('title' => $title, 'menu' => $menu, 'description' => $description));

			if ($validator -> isValid()) {
				$var = array('title' => $title, 'menu' => $menu, 'description' => $description, 'publish' => $publish, 'time_created' => time(), 'created_by' => $_SESSION['user_id']);

				$content -> insert($var);

				$this -> redirect('/ads/index');
				exit ;
			} else {
				$error = $validator -> getErrors();
			}
		}

		$var = array('title' => $title, 'menu' => $menu, 'description' => $description, 'publish' => $publish, 'time_created' => time(), 'link' => '/ads/add');

		$this -> js[] = "/public/js/tag-it.min.js";
		$this -> js[] = "/public/js/tinymce/tinymce.min.js";
		$this -> js[] = "/public/js/vendors/forms/jquery.form.min.js";
		$this -> js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
		$this -> js[] = "/public/js/controller/content.js";
		$this -> render("", "ads/add.php", $var);
	}

	public function edit() {
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db -> ads;
		$dcont = $content -> findOne(array('_id' => new MongoId($id)));

		$title = $dcont['title'];
		$menu = $dcont['menu'];
		$description = $dcont['description'];
		$publish = $dcont['publish'];
		$error = array();
		$info = array();

		if (!empty($_POST)) {
			$title = $_POST['title'];
			$menu = $_POST['menu'];
			$description = $_POST['description'];

			$validator = new Validator();
			$validator -> addRule('title', array('require', 'minlength' => 3));
			$validator -> addRule('description', array('require', 'minlength' => 3));

			$seo = helper::string_to_underscore_name($title);

			if (isset($_POST['publish'])) {
				$publish = 'yes';
			} elseif (!isset($_POST['publish'])) {
				$publish = 'not';
			}

			$validator -> setData(array('description' => $description, 'title' => $title, 'menu' => $menu));

			if ($validator -> isValid()) {
				$var = array('title' => $title, 'menu' => $menu, 'description' => $description, 'publish' => $publish, 'time_updated' => time());

				$content -> update(array('_id' => new MongoId($id)), array('$set' => $var));

				$this -> redirect('/ads/index');
				exit ;
			} else {
				$error = $validator -> getErrors();
			}
		}

		$var = array('title' => $title, 'menu' => $menu, 'description' => $description, 'publish' => $publish, 'link' => '/ads/edit?id=' . $id);
		$this -> js[] = "/public/js/tag-it.min.js";
		$this -> js[] = "/public/js/tinymce/tinymce.min.js";
		$this -> js[] = "/public/js/vendors/forms/jquery.form.min.js";
		$this -> js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
		$this -> js[] = "/public/js/controller/content.js";
		$this -> render("", "ads/add.php", $var);
	}

	public function delete() {
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db -> ads;

		$content -> remove(array('_id' => new MongoId($id)));
		$this -> redirect('/ads/index');
		exit ;
	}

}
?>