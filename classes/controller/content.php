<?php

class content_controller extends controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$page = "";
		if (isset($_GET['page']))
			$page = $_GET['page'];
		
		$search = "";
		if (isset($_POST['search']))
			$search = $_POST['search'];
		
		$quer = array();
		if(strlen(trim($search)) > 0)
		{
			$regex = new MongoRegex("/".$search."/i"); 
			$quer = array('title' => $regex);
		}
		
		if(strlen(trim($page)) > 0)
			$page = intval($page);
		else
			$page = 1;
		
		$docs_per_page = 10;
		$skip = (int)($docs_per_page * ($page - 1));
			
		$db = Db::init();
		$content = $db->contents;
		$c = $content->find($quer)->limit($docs_per_page)->skip($skip)->sort(array("time_created" => -1));
		$count = $content->count($quer);
		
		$pg = new Pagination();
		$pg -> pag_url = "/content/index?page=";
		$pg -> calculate_pages($count, $docs_per_page, $page);
		
		$var = array(
			'data' => $c,
			'pagination' => $pg->Show(),
			'search' => $search,
			'idx' => (($page-1)*$docs_per_page)+1,
		);
		
		$this->js[] = "/public/js/delete.js";
		$this->js[] = "/public/js/vendors/animation/animation.js";
		$this->render("", "content/index.php", $var);
	
	}

	public function terpilih(){
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db->contents;
		$mcon = $content->findOne(array('_id' => new MongoId($id)));
		if (isset($mcon['_id']))
		{
			if ($_GET['set']=='yes')
			{
				$var = array(
					'terpilih' => 'yes'
				);
				$content->update(array('_id' => new MongoId(trim($mcon['_id']))), array('$set' => $var));
			}
			else
			{
				$var = array(
					'terpilih' => 'no'
				);
				$content->update(array('_id' => new MongoId(trim($mcon['_id']))), array('$set' => $var));
			}
		}
	}
	
	public function add()
	{
		$db = Db::init();
		$content = $db->contents;
		
		$title = '';
		$event_date = '';
		$description = '';
		$quotearab = '';
		$quotelatin = '';
		$quotename = '';
		$url = '';
		$contenttype = '';
		$short_description = '';
		$tag = array();
		$category = array();
		$publish = 'yes';
		$slide = 'not';
		$fileimage = '';
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$title = $_POST['title'];
			$event_date = $_POST['event_date'];
			$description = $_POST['description'];
			$short_description = $_POST['short_description'];
			$quotearab = $_POST['quotearab'];
			$quotelatin = $_POST['quotelatin'];
			$quotename = $_POST['quotename'];
			$contenttype = $_POST['contenttype'];
			$url = $_POST['url'];
			
			$validator = new Validator();
			$validator->addRule('title', array('require', 'minlength' => 3));
			$validator->addRule('description', array('require', 'minlength' => 3));
			
			$seo = helper::string_to_underscore_name($title);			
			
			$tag = $_POST['tag'];
			$dtag = $db->tags;
			$tagcat = array();
			$ex = explode(',', $tag);										
			for($x=0; $x<count($ex); $x++)
			{
				$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
				if ($mcat){
					$acat = array('tag_id' => new MongoId($mcat['_id']));
					$tagcat[] = $acat;
				}else{
					$var = array(
						'name' => trim($ex[$x])
					);
					$dtag->insert($var);
					$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
					$acat = array('tag_id' => new MongoId($mcat['_id']));
					$tagcat[] = $acat;
				}
			}

			$cat = $db->categories;
			$category = array();
			foreach($_POST['category'] as $dct)
			{
				$col = $cat->findOne(array('_id' => new MongoId($dct)));
				$seo .= helper::string_to_underscore_name($col['name']);
				
				$c = array('category_id' => $dct);
				$category[] = $c;
			}
			
			if(!isset($_POST['publish']))
				$publish = 'not';
			if(isset($_POST['slide']))
				$slide = 'yes';
			
			//$validator->addRule('category', array('require', 'minlength' => 3));			
			
			$validator->setData(
                array(
				  'description' => $description,
				  'title' => $title,				  
            	)
			);
                    
            if ($validator->isValid())
			{
				$namafileimage = '';
				$namafileshaimage = '';
				if(isset($_FILES['fileimage']['name']))
				{
					$namafileimage = $_FILES['fileimage']['name'];
					if(strlen(trim($namafileimage)) > 0)
					{
						$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
					}
				}

				$var = array(
					'title' => $title,
					'event_date' => $event_date,
					'description' => $description,
					'short_description' => $short_description,
					'publish' => $publish,
					'tag' => $tagcat,
  				    'slide_show' => $slide,
				    'category' => $category,
				    'seo' => $seo,
					'image' => $namafileshaimage,
					'quotearab' => $quotearab,
					'quotelatin' => $quotelatin,
					'quotename' => $quotename,
					'url' => $url,
					'contenttype' => $contenttype,
					'time_created' => time(),
					'created_by' => $_SESSION['userid']
					
				);
				$content->insert($var);
				if(strlen(trim($namafileshaimage)) > 0)
				{
					$ftp = new upload();
					$ftp->upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
				}
				
				if(trim($slide) == 'yes')
				{
					$sld = $db->slide_shows;
					$vvar = array(
						'content_id' => trim($var['_id']),
						'event_date' => $event_date,
						'publish' => $publish,
						'title' => $title,
						'short_description' => $short_description,
						'description' => $description,
					 	'image' => $namafileshaimage,
					 	'time_created' => time()
					);
					$sld->insert($vvar);
				}
				
				if(count($tagcat) > 0) {
					foreach($tagcat as $dtag) {
						helper::addTagContent($dtag['tag_id'], $var['_id']);
					}
				}
				
				$this->redirect('/content/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}

		$dtag = $db->tags;
		$mtag = $dtag->find();
				
		$var = array(
			'title' => $title,
			'event_date' => $event_date,
			'description' => $description,
			'short_description' => $short_description,
			'tag' => $tag,
			'category' => $category,
			'publish' => $publish,
			'slide' => $slide,
			'fileimage' => $fileimage,
			'quotearab' => $quotearab,
			'quotelatin' => $quotelatin,
			'quotename' => $quotename,
			'contenttype' => $contenttype,
			'url' => $url,
			'error' => $error,
			'info' => $info,
			'atag' => $mtag,
			'link' => '/content/add'
		);
		
		$this->js[] = "/public/js/tag-it.min.js";		
		$this->js[] = "/public/js/tinymce/tinymce.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
		$this->js[] = "/public/js/controller/content.js";
		$this->render("", "content/add.php", $var);
	}

	public function edit()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db->contents;
		$mcon = $content->findOne(array('_id' => new MongoId($id)));
		
		$title = $mcon['title'];
		$event_date = $mcon['event_date'];
		$description = $mcon['description'];
		$short_description = $mcon['short_description'];
		$tag = $mcon['tag'];
		$category = $mcon['category'];
		$publish = $mcon['publish'];
		$slide = $mcon['slide_show'];
		$fileimage = $mcon['image'];
		$time_created = $mcon['time_created'];
		
		$quotearab = '';
		if (isset($mcon['quotearab']))
			$quotearab = $mcon['quotearab'];
		
		$quotelatin = '';
		if (isset($mcon['quotelatin']))
			$quotelatin = $mcon['quotelatin'];
			
		$quotename = '';
		if (isset($mcon['quotename']))
			$quotename = $mcon['quotename'];
		
		$url = '';
		if (isset($mcon['url']))
			$url = $mcon['url'];
		
		$contenttype = '';
		if (isset($mcon['contenttype']))
			$contenttype = $mcon['contenttype'];
		
		$error = array();
		$info = array();
		
		if(!empty($_POST))
		{
			$title = $_POST['title'];
			$event_date = $_POST['event_date'];
			$description = $_POST['description'];
			$short_description = $_POST['short_description'];
			
			$quotearab = $_POST['quotearab'];
			$quotelatin = $_POST['quotelatin'];
			$quotename = $_POST['quotename'];
			$contenttype = $_POST['contenttype'];
			$url = $_POST['url'];
			
			$validator = new Validator();
            $validator->addRule('title', array('require', 'minlength' => 3));
			$validator->addRule('description', array('require', 'minlength' => 3));
			
			$seo = helper::string_to_underscore_name($title);			
			
			$ptag = $_POST['tag'];
			$dtag = $db->tags;
			$tagcat = array();
			$ex = explode(',', $ptag);										
			for($x=0; $x<count($ex); $x++)
			{
				$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
				if ($mcat){
					$acat = array('tag_id' => new MongoId($mcat['_id']));
					$tagcat[] = $acat;
				}else{
					$var = array(
						'name' => trim($ex[$x])
					);
					$dtag->insert($var);
					$mcat = $dtag->findOne(array('name' => trim($ex[$x])));
					$acat = array('tag_id' => new MongoId($mcat['_id']));
					$tagcat[] = $acat;
				}
			}

			$cat = $db->categories;
			$category = array();
			foreach($_POST['category'] as $dct)
			{
				$col = $cat->findOne(array('_id' => new MongoId($dct)));
				$seo .= '_'.helper::string_to_underscore_name($col['name']);
				
				$c = array('category_id' => $dct);
				$category[] = $c;
			}
			
			if(isset($_POST['publish'])){
				$publish = 'yes';
			} elseif (!isset($_POST['publish'])) {
				$publish = 'not';
			}
			
			if(isset($_POST['slide'])){
				$slide = 'yes';
			} elseif (!isset($_POST['slide'])) {
				$slide = 'not';
			}
				
			/*if (isset($_POST['slide']) && $publish = 'not') {
				$time_created = time();
			}*/
			//$validator->addRule('category', array('require', 'minlength' => 3));			
			
			$validator->setData(
                array(
				  'description' => $description,
				  'title' => $title,				  
            	)
			);
                    
            if ($validator->isValid())
			{
				$namafileimage = '';
				$namafileshaimage = $fileimage;
				if(isset($_FILES['fileimage']['name']))
				{
					$namafileimage = $_FILES['fileimage']['name'];
					if(strlen(trim($namafileimage)) > 0)
					{
						$namafileshaimage = sha1(date('Y-m-d H:i:s', time()).$namafileimage).'.'.$this->findexts($namafileimage);
					}
				}

				$var = array(
					'title' => $title,
					'event_date' => $event_date,
					'description' => $description,
					'short_description' => $short_description,
					'publish' => $publish,
					'tag' => $tagcat,
  				    'slide_show' => $slide,
				    'category' => $category,
				    'seo' => $seo,
					'image' => $namafileshaimage,
					'quotearab' => $quotearab,
					'quotelatin' => $quotelatin,
					'quotename' => $quotename,
					'url' => $url,
					'contenttype' => $contenttype,
					'time_updated' => time(),
				);
				$content->update(array('_id' => new MongoId($id)), array('$set' => $var));
								
				if(strlen(trim($namafileshaimage)) > 0)
				{					
					if(trim($fileimage) != trim($namafileshaimage))
					{
						$ftp = new upload();
						$ftp->delete($fileimage, 'image_content');					
						$ftp->upload($_FILES['fileimage']['tmp_name'], $namafileshaimage, 'image_content');
					}						
				}

				if(isset($_POST['slide']))
				{
					$sld = $db->slide_shows;
					$msld = $sld->findOne(array('content_id' => trim($id)));
					
					
						$vvar = array(
							'content_id' => trim($id),
							'title' => $title,
						 	'image' => $namafileshaimage,
						 	'description' => $description,
						 	'short_description' => $short_description,
						 	'publish' => $publish,
						 	'time_created' => $time_created,
						 	'time_updated' => time()
						);
						
						$sld->update(array('content_id' => trim($id)), array('$set' => $vvar));
										
				}
				
				if(count($tagcat) > 0) {
					foreach($tag as $mtg) {
						helper::deleteTagContent($mtg['tag_id'], $id);
					}
					foreach($tagcat as $dtag) {
						helper::addTagContent($dtag['tag_id'], $id);
					}
				}
				
				$this->redirect('/content/index');
				exit;
			}
			else
			{
				$error = $validator->getErrors();
			}
		}

		$dtag = $db->tags;
		$mtag = $dtag->find();
		
		$var = array(
			'title' => $title,
			'event_date' => $event_date,
			'description' => $description,
			'short_description' => $short_description,
			'tag' => $tag,
			'category' => $category,
			'publish' => $publish,
			'slide' => $slide,
			'fileimage' => $fileimage,
			'quotearab' => $quotearab,
			'quotelatin' => $quotelatin,
			'quotename' => $quotename,
			'url' => $url,
			'contenttype' => $contenttype,
			'error' => $error,
			'info' => $info,
			'atag' => $mtag,
			'link' => '/content/edit?id='.$id
		);
		
		$this->js[] = "/public/js/tag-it.min.js";		
		$this->js[] = "/public/js/tinymce/tinymce.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.form.min.js";
		$this->js[] = "/public/js/vendors/forms/jquery.maskedinput.min.js";
		$this->js[] = "/public/js/controller/content.js";
		$this->render("", "content/add.php", $var);
	}

	public function delete()
	{
		$id = $_GET['id'];
		$db = Db::init();
		$content = $db->contents;
		$mcon = $content->findOne(array('_id' => new MongoId($id)));
		$slide = $db->slide_shows;
		
		if(isset($mcon['image']))
		{
			if(strlen(trim($mcon['image'])) > 0)
			{
				$ftp = new upload();
				$ftp->delete($mcon['image'], 'image_content');
			}			
		}
		
		$content->remove(array('_id' => new MongoId($id)));
		$slide->remove(array('content_id' => $id));
		$this->redirect('/content/index');
		exit;
	}

	private function findexts ($filename) { 
		$path_parts = pathinfo($filename);

		return $path_parts['extension'];
	}
	
}
