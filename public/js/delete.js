$(document).ready( function($) {
	$('#myModal').on('click', '.btn, .close', function() {
        $(this).addClass('modal-result'); // mark which button was clicked
      }).on('hidden.bs.modal', function() {
        var result = $(this).find('.modal-result').filter('.btn-warning').length > 0; // attempt to filter by what you consider the "YES" button; if it was clicked, result should be true.
        if(result)
        {
            var id = $(this).data('link');           
            $('<form action="'+id+'" method="post" ></form>').appendTo('body').submit();
            return false;
        }
    });

    $(document).on('click', ".mmmodal", function(e) {
        e.preventDefault();
        $("#myModal").data('link', $( this ).attr('ref')).modal('show');             
    });
});
