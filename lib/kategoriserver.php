<?php

class kategoriserver
{
	public function __construct()
	{
		if(!$this->getLogin())
		{
			echo 'ada error gak connect';
			die;
		}
	}
	
	private function periksa()
	{
		if(!isset($_SESSION['cookie_category']))
		{
			return 0;
		}
		if(strlen(trim($_SESSION['cookie_category'])) == 0)
		{
			return 0;
		}
		return 1;
	}
	
	public function getCount()
	{
		if(!$this->periksa())
			return 0;
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->post('http://pulsa.deboxs.com/category_product/getcountjson', array());
		//$count = $unqlite->getCount();	
		$count = 0;
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$res = $arr['result'][0];
				$count = intval($res['count']);
			}
		}	
		
		return $count;
	}
	
	public function update($id, $data)
	{
		if(!$this->periksa())
			return 0;
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->post('http://pulsa.deboxs.com/category_product/editjson?editid='.$id, $data);
		//$count = $unqlite->getCount();	
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$res = $arr['result'][0];
				if(isset($res['update']))
				{
					if($res['update'] == "SUCCESS")
					{
						return 1;
					}
				}
			}
		}	
		
		return 0;
	}
	
	public function delete($id)
	{
		if(!$this->periksa())
			return 0;
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->post('http://pulsa.deboxs.com/category_product/deletejson?editid='.$id, $data);
		//$count = $unqlite->getCount();	
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$res = $arr['result'][0];
				if(isset($res['delete']))
				{
					if($res['delete'] == "SUCCESS")
					{
						return 1;
					}
				}
			}
		}	
		
		return 0;
	}
	
	public function insert($data)
	{
		if(!$this->periksa())
			return 0;
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->post('http://pulsa.deboxs.com/category_product/insertjson', $data);
		//$count = $unqlite->getCount();	
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$res = $arr['result'][0];
				if(isset($res['insert']))
				{
					if($res['insert'] == "SUCCESS")
					{
						return 1;
					}
				}
			}
		}	
		
		return 0;
	}
	
	public function findOne($id)
	{
		if(!$this->periksa())
			return array();
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->get('http://pulsa.deboxs.com/category_product/getonejson', array(
			'id' => $id,
		));
		//$count = $unqlite->getCount();	
		$hasil = array();
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$hasil = $arr['result'][0];
			}
		}	
		
		return $hasil;
	}
	
	public function getAllParent($parent)
	{
		if(!$this->periksa())
			return array();
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->get('http://pulsa.deboxs.com/category_product/getallparentjson', array(
			'parent' => $parent,
		));
		//$count = $unqlite->getCount();	
		$hasil = array();
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$hasil = $arr['result'];
			}
		}	
		
		return $hasil;
	}
	
	public function getAll($skip, $limit)
	{
		if(!$this->periksa())
			return array();
		
		$curl = new Curl();
		$curl->setCookie('_SDIGIBEAT_SESS_', $_SESSION['cookie_category']);
		$curl->get('http://pulsa.deboxs.com/category_product/indexjson', array(
			'skip' => $skip,
			'limit' => $limit
		));
		//$count = $unqlite->getCount();	
		$hasil = array();
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$hasil = $arr['result'];
			}
		}	
		
		return $hasil;
	}
	
	private function getLogin()
	{
		if(isset($_SESSION['cookie_category']))
		{
			if(strlen(trim($_SESSION['cookie_category'])) > 0)
			{
				return 1;
			}
		}
		
		$curl = new Curl();
		$curl->post('http://pulsa.deboxs.com/user/loginjson', array(
			'email' => 'admin@digiumm.com',
			'password' => 'd1g1tama'
		));
		//print_r($curl->response);
		if(is_object($curl->response))
		{
			$arr = json_decode(json_encode($curl->response), true);
			if(is_null($arr['error']))
			{
				$res = $arr['result'][0];
				$_SESSION['cookie_category'] = $res['cookie'];
				return 1;
			}
		}
		
		return 0;
	}
}
