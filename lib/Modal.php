<?php

class Modal
{
	public static function Show($judul, $pesan="Anda yakin akan menghapus data ini ?")
	{		
		echo '<div id="myModal" class="modal" data-easein="bounceIn" data-easeout="bounceOut" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		echo '<div class="modal-dialog modal-sm">';
		echo '<div class="modal-content">';
        echo '<div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        echo '<h3 id="modal-title">'.$judul.'</h3>';
        echo '</div>';
        echo '<div class="modal-body">';    
        echo '<p>'.$pesan.'</p>';
        echo '</div>';
        echo '<div class="modal-footer">';
        echo '<button class="btn btn-warning" data-dismiss="modal">Hapus</button>';
        echo '<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Batal</button>';
        echo '</div>';
        echo '</div>';
		echo '</div>';
        echo '</div>';		
	}
	
	/*
 *<a href="#myModal13" role="button" class="btn btn-default" data-toggle="modal">bounce In | Out</a>
  <div id="myModal13" class="modal" data-easein="bounceIn" data-easeout="bounceOut" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Modal header</h4>
        </div>
        <div class="modal-body">
          <p>ORB &#8212 Power Admin Template. Here is the example of modal body.</p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </div>
    </div>
  </div>
	 */
}