<?php
class helper
{
	
	public static function addTagContent($tagid, $contentid) {
			$db = Db::init();
			$tag = $db->tag_contents;
			
			$q = array(
				'tag_id' => trim($tagid),
				'content_id' => trim($contentid)
			);
			
			$count = $tag->count($q);
			
			if($count == 0) {
				$data = array(
					'tag_id' => trim($tagid),
					'content_id' => trim($contentid)
				);
				$tag->insert($data);
			}
	}
	
	public static function deleteTagContent($tagid, $contentid) {
			$db = Db::init();
			$tag = $db->tag_contents;
			
			$q = array(
				'tag_id' => trim($tagid),
				'content_id' => trim($contentid)
			);
			
			$col = $tag->find($q);
			
			foreach($col as $dt) {
				$tag->remove(array('_id' => new MongoId($dt['_id'])));
			}
	}
	
	public static function string_to_underscore_name($string)
	{
		$string = preg_replace('/[\'"]/', '', $string);
		$string = preg_replace('/[^a-zA-Z0-9]+/', '_', $string);
		$string = trim($string, '_');
		$string = strtolower($string);
		 
		return $string;
	}

	public static function time_elapsed_string($ptime)
	{
	    $etime = time() - $ptime;
	
	    if ($etime < 1)
	    {
	        return '0 seconds';
	    }
	
	    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
	                30 * 24 * 60 * 60       =>  'month',
	                24 * 60 * 60            =>  'day',
	                60 * 60                 =>  'hour',
	                60                      =>  'minute',
	                1                       =>  'second'
	                );
	
	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
	        }
	    }
	}

	public static function getlinkImageResize($name, $type, $size)
	{
		$ff = "";
		if (!empty($name)){
			$path_parts = pathinfo($name);
			$ext = $path_parts['extension'];
			$f = $path_parts['filename'];                            
			$ff = IMAGE_URL.$type.'/'.$f.'.'.$size.'.'.$ext;
		}
		return $ff;
	}
	
	public static function getlinkImageOrigin($name, $type)
	{
		$ff = "";
		if (!empty($name)){
			$path_parts = pathinfo($name);
			$ext = $path_parts['extension'];
			$f = $path_parts['filename'];                            
			$ff = IMAGE_URL.$type.'/'.$f.'.'.$ext;
		}
		return $ff;
	}
	
	public static function limitString($string, $limit = 100) 
	{
	    // Return early if the string is already shorter than the limit
	    if(strlen($string) < $limit) {return $string;}
	
	    $regex = "/(.{1,$limit})\b/";
	    preg_match($regex, $string, $matches);
	    return $matches[1].'...';
	}
}
