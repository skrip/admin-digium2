<?php

class maillib
{
	var $folderattach;
	
	public function __construct() {
		$this->folderattach = "/var/www/attachment";
		$this->init();
	}
	
	public function periksa()
	{
		$dboxes = DBDeboxs::init();
		$em = $dboxes->user_emails;
		$dem = $em->findone(array('email_src' => $_SESSION['email']));
		if(!isset($dem['_id']))
			return false;
		return $dem;
	}
	
	public function kirimEmail($data)
	{
		$dem = $this->periksa();
		if(!$dem)
		{
			return;
		}
		$mailman = new CkMailMan();

		//  Any string argument automatically begins the 30-day trial.
		$success = $mailman->UnlockComponent('WILLAWMAILQ_V6R4qoSG0M4R');
		if ($success != true) {
		    print $mailman->lastErrorText() . "\n";
		    exit;
		}
		
		//  Set the SMTP server.
		$mailman->put_SmtpHost('192.168.89.39');
		
		//  Set the SMTP login/password (if required)
		$mailman->put_SmtpUsername($dem['email_digitama']);
		$mailman->put_SmtpPassword($dem['password_digitama']);
		$mailman->put_SmtpPort(25);
		$mailman->put_StartTLS(true);
		
		$db = Db::init();
		$usr = $db->users;
		$usrnm = $usr->findone(array('email' => $_SESSION['email']));
		
		//  Create a new email object
		$email = new CkEmail();
		
		$email->put_Subject($data['subject']);
		$email->put_Body($data['isi']);
		$email->put_From($usrnm['name'].' <'.$dem['email_digitama'].'>');
		$email->AddTo($data['toname'], $data['to']);
		//  To add more recipients, call AddTo, AddCC, or AddBcc once per recipient.
		
		//  Call SendEmail to connect to the SMTP server and send.
		//  The connection (i.e. session) to the SMTP server remains
		//  open so that subsequent SendEmail calls may use the
		//  same connection.
		$success = $mailman->SendEmail($email);
		if ($success != true) {
		    print $mailman->lastErrorText() . "\n";
		    exit;
		}
		
		//  Some SMTP servers do not actually send the email until
		//  the connection is closed.  In these cases, it is necessary to
		//  call CloseSmtpConnection for the mail to be  sent.
		//  Most SMTP servers send the email immediately, and it is
		//  not required to close the connection.  We'll close it here
		//  for the example:
		$success = $mailman->CloseSmtpConnection();
		if ($success != true) {
		    print 'Connection to SMTP server not closed cleanly.' . "\n";
			exit;
		}
		
		$dboxes = DBDeboxs::init();
		$emz = $dboxes->user_outboxs;
		$emz->insert($data);
	}

	private function init()
	{
		$dem = $this->periksa();
		if(!$dem)
		{
			return;
		}
		$imap = new CkImap();

		$success = $imap->UnlockComponent('WILLAWIMAPMAILQ_NWvaP7qM4M2K');
		if ($success != true) {
		    print $imap->lastErrorText() . "\n";
		    exit;
		}
		
		$imap->put_Ssl(true);
		$imap->put_Port(993);
		$success = $imap->Connect('192.168.89.39'); // $dem['domain_digitama']
		if ($success != true) {
		    print $imap->lastErrorText() . "\n";
		    exit;
		}

		$success = $imap->Login($dem['email_digitama'],$dem['password_digitama']);
		if ($success != true) {
		    print $imap->lastErrorText() . "\n";
		    exit;
		}

		$success = $imap->SelectMailbox('Inbox');
		if ($success != true) {
		    print $imap->lastErrorText() . "\n";
		    exit;
		}
		
		$fetchUids = true;
		$notSeenSearch = 'NOT SEEN';
		$all = 'ALL';
		$messageSet = $imap->Search($all,$fetchUids);
		if (is_null($messageSet)) {
		    print $imap->lastErrorText() . "\n";
		    exit;
		}
		
		$bundle = $imap->FetchBundle($messageSet);
		if (is_null($bundle)) {
		
		    print $imap->lastErrorText() . "\n";
		    exit;
		}
		
		$dboxes = DBDeboxs::init();
		$emz = $dboxes->user_mailboxs;
		
		for ($i = 0; $i <= $bundle->get_MessageCount() - 1; $i++) {
		
		    $email = $bundle->GetEmail($i);
			$recipients = array();
			for ($j = 0; $j <= $email->get_NumTo() - 1; $j++) {
				$p = array(
					'toName' => $email->getToName($j),
					'toAddr' => $email->getToAddr($j)
				);
				$recipients[] = $p;
		    }
		
			$cc = array();
		    for ($j = 0; $j <= $email->get_NumCC() - 1; $j++) {
		    	$p = array(
					'CcName' => $email->getCcName($j),
					'CcAddr' => $email->getCcAddr($j)
				);
				$cc[] = $p;
		    }
			
			$bcc = array();
		    for ($j = 0; $j <= $email->get_NumBcc() - 1; $j++) {
		    	$p = array(
					'CcName' => $email->getBccName($j),
					'CcAddr' => $email->getBccAddr($j)
				);
				$cc[] = $p;
		    }
			
			$folderattach = "";
			$fileattach = array();
			$numAttach = $imap->GetMailNumAttach($email);
		    if ($numAttach > 0) {
		        $uidStr = $email->getHeaderField('ckx-imap-uid');
		        $uid = $uidStr;
		
				$folderattach = sha1(date('Y-m-d H:i:s', time()).$this->folderattach);
		        // fullEmail is a CkEmail
		        $fullEmail = $imap->FetchSingle($uid,true);
		        if (!(is_null($fullEmail))) {
		            $fullEmail->SaveAllAttachments($this->folderattach.'/'.$folderattach);
		        }
			
		        for ($j = 0; $j <= $numAttach - 1; $j++) {
		        	$p = array(
						'filename' => $imap->getMailAttachFilename($email,$j),
					);
		            $fileattach[] = $p;
		        }
		
		    }
			
			$body = mb_convert_encoding($email->body(), "UTF-8", "ISO-8859-1");
			$p = array(
				'uidl' => $messageSet->GetId($i),
				'localDateStr' => $email->localDateStr(),
				'emailDateStr' => $email->emailDateStr(),
				'fromName' => $email->fromName(),
				'fromAddress' => $email->fromAddress(),
				'subject' => $email->subject(),
				'body' => $body,
				'time_created' => time(),
				'email_src' => $_SESSION['email'],
				'status' => 'UNREAD',
				'bcc' => $bcc,
				'cc' => $cc,
				'recipients' => $recipients,
				'folderattach' => $folderattach,
				'fileattach' => $fileattach
			);		
			//print_r($p);
			$emz->insert($p);
			//die;
			
			$success = $imap->SetMailFlag($email,'Deleted',1);
		    if ($success != true) {
		        print $imap->lastErrorText() . "\n";
		        exit;
		    }
		}

		$success = $imap->ExpungeAndClose();
		if ($success != true) {
		    print $imap->lastErrorText() . "\n";
		
		    exit;
		}
		
		$imap->Disconnect();
	}
}
