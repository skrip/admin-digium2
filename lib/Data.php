<?php

class Data 
{
	public static function getTotalUser() {
		$db = Db::init();
		$col = $db->users_digiumm;
		$mcol = $col->count();
		
		return $mcol;
	}
	
	public static function getUserPerMinggu() {
		$db = Db::init();
		$col = $db->users_digiumm;
		
		$start = time();
		$end = strtotime('-1 weeks', $start);
		
		$q = array(
			'time_created' => array('$gt' => $end, '$lt' => $start)
		);
		
		$mcol = $col->count($q);
		
		return $mcol;
	}
	
	public static function getTotalContent() {
		$db = Db::init();
		$col = $db->contents;
		
		$mcol = $col->count();
		
		return $mcol;
	}
	
	public static function getContentPerMinggu() {
		$db = Db::init();
		$col = $db->contents;
		
		$start = time();
		$end = strtotime('-1 weeks', $start);
		
		$q = array(
			'publish' => 'yes',
			'time_created' => array('$gt' => $end, '$lt' => $start)
		);
		
		$mcol = $col->count($q);
		
		return $mcol;
	}
}
