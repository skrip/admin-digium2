<?php

class upload
{
	private $ftp;
	private $hostname;
	private $username;
	private $password;
	
	public function __construct()
    {
    	$this->hostname = "192.168.89.40";
		$this->username = "digiumm";
		$this->password = "d1g1umm";
        $this->init();
    }
	
	private function rubahfolder($folder)
	{
		$dirExists = $this->ftp->ChangeRemoteDir($folder);
		if ($dirExists == true) {
			
		}
		else {
			$success = $this->ftp->CreateRemoteDir($folder);
			if ($success != true) {
			    print $this->ftp->lastErrorText() . "\n";
			    exit;
			}
			$dirExists = $this->ftp->ChangeRemoteDir($folder);
		}
	}
	
	public function delete($namafilesaja, $folder)
	{
		$this->rubahfolder('/admin');
		$this->rubahfolder('/admin/'.$folder);
		
		$localFilename = $namafilepath;
		$remoteFilename = $namafilesaja;
		
		$fileSize = $this->ftp->GetSizeByName($namafilesaja);
		if ($fileSize < 0) {
		    //print 'file does not exist' . "\n";
		}
		else {
		    //print 'file exists and is ' . $fileSize . ' bytes in size' . "\n";
			
			$success = $this->ftp->DeleteRemoteFile($namafilesaja);
			if ($success != true) {
			    print $this->ftp->lastErrorText() . "\n";
			    exit;
			}
		}
	}
	
	public function upload($namafilepath, $namafilesaja, $folder)
	{
		$this->rubahfolder('/admin');
		$this->rubahfolder('/admin/'.$folder);
		
		$localFilename = $namafilepath;
		$remoteFilename = $namafilesaja;
		
		$success = $this->ftp->PutFile($localFilename, $remoteFilename);
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
	}
	
	private function init()
	{
		$this->ftp = new CkFtp2();

		//  Any string unlocks the component for the 1st 30-days.
		$success = $this->ftp->UnlockComponent('WILLAWFTP_vLuYpQZk9JnJ');
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
		$this->ftp->put_Hostname($this->hostname);
		$this->ftp->put_Username($this->username);
		$this->ftp->put_Password($this->password);
		
		//  Connect and login to the FTP server.
		$success = $this->ftp->Connect();
		if ($success != true) {
		    print $this->ftp->lastErrorText() . "\n";
		    exit;
		}
	}
	
	function __destruct() {
       $this->ftp->Disconnect();
   	}
}
