<?php

final class Base
{
	private $controller = "";
	
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new Base();
        }
        return $inst;
    }

    private function __construct()
    {

    }
	
	public function setControllerName($cont)
	{
		$this->controller = $cont;
	}
	
	public function getControllerName()
	{
		return $this->controller;
	}
}