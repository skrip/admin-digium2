<?php
date_default_timezone_set("Asia/Jakarta");
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('DOCVIEW', DOCROOT."view".DIRECTORY_SEPARATOR); 
define('IMAGE_URL', 'http://cdn.digiumm.com/admin/'); 
define('SSO_URL', 'https://sso.dboxid.com/user/login?client_id=53cc8e7d7f2f4fc3b38b4672&appId=digiumm');
define('APP_ID', 'dev.digiumm');
define('BASE_URL', 'http://admin-digiumm.com/');
define('ATTACHMENTS_DIR',  '/var/www/attachment');

session_name('digiumm_session');
session_set_cookie_params(0, '/', 'admin.digiumm.com');
session_start();

include_once(DOCROOT."lib/chilkat/chilkat_9_5_0.php");

require_once DOCROOT.'lib/UserAgentParser.php';



function __autoload($class_name) {
	$s = explode("_", $class_name);
	if(count($s) == 1)
	{
		if(file_exists(DOCROOT."/classes/".$class_name . '.php'))
		{
	    	include DOCROOT."/classes/".$class_name . '.php';
		}
		else if(file_exists(DOCROOT."/lib/".$class_name . '.php'))
		{
			include DOCROOT."/lib/".$class_name . '.php';
		}
	}
	else {
		if(file_exists(DOCROOT."/classes/".$s[1]."/".$s[0] . '.php'))
		{
			include DOCROOT."/classes/".$s[1]."/".$s[0] . '.php';
		}
	}
	
}

header('Expires: Tue, 20 Oct 1981 05:00:00 GMT'); 
header('Cache-Control: no-store, no-cache, must-revalidate'); 
header('Pragma: no-cache');
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if($path == "/")
{
	//header( 'Location: '.BASE_URL.'user/login/?client_id=5379de237f2f4f275e8b4567' ) ;
	Base::Instance()->setControllerName('welcome');
	$r = new welcome_controller();
	$r->index();
}
else {
	$adacontent = false;
	$pp = explode("/", $path);
	if(count($pp) > 2)
	{
		if (class_exists($pp[1].'_controller')) {
			$rr = $pp[1].'_controller';
			Base::Instance()->setControllerName($pp[1]);
			$r = new $rr();
			if(method_exists($r, $pp[2]))
			{
				$r->$pp[2]();
				$adacontent = true;
			}
			/*else {
				if($rr == 'watch_controller')
				{
					if(isset($pp[2]))
					{
						$r->watch($pp[2]);
						$adacontent = true;
					}
				}
			}*/
		}
	}
	if(!$adacontent)
	{
		$r = new error_controller();
		$r->err404();
	}
}		
