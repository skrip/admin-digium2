<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Slide Show</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <!--div class="big-icons-buttons pull-right" ><a href="/slideshow/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div-->	
          <h1>Slide Show<small> list Slide Show</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th width="90%" colspan="3">Slide Show</th>
                      <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = 1;
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						$ff = helper::getlinkImageResize($dt['image'], 'image_content', "f50x50");
						echo '<td width="7%"><img src="'.$ff.'"></td>';
                      	echo '<td><h5>'.$dt['title'].'</h5></td>';
                      	echo '<td class="text-center">';
                      	//echo '<a href="/content/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/slideshow/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="3">Slide Show</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <?php echo Modal::Show("Delete Konten Slide Show"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 