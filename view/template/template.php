<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="keywords" content="admin digiumm digitama">
<meta name="author" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>admin - digiumm</title>
<!--link href="/public/css/styles.css" rel="stylesheet" type="text/css">
<link href="/public/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="/public/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css"-->
<?php
	foreach($css as $datcss)
	{
		echo '<link rel="stylesheet" href="'.$datcss.'">';
	}
?>

<link rel="shortcut icon" type="image/x-icon" href="/public/favicon.ico" />
<script type="text/javascript" src="/public/js/vendors/modernizr/modernizr.custom.js"></script>

<?php
	foreach($js as $datjs)
	{
		echo '<script type="text/javascript" src="'.$datjs.'"></script>';
	}
?>
<script>
$('.powerwidget > header').on('touchstart', function(event){});
</script>
</head>

<body>

<!--Smooth Scroll-->
<div class="smooth-overflow">
<!--Navigation-->
  <?php echo $topmenu; ?>>
  
  <!--MainWrapper-->
  <div class="main-wrap"> 
    
    <!--OffCanvas Menu -->
    <aside class="user-menu"> 
      
      <!-- Tabs -->
      <div class="tabs-offcanvas">
        <ul class="nav nav-tabs nav-justified">
          <li class="active"><a href="#userbar-one" data-toggle="tab">Main</a></li>
          <li><a href="#userbar-three" data-toggle="tab">ToDo</a></li>
        </ul>
        <div class="tab-content"> 
          
          <!--User Primary Panel-->
          <?php
              $db = Db::init();
			  $eml = $db->emails;
			  $memail = $eml->findOne(array('email' => trim($_SESSION['email'])));
			  
			  $usr = $db->users;
			  $musr = $usr->findOne(array('_id' => new MongoId($memail['user_id'])));
			  
			  $role = $db->roles;
			  $mrole = $role->findOne(array('_id' => new MongoId($musr['role'])));
			  
			  $name = $_SESSION['name'];
			  if(strlen(trim($_SESSION['name'])) == 0)
			  	$name = $musr['name'];
			  
			  $url = "";
			  if(strlen(trim($musr['foto'])) > 0){
			  	$path_parts = pathinfo($musr['foto']);
				$f = $path_parts['filename'];
				$ext = $path_parts['extension'];
				$url = $f.".c300x300.".$ext;
				
			  }
              ?>
          <div class="tab-pane active" id="userbar-one">
            <div class="main-info">
            	<div class="user-img">
            	<?php
            	if(strlen(trim($url)) > 0)
            		echo '<img src="'.IMAGE_URL.'image_user/'.$url.'" alt="'.$name.' Picture" /></div>';
				else {
					echo '<img src="/public/images/user-male.png" alt="'.$name.' Picture" /></div>';
				}
            	?>
              
              	<h1><?=$name?> <small><?=$mrole['name']?></small></h1>
              
            </div>
            <div class="list-group"> 
            	<a href="/users/profile?id=<?php echo trim($musr['_id']); ?>" class="list-group-item"><i class="fa fa-user"></i>Profile</a> 
              	<a href="/contact/index" class="list-group-item"><i class="fa fa-comment"></i>Messages <!-- <span class="badge">12</span> --></a> 
              	<a href="/comment/index" class="list-group-item"><i class="fa fa-comments"></i> Comments <!--  <span class="badge">45</span> --></a>
            	<a href="/preference/index" class="list-group-item"><i class="fa fa-cog"></i>Settings</a> 
              	<a data-toggle="modal" href="/account/logout" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> 
              </div>
          </div>
          
          <!--User Tasks Panel-->
          <div class="tab-pane" id="userbar-three">
            <div class="nano"> 
              <!--Adding Some Scroll-->
              <div class="nano-content">
                <div class="small-todos">
                  <div class="input-group input-group-sm">
                    <input id="new-todo" placeholder="Add ToDo" type="text" class="form-control">
                    <span class="input-group-btn">
                    <button id="add-todo" class="btn btn-default" type="button"><i class="fa fa-plus-circle"></i></button>
                    </span> </div>
                  <section id="task-list">
                    <ul id="todo-list">
                    </ul>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- /tabs --> 
      
    </aside>
    <!-- /Offcanvas user menu--> 
    
    <!--Main Menu-->
    <div class="responsive-admin-menu">
      <div class="responsive-menu">digiumm
        <div class="menuicon"><i class="fa fa-angle-down"></i></div>
      </div>
      <ul id="menu">
      	<?php
      	if((Base::Instance()->getControllerName() == 'welcome') ||
		   (Base::Instance()->getControllerName() == 'preference') || 
		   (Base::Instance()->getControllerName() == 'ads') ||
		   (Base::Instance()->getControllerName() == 'contact') ||
		   (Base::Instance()->getControllerName() == 'mainmenu'))
		{
		   	echo '<li><a class="submenu active" href="/" title="Dashboard" data-id="dashboard-sub">';
		}
		else
		{
			echo '<li><a class="submenu" href="/" title="Dashboard" data-id="dashboard-sub">';	
		}
      	?>
        <i class="entypo-briefcase"></i><span> Dashboard</span></a>
        	<ul id="dashboard-sub">
        		<?php 
          		if(Base::Instance()->getControllerName() == 'preference') echo '<li><a class="active2"';
				else echo '<li><a ';
          		?>
            	 href="/preference/index" title="Preference"><i class="fa fa-cog"></i><span> Preference</span></a></li>
            	 <?php 
          		if(Base::Instance()->getControllerName() == 'ads') echo '<li><a class="active2"';
				else echo '<li><a ';
          		?>
            	 href="/ads/index" title="ads"><i class="fa fa-shopping-cart"></i><span> Ads Management</span></a></li>
            	<?php 
          		if(Base::Instance()->getControllerName() == 'contact') echo '<li><a class="active2"';
				else echo '<li><a ';
          		?> 
            	 href="/contact/index" title="Contact"><i class="fa fa-envelope"></i><span> Contact</span></a></li>
            	<?php 
          		if(Base::Instance()->getControllerName() == 'mainmenu') echo '<li><a class="active2"';
				else echo '<li><a ';
          		?>  
            	 href="/mainmenu/index" title="Main Menu"><i class="entypo-layout"></i><span> Main Menu</span></a></li>
          	</ul>
        </li>
        
        <?php
      	if((Base::Instance()->getControllerName() == 'category') ||
      	   (Base::Instance()->getControllerName() == 'tag') || 
		   (Base::Instance()->getControllerName() == 'content') || 
		   (Base::Instance()->getControllerName() == 'slideshow') ||
		   (Base::Instance()->getControllerName() == 'comment') ||
		   (Base::Instance()->getControllerName() == 'image') ||
		   (Base::Instance()->getControllerName() == 'kegiatan') || 
		   (Base::Instance()->getControllerName() == 'quote'))
		{
		   	echo '<li><a class="submenu active" href="#" title="Content" data-id="widgets-sub">';
		}
		else
		{
			echo '<li><a class="submenu" href="#" title="Content" data-id="widgets-sub">';	
		}
      	?>
        <i class="fa fa-archive"></i><span> Content</span></a>
          <ul id="widgets-sub">
          	<?php 
          		if(Base::Instance()->getControllerName() == 'category') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?>
             href="/category/index" title="Category"><i class="fa fa-folder-open"></i><span> Category</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'tag') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/tag/index" title="Tags"><i class="fa fa-tags"></i><span> Tags</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'content') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/content/index" title="Content"><i class="entypo-newspaper"></i><span> Content</span></a></li>
             <?php 
          		if(Base::Instance()->getControllerName() == 'quote') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/quote/index" title="Content"><i class="fa fa-quote-right"></i><span> Quote</span></a></li>
             <?php
             	if(Base::Instance()->getControllerName() == 'kegiatan') echo'<li><a class="active2"';
				else echo '<li><a ';
             ?>
             href="/kegiatan/index" title="Kegiatan"><i class="entypo-newspaper"></i><span> Kegiatan</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'comment') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/comment/index" title="Comment"><i class="fa fa-comments"></i><span> Comment</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'slideshow') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?>  
             href="/slideshow/index" title="Slide Show"><i class="fa fa-caret-square-o-right"></i><span> Slide Show</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'image') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?>  
             href="/image/index" title="Image"><i class="fa fa-file-image-o"></i><span> Image</span></a></li>
          </ul>
        </li>
        
        <?php
      	if((Base::Instance()->getControllerName() == 'users') ||
		   (Base::Instance()->getControllerName() == 'role') )
		{
		   	echo '<li><a class="submenu active" href="#" title="Users" data-id="users-sub">';
		}
		else
		{
			echo '<li><a class="submenu" href="#" title="Users" data-id="users-sub">';	
		}
      	?>
        <i class="fa fa-lock"></i><span> Security</span></a>
          <ul id="users-sub">
          	<?php 
          		if(Base::Instance()->getControllerName() == 'users') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/users/index" title="Users"><i class="fa fa-user"></i><span> Users</span></a></li>
             <?php 
          		if(Base::Instance()->getControllerName() == 'role') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/role/index" title="Role"><i class="fa fa-sitemap"></i><span> Role</span></a></li>
          </ul>
        </li>
        
        <?php
      	if((Base::Instance()->getControllerName() == 'forum') ||
		   (Base::Instance()->getControllerName() == 'forumcategory') )
		{
		   	echo '<li><a class="submenu active" href="#" title="Forum" data-id="forum-sub">';
		}
		else
		{
			echo '<li><a class="submenu" href="#" title="Forum" data-id="forum-sub">';	
		}
      	?>
        <i class="fa fa-lock"></i><span> Forum</span></a>
          <ul id="forum-sub">
          	<?php 
          		if(Base::Instance()->getControllerName() == 'forum') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/forum/index" title="Forum"><i class="fa fa-user"></i><span> Forum</span></a></li>
             <?php 
          		if(Base::Instance()->getControllerName() == 'forumcategory') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/forumcategory/index" title="Forum Kategori"><i class="fa fa-sitemap"></i><span> Forum Kategori</span></a></li>
          </ul>
        </li>
        
        <?php
      	if((Base::Instance()->getControllerName() == 'inbox') ||
		   (Base::Instance()->getControllerName() == 'mycontact') )
		{
		   	echo '<li><a class="submenu active" href="#" title="Inbox" data-id="message-sub">';
		}
		else
		{
			echo '<li><a class="submenu" href="#" title="Inbox" data-id="message-sub">';	
		}
      	?>
        <i class="fa fa-lock"></i><span> Message</span></a>
          <ul id="message-sub">
          	<?php 
          		if(Base::Instance()->getControllerName() == 'inbox') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/inbox/index" title="Forum"><i class="fa fa-user"></i><span> Inbox</span></a></li>
            <?php 
          		if(Base::Instance()->getControllerName() == 'mycontact') echo '<li><a class="active2"';
				else echo '<li><a ';
          	?> 
             href="/mycontact/index" title="Forum"><i class="fa fa-user"></i><span> My Contact</span></a></li> 
             
          </ul>
        </li>
        
      </ul>
    </div>
    <!--/MainMenu-->
    
    <?php echo $content; ?>
    
    </div>
    <!--/MainWrapper-->
</div>
<!--/Smooth Scroll--> 
<!--Modals--> 

<!--Power Widgets Modal-->
<div class="modal" id="delete-widget">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">
        <p>Are you sure to delete this widget?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
        <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="yesigo">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Lock Screen Dialog Modal-->
<div class="modal" id="lockscreen">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="yesilock">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<!--Scripts--> 
<!--JQuery--> 

<!--/Scripts-->

</body>
</html>
