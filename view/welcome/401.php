<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="keywords" content="admin template, admin dashboard, inbox templte, calendar template, form validation">
<meta name="author" content="DazeinCreative">
<meta name="description" content="ORB - Powerfull and Massive Admin Dashboard Template with tonns of useful features">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>digiumm | 401 Error</title>
<link href="/public/css/styles.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="/public/favicon.ico" />
<script type="text/javascript" src="/public/js/vendors/modernizr/modernizr.custom.js"></script>
</head>

<body>
<div class="standalone-page-wrapper"> 
  
  <!--Top Block-->
  <div class="error-top-block">
    <div class="error-top-block-image"> <img src="/public/images/error-robot-2.png" alt="Ooops!" /> </div>
  </div>
  <!--/Top Block--> 
  <!--Bottom Block-->
  <div class="error-bottom-block">
    <div class="col-md-6 col-md-offset-3 error-description">
      <div class="error-code">401 Error</div>
      <div class="error-meaning">Unauthorized</div>
      <div class="todo">
        <h4>It is not you. It is us. We are sorry.</h4>
        We are experiencing internal server error &#8212; something went terribly wrong. You can try refreshing the page, the problem may be temporary. </div>
      <div class="copyrights"> digiumm Admin Dashboard <br>
        Created by <a href="http://digitamagroup.com">DigitamaGroup</a> &copy; 2014 </div>
    </div>
  </div>
  <!--/Bottom Block--> 
</div>
</body>
</html>