<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Kegiatan</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <div class="big-icons-buttons pull-right" ><a href="/kegiatan/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>	
          <h1>Kegiatan<small> list Kegiatan</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="4">Kegiatan
                        <form action="/kegiatan/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:10px 3px;">Search</button>
                      	</form>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = $idx;
					$db = Db::init();
					$usr = $db->users;
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						
						$created_by = 'admin';
						if(isset($dt['created_by']))
						{
							$mcontent = $usr->findone(array('_id' => new MongoId($_SESSION['userid'])));
							$created_by = $mcontent['name'];
						}
						
						$path_parts = pathinfo($dt['image']);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".f50x50.".$ext;
						echo '<td width="7%"><img src="'.IMAGE_URL.'image_content/'.$url.'"></td>';
                      	echo '<td><h5>'.$dt['title'].' ( '.$created_by.' - '.date('d-m-Y H:i:s )', $dt['time_created']).'</h5>';
						
                        echo '<small>'.helper::limitString($dt['description'], 200).'</small></td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/kegiatan/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/kegiatan/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="3">Kegiatan</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Kegiatan"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 