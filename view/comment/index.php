<script type="text/javascript">
	$(document).ready(function(){
		$('.view-comment').click(function(){
			var id = $(this).attr('data-id');
			$('.comment-slide').slideUp(200, function(){
				$('#' + id).slideDown("fast");
			})
			return false;
		});
		
		$('.view-reply').click(function(){
			var ids = $(this).attr('data-id');
			$('.reply-slide').slideUp(200, function(){
				$('.reply-slide-' + ids).slideDown("fast");
			})
			return false;
		});
	});
	
</script>
<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/content/index">Content</a></li>
            <li class="active">Content Comment</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<!--div class="big-icons-buttons pull-right" ><a href="/category/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div-->
          <h1>Comment<small> list Comment</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
              	<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="5">View Comment by Content
                        <form action="/content/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:3px 10px;">Search</button>
                      	</form>
                      </th>
                    </tr>
                  </thead>
                  <form action="/comment/delete" class="orb-form" method="post">
                  <tbody>
                    <tr>
                      <td width="100%" colspan="5">
                      	<script type="text/javascript">
							$(document).ready(function(){
								$("#checkall").click(function(){
								    $(".checkbox").not(this).prop("checked", this.checked);
								});
							});
							
						</script>
						<input type="checkbox" name="checkall" id="checkall" style="vertical-align:middle"> &nbsp; 
						<button type="submit" class="btn btn-default" style="padding:3px 10px; vertical-align:middle">Delete Checked Comment</button>
                      </td
                    </tr>
                  </tbody>
                  <tbody>
                  	<?php
                  	$db = Db::init();
					$no = 0;
					foreach($data as $val)
					{
						$rcounter = 0;
						if (isset($val['reply']))
							$rcounter = count($val['reply']);
						$imagecode = 'http://www.gravatar.com/avatar.php?gravatar_id='.md5(strtolower($val['email']));
						echo '<tr>';
						echo '<td width="3%" style="font-size:13px"><input class="checkbox" type="checkbox" name="delcomment[]" value="'.$val['_id'] .'" /></td>';
						echo '<td width="70%" style="font-size:13px"><b>'. $val['author'] .'</b></br> '. $val['email'] .' | '. $val['url'] .' </br></br>'. strip_tags($val['comment']) .'</td>';
						echo '<td width="10%" style="font-size:13px"> '. date("d/m/Y", substr($val['time_created'],0,10)) .'</td>';
						echo '<td width="5%"><a href="#" ref="/comment/delete?id='.trim($val['_id']).'&comment=yes" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a></td>';
						echo '<td width="15%" class="text-center">
						<a href="#" class="view-reply" data-id="'.$val['_id'].$no.'" style="font-size:13px; color:#f39815">View Reply ( '. $rcounter .' )</a>';
						echo '</td></tr>';
						
						
						echo '<tr class="reply-slide reply-slide-'.$val['_id'].$no.'" id="'.$val['_id'].$no.'" style="display:none">';
						echo '<td style="font-size:13px; width:80px;"></td>';
						echo '<td colspan="7" width="95%">';
						if ($rcounter > 0){
							
							echo '<table class="table table-striped table-bordered table-hover">
	                  				<tbody>';
							foreach ($val['reply'] as $v) {
								$imagecode = 'http://www.gravatar.com/avatar.php?gravatar_id='.md5(strtolower($v['email']));
								echo '<tr>';
								echo '<td style="font-size:13px"><b>'. $v['author'] .'</b></br> '. $v['email'] .' | '. $v['url'] .'</br></br>'. strip_tags($v['comment']) .'</td>';
								echo '<td style="font-size:13px"> '. date("d/m/Y", substr($v['time_created'],0,10)) .'</td>';
								echo '<td><a href="#" ref="/comment/delete?id='.trim($v['_id']).'&reply=yes" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a></td>';
								echo '</tr>';
								
							}
							echo '</tbody>
	                  				</table>';
						}else{
							echo "This post has no reply";
						}
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  	</form>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th colspan="5">Comment</th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Comment"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 