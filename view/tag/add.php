<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Tags</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Tags<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post" enctype='multipart/form-data'>
                  <fieldset>
                    
                    <section>
                    	<label class="label">Name</label>
                      <?php
                    	if(isset($error['title']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="name" value="<?php echo $name; ?>">
                      </label>
                      <?php
	                    if(isset($error['name']))
	                    {
	                    	foreach($error['name'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                   <section>
	                    <label class="label">Status</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
	                    	<?php
	                    		if(!empty($status))
								{
									if(trim($status) == 'yes')
										echo '<input type="checkbox" name="status" checked>';
									else
										echo '<input type="checkbox" name="status" >';
								}
								else
									echo '<input type="checkbox" name="status" checked>';
	                    	?>
	                    	<i></i>Active</label>		                    			                    
	                    </div>
                    </section>					
                      
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 