<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/content/index">Content</a></li>
            <li class="active">Category</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Content Category<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post">
                  <fieldset>
                  	<section class="col-4">
		                <label class="label">Parent</label>
		                <label class="select">
		                    <select name="parent">
		                      	<?php
		                      		$db = Db::init();
									$cat = $db->categories;
									
									$p = array('parent' => '0');
									$col = $cat->find($p);
									echo '<option value="0">-- Pilih Kategori --</option>';
									
									foreach($col as $dd)
									{
										if(trim($dd['_id']) == $parent)
											echo '<option selected value="'.trim($dd['_id']).'">&nbsp;&nbsp;-&nbsp;'.trim($dd['name']).'</option>';
										else
											echo '<option value="'.trim($dd['_id']).'">&nbsp;&nbsp;-&nbsp;'.trim($dd['name']).'</option>';
											
										$p = array('parent' => trim($dd['_id']));
										$colcol = $cat->find($p);
										
										foreach($colcol as $ddcol)
										{
											if(trim($ddcol['_id']) == $parent)
												echo '<option selected value="'.trim($ddcol['_id']).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.trim($ddcol['name']).'</option>';
											else
												echo '<option value="'.trim($ddcol['_id']).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'.trim($ddcol['name']).'</option>';
											
											$p = array('parent' => trim($ddcol['_id']));
											$colcol2 = $cat->find($p);
											foreach($colcol2 as $ddcol2)
											{
												if(trim($ddcol2['_id']) == $parent)
													echo '<option selected value="'.trim($ddcol2['_id']).'">'.trim($ddcol2['name']).'</option>';
												else
													echo '<option value="'.trim($ddcol2['_id']).'">'.trim($ddcol2['name']).'</option>';
											}
										}
									}
		                      	?>
		                    </select>
		                <i></i> </label>
                    </section>
                    
                    <section class="col-4">
                    	<label class="label">Menu</label>
                    		<label class="select">
                    			<select name="menu">
                    				<?php
                    					$m='<option value="Tafaqquh">Tafaqquh</option>';
                    					if ($menu=="Tafaqquh")
											$m='<option value="Tafaqquh" selected="selected">Tafaqquh</option>';
										$mm .= $m;
										
										$m='<option value="Kegiatan">Kegiatan</option>';
                    					if ($menu=="Kegiatan")
											$m='<option value="Kegiatan" selected="selected">Kegiatan</option>';
										$mm .= $m;
										
										$m='<option value="Rehat">Rehat</option>';
                    					if ($menu=="Rehat")
											$m='<option value="Rehat" selected="selected">Rehat</option>';
										$mm .= $m;
										
										$m='<option value="Profil">Profil</option>';
                    					if ($menu=="Profil")
											$m='<option value="Profil" selected="selected">Profil</option>';
										$mm .= $m;
										
										$m='<option value="Konsultasi">Konsultasi</option>';
                    					if ($menu=="Konsultasi")
											$m='<option value="Konsultasi" selected="selected">Konsultasi</option>';
										$mm .= $m;
										
										$m='<option value="Adab">Adab</option>';
                    					if ($menu=="Adab")
											$m='<option value="Adab" selected="selected">Adab</option>';
										$mm .= $m;
										
										$m='<option value="Ramadhan">Ramadhan</option>';
                    					if ($menu=="Ramadhan")
											$m='<option value="Ramadhan" selected="selected">Ramadhan</option>';
										$mm .= $m;
										
										$m='<option value="Keluarga">Keluarga</option>';
                    					if ($menu=="Keluarga")
											$m='<option value="Keluarga" selected="selected">Keluarga</option>';
										$mm .= $m;
										
										$m='<option value="Perempuan">Perempuan</option>';
                    					if ($menu=="Perempuan")
											$m='<option value="Perempuan" selected="selected">Perempuan</option>';
										$mm .= $m;
										
										$m='<option value="Oase">Oase</option>';
                    					if ($menu=="Oase")
											$m='<option value="Oase" selected="selected">Oase</option>';
										$mm .= $m;
										
										$m='<option value="Kutipan">Kutipan</option>';
                    					if ($menu=="Kutipan")
											$m='<option value="Kutipan" selected="selected">Kutipan</option>';
										$mm .= $m;
										
										echo $mm;
                    				?>
                    				
                    			</select>
                    		<i></i></label>
                    </section>
                    
                    <section class="col-4">
                    	<label class="label">Category Name</label>
                      <?php
                    	if(isset($error['name']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="name" value="<?php echo $name; ?>">
                      </label>
                      <?php
	                    if(isset($error['name']))
	                    {
	                    	foreach($error['name'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                     <section class="col-2">
                    	<label class="label">Nomor Urut</label>
                      <?php
                    	if(isset($error['nomor']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="nomor" value="<?php echo $nomor; ?>">
                      </label>
                      <?php
	                    if(isset($error['nomor']))
	                    {
	                    	foreach($error['nomor'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section class="col-2">
                    	<label class="label">Color</label>
                      <?php
                    	if(isset($error['color']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="color" name="color" value="<?php echo $color; ?>">
                      </label>
                      <?php
	                    if(isset($error['color']))
	                    {
	                    	foreach($error['color'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">description</label>
                      <?php
                    	if(isset($error['description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" name="description"><?php echo $description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['description']))
	                    {
	                    	foreach($error['description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
	                    <label class="label">Status</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
		                    	<?php
		                    		if(!empty($status))
									{
										if(trim($status) == 'active')
											echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
										else
											echo '<input type="checkbox" name="status" > <i></i>Active</label>';
									}
									else
										echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
		                    	?>		                    			                    
	                    </div>
                    </section>
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 