<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/content/index">Content</a></li>
            <li class="active">Content Category</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/category/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>
          <h1>Category<small> list Content Category</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="6">Category
						<form action="/category/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:3px 10px;">Search</button>
                      	</form>
					  </th>
                    </tr>
                    <tr>
                    	<th>No.</th>
                    	<th>Category</th>
                    	<th>Menu</th>
                    	<th>Color</th>
                    	<th>No Urut</th>
                    	<th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$db = Db::init();
					$cat = $db->categories;
					
                  	$no = $idx;					
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						if(trim($dt['parent']) != '0')
						{
							$col = $cat->findOne(array("_id" => new MongoId($dt['parent'])));
							echo '<td><h5>'.$col['name'].' => '.$dt['name'].'</h5>';
						}
						else
                      		echo '<td width="59%"><h5>'.$dt['name'].'</h5>';
                      		
                        echo '<small>'.$dt['description'].'</small></td>';
						echo '<td width="20%"><h5>'.$dt['menu'].'</h5></td>';
						$color = '';
						if (isset($dt['color']))
							$color = $dt['color'];
						
						echo '<td width="20%" style="background:'.$color.'">'.$color.'</td>';
						echo '<td width="10%"><h5>'.$dt['nomor'].'</h5></td>';
                      	echo '<td class="text-center" width="10%">';
                      	echo '<a href="/category/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/category/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>                  	
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="100%" colspan="6">Category</th>
                      
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Kategori"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 