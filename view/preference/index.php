<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/">Dashboard</a></li>
            <li class="active">Preference</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Preference<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="/preference/index" class="orb-form" method="post" enctype='multipart/form-data'>
                  <fieldset>
                    <section>
                    	<label class="label">facebook page</label>
                      <?php
                    	if(isset($error['facebook']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="facebook" value="<?php echo $facebook; ?>">
                      </label>
                      <?php
	                    if(isset($error['facebook']))
	                    {
	                    	foreach($error['facebook'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">twitter page</label>
                      <?php
                    	if(isset($error['twitter']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="twitter" value="<?php echo $twitter; ?>">
                      </label>
                      <?php
	                    if(isset($error['twitter']))
	                    {
	                    	foreach($error['twitter'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">google+ page</label>
                      <?php
                    	if(isset($error['google']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="google" value="<?php echo $google; ?>">
                      </label>
                      <?php
	                    if(isset($error['google']))
	                    {
	                    	foreach($error['google'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>

                    <section>
						<label class="label">Motto</label>                    	
                    	<?php
                    		if (isset($error['motto'])) 
								echo '<label class="input state-error">';
							else 
								echo '<label class="input">';
                    	?>
                    	<input type="text" name="motto" value="<?php echo $motto; ?>" />
                    	</label>
                    	<?php
                    		if (isset($error['motto'])) {
								foreach ($error['motto'] as $err) {
									echo '<div class="note note-error">'.$err.'</div>';
								}
							}
                    	?>
                    </section>

                    <section>
                      <label class="label">description</label>
                      <?php
                    	if(isset($error['description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" id="description" name="description"><?php echo $description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['description']))
	                    {
	                    	foreach($error['description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                  </fieldset>
                  
                  <fieldset>
                  	<legend>video & quote</legend>
                  	<section>
                  		<label class="label">video url</label>
                  		<?php
                  			if (isset($error['video_url'])) 
								echo '<label class="input state-error">';
							else 
								echo '<label class="input">';
                  		?>
                  		<input type="text" name="video_url" value="<?php echo $video_url; ?>">
                  		</label>
                  		<?php
	                    if(isset($error['video_url']))
	                    {
	                    	foreach($error['video_url'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                  	</section>
                  	
                  	<section>
                  		<label class="label">quote</label>
                  		<?php
                  			if(isset($error['quote']))
								echo '<label class="textarea textarea-resizable state-error">';
							else 
								echo '<label class="textarea textarea-resizable">';
                  		?>	
                  		<textarea rows="3" name="quote"><?php echo $quote; ?></textarea>
                  		</label>
                  		<?php
                  			 if(isset($error['quote']))
	                    {
	                    	foreach($error['quote'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
                  		?>
                  	</section>
                  </fieldset>
	
				  <fieldset>
				  	<legend>meta description</legend>
				  	<section>
                      <label class="label">description</label>
                      <?php
                    	if(isset($error['meta_description']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="meta_description" value="<?php echo $meta_description; ?>">
                      </label>
                      <?php
	                    if(isset($error['meta_description']))
	                    {
	                    	foreach($error['meta_description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">author</label>
                      <?php
                    	if(isset($error['meta_author']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="meta_author" value="<?php echo $meta_author; ?>">
                      </label>
                      <?php
	                    if(isset($error['meta_author']))
	                    {
	                    	foreach($error['meta_author'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">keywords</label>
                      <?php
                    	if(isset($error['meta_keywords']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="meta_keywords" value="<?php echo $meta_keywords; ?>">
                      </label>
                      <?php
	                    if(isset($error['meta_keywords']))
	                    {
	                    	foreach($error['meta_keywords'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
				  </fieldset>
				  
				  <fieldset>
				  	<legend>Contact Info</legend>
				  	<section>
                      <label class="label">address</label>
                      <?php
                    	if(isset($error['contact_address']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="contact_address" value="<?php echo $contact_address; ?>">
                      </label>
                      <?php
	                    if(isset($error['contact_address']))
	                    {
	                    	foreach($error['contact_address'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">email</label>
                      <?php
                    	if(isset($error['contact_email']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="contact_email" value="<?php echo $contact_email; ?>">
                      </label>
                      <?php
	                    if(isset($error['contact_email']))
	                    {
	                    	foreach($error['contact_email'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    <section>
                      <label class="label">phone</label>
                      <?php
                    	if(isset($error['contact_phone']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="contact_phone" value="<?php echo $contact_phone; ?>">
                      </label>
                      <?php
	                    if(isset($error['contact_phone']))
	                    {
	                    	foreach($error['contact_phone'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">latitude</label>
                      <?php
                    	if(isset($error['contact_lat']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="contact_lat" value="<?php echo $contact_lat; ?>">
                      </label>
                      <?php
	                    if(isset($error['contact_lat']))
	                    {
	                    	foreach($error['contact_lat'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">Longitude</label>
                      <?php
                    	if(isset($error['contact_long']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="contact_long" value="<?php echo $contact_long; ?>">
                      </label>
                      <?php
	                    if(isset($error['contact_long']))
	                    {
	                    	foreach($error['contact_long'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">description</label>
                      <?php
                    	if(isset($error['contact_description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" name="contact_description"><?php echo $contact_description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['contact_description']))
	                    {
	                    	foreach($error['contact_description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
				  </fieldset>	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 