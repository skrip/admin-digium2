<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/">Dashboard</a></li>
            <li class="active">Ads Management</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Content Ads<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post">
                  <fieldset>
                  	
                    
                    <section>
                    	<label class="label">Menu</label>
                    		<label class="select">
                    			<select name="menu">
       								<option value="Home">Home</option>
       								<option value="Artikel">Artikel</option>
                    				<option value="Tafaqquh">Tafaqquh</option>
                    				<option value="Kegiatan">Kegiatan</option>
                    				<option value="Rehat">Rehat</option>
                    				<option value="Profil">Profil</option>
                    			</select>
                    		<i></i></label>
                    </section>
                    
                    <section>
                    	<label class="label">Ads Name</label>
                      <?php
                    	if(isset($error['title']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="title" value="<?php echo $title; ?>">
                      </label>
                      <?php
	                    if(isset($error['title']))
	                    {
	                    	foreach($error['title'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">description</label>
                      <?php
                    	if(isset($error['description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" name="description"><?php echo $description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['description']))
	                    {
	                    	foreach($error['description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
	                    <label class="label">Publish</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
	                    	<?php
	                    		if(!empty($publish))
								{
									if(trim($publish) == 'yes')
										echo '<input type="checkbox" name="publish" checked>';
									else
										echo '<input type="checkbox" name="publish" >';
								}
								else
									echo '<input type="checkbox" name="publish" checked>';
	                    	?>
	                    	<i></i>Publish</label>		                    			                    
	                    </div>
                    </section>
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 