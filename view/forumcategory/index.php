<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/forumcategory/index">Forum Category</a></li>
            <li class="active">Forum Category</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/forumcategory/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>
          <h1>Forum Category<small> list Forum Category</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="3">Forum Category
						<form action="/forumcategory/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:3px 10px;">Search</button>
                      	</form>
					  </th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	//$db = Db::init();
					//$cat = $db->categories;
					$katserver = new kategoriserver();
                  	$no = $idx;					
                  	foreach($data as $dt)
					{
						$ppname = "";
						$parent = $dt['parent'];
						$par = $katserver->getAllParent($parent);
						foreach($par as $pp)
						{
							$ppname = " => " . $ppname;
							$ppname = $pp['name'] . $ppname;
						}
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						//if(trim($dt['parent']) != '0')
						//{
						//	$col = $cat->findOne(array("_id" => new MongoId($dt['parent'])));
						//	echo '<td><h5>'.$col['name'].' => '.$dt['name'].'</h5>';
						//}
						//else
                      	echo '<td>'.$ppname.'<b>'.$dt['name'].'</b>';
                      		
                        echo '<small>'.$dt['description'].'</small></td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/forumcategory/edit?id='.trim($dt['id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/forumcategory/delete?id='.trim($dt['id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>                  	
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="2">Category</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete forum Kategori"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 