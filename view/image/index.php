<script type="text/javascript">
	$(document).ready(function () {
		$('.copy_image_link').click(function (e) {
            e.preventDefault();
            $(this).select()
        });
	});
</script>
<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Image</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <div class="big-icons-buttons pull-right" ><a href="/image/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>	
          <h1>Image<small> list Image</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th width="90%" colspan="3">Image</th>
                      <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = 1;
					$db = Db::init();
					$usr = $db->users;
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						
						$created_by = 'admin';
						if(isset($dt['created_by']))
						{
							$mcontent = $usr->findone(array('_id' => new MongoId($_SESSION['userid'])));
							$created_by = $mcontent['name'];
						}
						
						$path_parts = pathinfo($dt['image']);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".f50x50.".$ext;
						$url2 = $f.".".$ext;
						echo '<td width="7%"><img src="'.IMAGE_URL.'image_content/'.$url.'"></td>';
                      	echo '<td><h5>'.$dt['title'].'</h5>
                      		  	<textarea class="copy_image_link" style="width:100%; font-size:11px; height:20px; line-height:17px; padding:0px 2px;">'.IMAGE_URL.$url2.'</textarea>
                      		  </td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/image/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/image/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="3">Image</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <?php echo Modal::Show("Delete Image"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 