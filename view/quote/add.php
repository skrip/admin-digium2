<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Quote</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Quote<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post" enctype='multipart/form-data'>
                  <fieldset>
                    <section>
                      <label class="label">Kutipan (Arabic)</label>
                      <?php
                    	if(isset($error['quotearab']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" id="quotearab" name="quotearab"><?php echo $quotearab; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['quotearab']))
	                    {
	                    	foreach($error['quotearab'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">Kutipan (Latin)</label>
                      <?php
                    	if(isset($error['quotelatin']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" id="quotelatin" name="quotelatin"><?php echo $quotelatin; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['quotelatin']))
	                    {
	                    	foreach($error['quotelatin'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	<label class="label">Quote (Nama)</label>
                      <?php
                    	if(isset($error['quotename']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="quotename" value="<?php echo $quotename; ?>">
                      </label>
                      <?php
	                    if(isset($error['quotename']))
	                    {
	                    	foreach($error['quotename'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
              		<section>
	                    <label class="label">Publish</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
	                    	<?php
	                    		if(!empty($publish))
								{
									if(trim($publish) == 'yes')
										echo '<input type="checkbox" name="publish" checked>';
									else
										echo '<input type="checkbox" name="publish" >';
								}
								else
									echo '<input type="checkbox" name="publish" checked>';
	                    	?>
	                    	<i></i>Publish</label>		                    			                    
	                    </div>
                    </section>
                    
                    <section>
	                    <label class="label">Waktu Publish</label>
	                    <div class="inline-group">
	                    	
	                    	<?php
	                    		if (!isset($publish)) {
									if (trim($publish) == 'now') {
										echo '
										<label class="radio">
										<input type="radio" value="now" name="time_publish" checked="checked">
										<i></i>Now</label>
										<label class="radio">
										<input type="radio" value="later" name="time_publish">
										<i></i>Later</label>
										';
								}
									
									else {
										echo '
										<label class="radio">
										<input type="radio" value="now" name="time_publish">
										<i></i>Now</label>
										<label class="radio">
										<input type="radio" value="later" name="time_publish" checked="checked">
										<i></i>Later</label>
										';
									}
								}
								else {
									echo '
										<label class="radio">
										<input type="radio" value="now" name="time_publish" checked="checked">
										<i></i>Now</label>
										<label class="radio">
										<input type="radio" value="later" name="time_publish">
										<i></i>Later</label>
										';
								}
	                    	?>
	                    		                    			                    
	                    </div>
                    </section>
                    
                    
                    <section>
                    	<label class="label">Tanggal & Jam Publish</label>
                    </section>
                    <div class="row">	
                    <section class="col col-6">
	                    <label class="input"> 
	                    	<i class="icon-append fa fa-calendar"></i>
	                    	<?php
	                    		if (!empty($publish_date)) {
									echo '<input type="text" name="publish_date" id="date" value='.$publish_date.'>';
								} else {
									echo '<input type="text" name="publish_date" id="date" value="">';
								}
	                    	?>	
                       	</label>
	                </section>
	                </div>
	                
	                <div class="row"> 
                    <section class="col col-6">
                    	
                    	<?php
                    		if (!empty($publish_time)) {
								echo '<input type="text" name="publish_time" value='.$publish_time.'>';
							} else {
								echo '<input type="text" name="publish_time" value="">';
							}
                    	?>
                        
                    </section>
                    </div>  
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 