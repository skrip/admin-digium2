<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Quote</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <div class="big-icons-buttons pull-right" ><a href="/quote/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>	
          <h1>Quote<small> list Quote</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="3">Quote
                        <form action="/quote/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:10px 3px;">Search</button>
                      	</form>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = $idx;
					$db = Db::init();
					$usr = $db->users;
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						
						$created_by = 'admin';
						if(isset($dt['created_by']))
						{
							$mcontent = $usr->findone(array('_id' => new MongoId($_SESSION['userid'])));
							$created_by = $mcontent['name'];
						}
						
						
                      	echo '<td><h5>'.$dt['quotearab'].' - '.$dt['quotelatin'].'  ( '.$dt['quotename'].')</h5></td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/quote/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/quote/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="2">Quote</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Quote"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 