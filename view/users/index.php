<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Users</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/users/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>
          <h1>Users<small> list Users</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th width="90%" colspan="3">Users</th>
                      <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = 1;
                  	foreach($data as $dt)
					{
						echo '<tr>';
						echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						if((isset($dt['foto'])) && (strlen(trim($dt['foto'])) > 0))
						{
							$path_parts = pathinfo($dt['foto']);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".f50x50.".$ext;
							echo '<td width="7%"><img src="'.IMAGE_URL.'image_user/'.$url.'"></td>';
						}						
						else
							echo '<td width="7%"></td>';
							
                      	echo '<td><h5>'.$dt['name'].'</h5>';
						if(isset($dt['email']))
                        	echo '<small>'.$dt['email'].'</small>';
                    	echo '</td>';							
                      	echo '<td class="text-center">';
                      	echo '<a href="/users/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/users/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="3">Users</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <?php echo Modal::Show("Delete User"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 