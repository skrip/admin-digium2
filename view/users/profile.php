<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Users</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>User Profiles<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post" enctype='multipart/form-data'>
                  <fieldset>                  	                   	                    	                   	             
                    <section>
                    	<label class="label">Name</label>
                      <?php
                    	if(isset($error['name']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="name" value="<?php echo $name; ?>">
                      </label>
                      <?php
	                    if(isset($error['name']))
	                    {
	                    	foreach($error['name'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">Biografi</label>
                      <?php
                    	if(isset($error['bio']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" name="bio"><?php echo $bio; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['bio']))
	                    {
	                    	foreach($error['bio'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">Birthdate</label>
                      <label class="input"> <i class="icon-append fa fa-calendar"></i>
                      	<?php
                      	if (!empty($birthdate))
							echo '<input type="text" name="birthdate" id="date" value="'. date('d-m-Y', $birthdate) .'">';
						else
							echo '<input type="text" name="birthdate" id="date" value="">';
                      	?>
                      </label>
                    </section>
                    
                    <section>
                    	<label class="label">From</label>
	                    <label class="input">
                        <input type="text" name="from" value="<?php echo $from; ?>"></label>
                    </section>
                    
                    <section>
                      <label class="label">Foto</label>
                      <?php
	                    if(strlen(trim($foto)) > 0)
						{
				      ?>
                      <div class="col-md-3 col-sm-6 thumbnail">
                      	<?php
	                        $path_parts = pathinfo($foto);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".c300x300.".$ext;
                        ?>
                        <div class='hover-fader'><a href="#image-1"><img class="img-rounded img-responsive" src="<?php echo IMAGE_URL.'image_user/'.$url; ?>" alt="image01"><span class='zoom'><i class='fa fa-search-plus'></i></span></a></div>                        
                        <div class="gal-overlay" id="image-1"><img src="<?php echo IMAGE_URL.'image_user/'.$url; ?>" alt="image01" />
                          <a href="#die" class="gal-close"><i class="fa fa-times-circle"></i></a> </div>
                      </div>
                      <?php } ?>
                      
                      <div class="col-md-8">
                        <input id="filebutton" name="foto" class="input-file" type="file">
                      </div>
                      <!--div class="note note-error">File size must be less than 3Mb.</div-->
                    </section>
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 