<div class="content-wrapper"> 
		<?php echo $shorcut; ?>
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="index.html">Dashboard</a></li>
            <li class="active">Data</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Inbox<small>List Inbox</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          <div class="col-md-12 bootstrap-grid"> 
            
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="mailinbox" data-widget-editbutton="false">
              <header>
                <h2>Inbox<small>Mail Inbox</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	/*
                <div class="callout callout-warning">
                  <h4>JavaScript Required</h4>
                  Please note that Inbox Features such as marking read/unread, refresh stub, delete messages requires JavaScript to operate.
                  All required JS code can be found at main <code>scripts.js</code> file under Inbox Chapter. </div>
				 * */
                ?>
                <div class="mailinbox">
                  <div class="row">
                    <div class="col-md-1">
                      <div class="left-content">
                        <div class="list-group"> <a href="#" class="list-group-item active"><i class="entypo-inbox"></i><b>Inbox</b><span class="badge">32</span></a> <a href="/inbox/outbox" class="list-group-item"><i class="entypo-paper-plane"></i><b>Sent</b></a> <a href="#" class="list-group-item"><i class="entypo-doc-text"></i><b>Drafts</b></a> <a href="#" class="list-group-item"><i class="entypo-archive"></i><b>Archive</b></a> <a href="#" class="list-group-item"><i class="entypo-trash"></i><b>Trash</b></a> </div>
                      </div>
                    </div>
                    <div class="col-md-11">
                      <div class="right-content clearfix">
                        <div class="big-icons-buttons clearfix margin-bottom"> <a href='/inbox/compose' class="btn btn-sm btn-default"><i class="fa fa-envelope"></i>New Message</a>
                          <div class="btn-group btn-group-sm pull-right"> <a class="btn btn-default mark_read"><i class="fa fa-check-circle-o"></i>Read</a> <a class="btn btn-default mark_unread"><i class="fa fa-circle-o"></i>Unread</a> <a class="btn btn-default delete"><i class="fa fa-times-circle"></i> Delete</a> <a class="btn btn-default refresh"><i class="fa fa-refresh"></i> Refresh</a> </div>
                        </div>
                        <div class="input-group margin-bottom">
                          <input type="text" class="form-control" placeholder="Search Inbox">
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button">Search</button>
                          </span> </div>
                        <!-- /input-group -->
                        
                        <div class="table-relative table-responsive">
                          <table class="table table-condensed table-striped margin-0px">
                            <thead>
                              <tr>
                                <th><input id="all" type="checkbox" class="checkall" />
                                  <label for="all"></label></th>
                                <th colspan="2">Author</th>
                                <th>Message Header</th>
                                <th>Date</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            	foreach($data as $dat)
                            	{
                            		if($dat['status'] == 'UNREAD')
										echo '<tr class="unread">';
									else {
										echo '<tr>';
									}
									echo '<td><div class="user-image"><img alt="User" src="http://placehold.it/150x150"/>';
                                    echo '<input id="1" type="checkbox" class="checkbox" name="check-row" />';
                                    echo '<label for="1"></label>';
                                    echo '</div></td>'; 
									echo '<td class="star"><a class="fa fa-flag flagged-grey"></a></td>';
									echo '<td><a href="#">'.$dat['fromName'].'</a> <small><a href="#">Friends</a></small></td>';
                                	echo '<td><a href="#" class="pull-right"><i class="fa fa-paperclip"></i></a> <a href="#">'.$dat['subject'].'<small>'.helper::limitString($dat['body']).'</small></a></td>';
                                	echo '<td>'.$dat['localDateStr'].'<small><a href="#">Today <i class="fa fa-caret-right"></i></a></small></td>';
                            	}
                            ?>
                            </tbody>
                          </table>
                          
                         
                        </div>
                        <div class="margin-top">
                        <div class="padding-15px pull-left">
                        <small>Showing: 20 from 153</small></div>
                        
                        <?php echo $pagination;?>
                        
                        </div>
                        

                        
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- End Widget --> 
            
          </div>
          <!-- /Inner Row Col-md-12 --> 
        </div>
        <!-- /Widgets Row End Grid--> 
      </div>
      <!-- / Content Wrapper -->