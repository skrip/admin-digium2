<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/inbox/create">Create Email</a></li>
            <li class="active">Create Email</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Create Email<small> Create Email digitama domain</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post">
                  <fieldset>
                  	<section>
		                <label class="label">Forum Category</label>
		                <?php
	                    	if(isset($error['domain']))
	                    		echo '<label class="input state-error">';
							else
								echo '<label class="input">';
					  	?>
		                    <select name="domain">
		                      	<?php
		                      		echo '<option value="0">-- Pilih Domain digitama yang tersedia --</option>';
		                      		$sql = dbsql::init();								
									$users = $sql->get ("virtual_domains");
									if ($sql->count > 0)
									    foreach ($users as $user) { 
									       	if($domain == $user['name'])
												echo '<option value="'.$user['name'].'" selected>'.$user['name'].'</option>';
											else
												echo '<option value="'.$user['name'].'" >'.$user['name'].'</option>';
										}
		                      	?>
		                    </select>
		                <i></i> </label>
		                <?php
	                    if(isset($error['domain']))
	                    {
	                    	foreach($error['domain'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	<label class="label">Email Name</label>
                      <?php
                    	if(isset($error['email']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="email" value="<?php echo $email; ?>">
                      </label>
                      <?php
	                    if(isset($error['email']))
	                    {
	                    	foreach($error['email'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 