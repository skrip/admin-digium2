<!--Content Wrapper-->
      <div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/">Dashboard</a></li>
            <li class="active">Compose</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Inbox<small>With Prototyping</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          <div class="col-md-12 bootstrap-grid"> 
            
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="mailinbox" data-widget-editbutton="false">
              <header>
                <h2>Inbox<small>Mail Inbox</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="callout callout-warning">
                  <h4>JavaScript Required</h4>
                  Please note that Inbox Features such as marking read/unread, refresh stub, delete messages requires JavaScript to operate.
                  All required JS code can be found at main <code>scripts.js</code> file under Inbox Chapter. </div>
                
                <div class="inbox-new-message">
                  <div class="page-header">
                    <h3>New Message<small>Compose New</small></h3>
                  </div>
                  <form role="form" method="post" action="/inbox/compose">
                    <div class="row">
                      <div class="form-group col-md-4">
                        <input type="email" class="form-control" id="exampleInputEmail1" name='to' placeholder="email tujuan">
                      </div>
                      <div class="form-group col-md-4">
                        <input type="email" class="form-control" id="exampleInputEmail2" name='cc' placeholder="CC">
                      </div>
                      <div class="form-group col-md-4">
                        <input type="email" class="form-control" id="exampleInputEmail3" name='bcc' placeholder="BCC">
                      </div>
                      <div class="form-group col-md-12">
                        <input type="text" class="form-control" id="exampleInputPassword1" name='subject' placeholder="Topic of Message">
                      </div>
                      <div class="form-group col-md-12">
                        <textarea type="text" class="form-control" name='isi' placeholder="Topic of Message"></textarea>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-info">Send</button>
                    <button type="submit" class="btn btn-default">Save as Draft</button>
                  </form>
                </div>
              </div>
            </div>
            <!-- End Widget --> 
            
          </div>
          <!-- /Inner Row Col-md-12 --> 
        </div>
        <!-- /Widgets Row End Grid--> 
      </div>
      <!-- / Content Wrapper --> 