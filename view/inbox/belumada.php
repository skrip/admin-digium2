<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/inbox/index">Inbox</a></li>
            <li class="active">Inbox</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/outbox/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add Message</a></div>
          <h1>Inbox<small> list Inbox</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <div class="callout callout-warning">
                  <h4>Anda Belum mempunyai account email di digitama</h4>
                  <p>Silahkan untuk membuat terlebih dahulu !</p>
                  <a href="/inbox/create" class="btn btn-primary">Create email sekarang</a>
                </div>
              </div>
            </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 