<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Main Menu</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/mainmenu/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>
          <h1>Mainmenu<small> list Main Menu</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th width="90%" colspan="2">Mainmenu</th>
                      <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
					
                  	$no = 1;					
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						if(trim($dt['parent']) != '0')
						{
							$col = $cat->findOne(array("_id" => new MongoId($dt['parent'])));
							echo '<td><h5>'.$col['name'].' => '.$dt['name'].'</h5>';
						}
						else
                      		echo '<td><h5>'.$dt['name'].'</h5>';
                      		
                        echo '<small>'.$dt['description'].'</small></td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/category/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/category/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
					}
                  	?>                  	
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="2">Category</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <?php echo Modal::Show("Delete Kategori"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 