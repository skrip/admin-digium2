<script type="text/javascript">
	$(document).ready(function () {
		var counter = 1;
		$('#add_submenu').click(function (e) {
            e.preventDefault();
            $('#submenu_input_wrapper').append(
            	'<label class="input">&nbsp;</label><label class="label">Sub Menu '+ counter +'</label>'
            	+'<label class="input">'
            	+'	<input type="text" name="submenu[]" value="">'
				+'</label>'
            	+'<label class="label">Sub Menu Link '+ counter +'</label>'
            	+'<label class="input">'
            	+'	<input type="text" name="sublink[]" value="">'
				+'</label>'
            );
            counter = counter+1;
        });
	});
</script>
<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Main Menu</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Content Category<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $action; ?>" class="orb-form" method="post">
                  <fieldset>
                    <section>
                    	<label class="label">Main Menu</label>
                      <?php
                    	if(isset($error['title']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="title" value="<?php echo $title; ?>">
                      </label>
                      <?php
	                    if(isset($error['title']))
	                    {
	                    	foreach($error['title'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	<label class="label">Menu Link</label>
                      <?php
                    	if(isset($error['link']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="link" value="<?php echo $link; ?>">
                      </label>
                      <?php
	                    if(isset($error['link']))
	                    {
	                    	foreach($error['link'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	
                    	<span id="submenu_input_wrapper">
                    <?php
             
                    $no=0;
                    foreach($submenu as $nm)
                    {
                        echo '
                        	<label class="label">Sub Menu</label>
                        	<label class="input">
                        		<input type="text" name="submenu[]" value="'.$nm['submenu'].'">
							</label>';
                        $no++;
                    }
					$no=0;
                    foreach($sublink as $nm)
                    {
                        echo '
                        	<label class="label">Sub Menu Link</label>
                        	<label class="input">
                        		<input type="text" name="sublink[]" value="'.$nm['sublink'].'">
							</label>';
                        $no++;
                    }
					
					echo '</span><label class="input">&nbsp;</label>';
					echo '<label class="input"><button type="button" id="add_submenu">Add Submenu</button></label>';
                    
                    ?>
                    </section>
                    <section>
	                    <label class="label">Status</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
		                    	<?php
		                    		if(!empty($status))
									{
										if(trim($status) == 'active')
											echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
										else
											echo '<input type="checkbox" name="status" > <i></i>Active</label>';
									}
									else
										echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
		                    	?>		                    			                    
	                    </div>
                    </section>
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 