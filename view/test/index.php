<div class="content-wrapper">
	<?php echo $shorcut; ?>
	
	<div class="breadcrumb clearfix">
		<ul>
          <li><a href="/"><i class="fa fa-home"></i></a></li>
          <li class="active">Dashboard</li>
        </ul>
	</div>
	
	<!-- Widget Row Start grid -->
	  <div class="row" id="powerwidgets"> 
	    
	    
	    <div class="col-md-12 bootstrap-grid"> 
	      
	      <!-- New widget -->
	      
	      <div class="powerwidget cold-grey" id="serverstatus-index">
	        <header>
	          <h2></h2>
	        </header>
	        <div>
	          <div class="inner-spacer">
	          	<div class="page-header">
			    	<h2>Data<small>Summary</small></h2>
			    </div>	          	
            	<div class="row"> 
                  <!--Row-->                  
				    <ul class="tiny-stats">
	                <li class="col-md-3 col-sm-6">
	                  <div class="statistic clearfix">
	                    <!--div class="easy-pie-chart-container"><span class="chart" id="easy2" data-percent="<?php echo Data::getTotalUser(); ?>">  </span></div-->
	                    <span class="title">User Register (minggu ini)<small class="text-cold-grey"> Total User</small></span>
	                    <ul class="pull-right">
	                      <li><span class="label bg-cold-grey"><i class="fa fa-plus"></i> <?php echo Data::getUserPerMinggu(); ?></span></li>
	                      <li><span class="label bg-pink"> <?php echo Data::getTotalUser(); ?></span></li>
	                    </ul>
	                  </div>
	                </li>
	                <li class="col-md-3 col-sm-6">
	                  <div class="statistic clearfix">
	                    <!--div class="easy-pie-chart-container"><span class="chart" id="easy2" data-percent="<?php echo Data::getTotalUser(); ?>">  </span></div-->
	                    <span class="title">Artikel Terbaru (minggu ini)<small class="text-cold-grey"> Total Artikel</small></span>
	                    <ul class="pull-right">
	                      <li><span class="label bg-cold-grey"><i class="fa fa-plus"></i> <?php echo Data::getContentPerMinggu(); ?></span></li>
	                      <li><span class="label bg-dark-red"> <?php echo Data::getTotalContent(); ?></span></li>
	                    </ul>
	                  </div>
	                </li>
	              </ul>                 
                </div>
	            <!--/Row-->
	          </div>
	        </div>
          </div>
	      
	      <div class="powerwidget cold-grey" id="serverstatus-index">
	        <header>
	          <h2></h2>
	        </header>
	        <div>
	          <div class="inner-spacer">	          	
            	<div class="row"> 
                  <!--Row-->
                  <div class="col-md-8">
                  	<div class="page-header">
				    	<h2><small></small></h2>
				    </div>
				    
				    <!-- 
				    	
				    	Buat Map 
				    	
			    	-->
				    
				    
                  </div>
                  <div class="col-md-4">
                  	<div class="page-header">
				    	<h2>User<small>Terbaru</small></h2>
				    </div>
                    <div class="table-responsive">
	                  <table class="table table-striped table-hover margin-0px airtable">
	                    <thead>
	                      <tr>
	                  		<th colspan="2">Nama</th>
			                <th>Tgl Gabung</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    	<?php
	                    		$db = Db::init();
								$emails = $db->emails_digiumm;
		                    	$n = 1;
								foreach ($userbaru as $keyuser) {
									$memails = $emails->findOne(array('user_id' => trim($keyuser['_id'])));
									echo '<tr>';
			                        echo '<td><span class="num">'.$n.'</span></td>';
									
									echo '<td><h5>'.$keyuser['name'].' <small class="text-muted">'.$memails['email'].'</small></h5></td>';																
			                        echo '<td><span class="badge">'.date("d F Y", $keyuser['time_created']).'</span></td>';
			                        //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
			                      	echo '</tr>';
									$n++;
								}
	                    	?>	                      
	                    </tbody>
	                    <tfoot>
	                      <tr>
	                        <th colspan="2">Nama</th>
			                <th>Tgl Gabung</th>
	                      </tr>
	                    </tfoot>
	                  </table>
	                </div>
                  </div>                  
                </div>
	            <!--/Row-->
	          </div>
	        </div>
          </div>
	      
	      <div class="powerwidget cold-grey">
	        <header>
	          <!--h2>Statistik Konten Digiumm</h2-->
	        </header>
	        <div>
	          <div class="inner-spacer">	          	
	            <div class="row"> 
	              <!--Row-->
	              <div class="col-md-4">
	              	<div class="page-header">
				    	<h2>Artikel<small>Terbaru</small></h2>
				    </div>
	                <div class="table-responsive">
	                  <table class="table table-striped table-hover margin-0px airtable">
	                    <thead>
	                      <tr>
	                  		<th colspan="2">Judul Artikel</th>
			                <th>Kategori/Rubik</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    	<?php
	                    		$db = Db::init();
	                    		$categbaru = $db->categories;
		                    	$n = 1;
								foreach ($terbaru as $keybaru) {
									echo '<tr>';
			                        echo '<td><span class="num">'.$n.'</span></td>';
									
									$catname = '';
									foreach ($keybaru['category'] as $catbaru) {										
										$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
										$catname = $mcategbaru['name'];
									}
																		
			                        echo '<td><h5>'.$keybaru['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
										
									/*$status = '<span class="label label-danger">Not Publish</span>';
			                        if ($keybaru['publish'] == 'yes')
										$status = '<span class="label label-success">Publish </span><span class="badge">'.date("d F Y", $keybaru['time_created']).'</span>';*/						
			                        echo '<td><span class="badge">'.date("d F Y", $keybaru['time_created']).'</span></td>';
			                        //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
			                      	echo '</tr>';
									$n++;
								}
	                    	?>	                      
	                    </tbody>
	                    <tfoot>
	                      <tr>
	                        <th colspan="2">Judul Artikel</th>
			                <th>Kategori/Rubik</th>
	                      </tr>
	                    </tfoot>
	                  </table>
	                </div>
	              </div>
	              <div class="col-md-4">
	              	<div class="page-header">
				    	<h2>Artikel<small>Terpopuler</small></h2>
				    </div>
	                <div class="table-responsive">
	                  <table class="table table-striped table-hover margin-0px airtable">
	                    <thead>
	                      <tr>
	                  		<th colspan="2">Judul Artikel</th>
			                <th>Viewers</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    	<?php
	                    		$db = Db::init();
		                    	$nn = 1;
								foreach ($terpopuler as $keypop) {
									echo '<tr>';
			                        echo '<td><span class="num">'.$nn.'</span></td>';
																		
									$catname = '';
									foreach ($keypop['category'] as $catbaru) {										
										$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
										$catname = $mcategbaru['name'];
									}
																		
			                        echo '<td><h5>'.$keypop['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
																
			                        echo '<td><span class="badge">'.number_format( $keypop['count'] , 0 , '' , '.' ).'</span></td>';
			                        //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
			                        //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
			                      	echo '</tr>';
									$nn++;
								}
	                    	?>	                      
	                    </tbody>
	                    <tfoot>
	                      <tr>
	                        <th colspan="2">Judul Artikel</th>
			                <th>Viewers</th>
	                      </tr>
	                    </tfoot>
	                  </table>
	                </div>
	              </div>
	              <div class="col-md-4">
	              	<div class="page-header">
				    	<h2>Artikel<small>Terkomentar</small></h2>
				    </div>
	                <div class="table-responsive">
	                  <table class="table table-striped table-hover margin-0px airtable">
	                    <thead>
	                      <tr>
	                  		<th colspan="2">Judul Artikel</th>
			                <th>Jumlah Komentar</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    	<?php
	                    		$db = Db::init();
								$comment = $db->comments;
		                    	$n = 1;
								foreach ($komentar as $keykom) {
									$qcomment = array('content_id' => new MongoId(trim($keykom['_id'])));
									$mcomment = $comment->find($qcomment);
									$countcomment = $comment->count($qcomment);
									if ($countcomment > 0) {
										echo '<tr>';
				                        echo '<td><span class="num">'.$n.'</span></td>';	
										
										$catname = '';
										foreach ($keykom['category'] as $catbaru) {										
											$mcategbaru = $categbaru->findOne(array('_id' => new MongoId($catbaru['category_id'])));
											$catname = $mcategbaru['name'];
										}
																		
				                        echo '<td><h5>'.$keykom['title'].' <small class="text-muted">'.$catname.'</small></h5></td>';
																			
										
				                        echo '<td>'.$countcomment.'</td>';
				                        //echo '<small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>';
				                        //echo '<td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>';
				                        //echo '<td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>';
				                      	echo '</tr>';
										$n++;
									}									
								}
	                    	?>	                      
	                    </tbody>
	                    <tfoot>
	                      <tr>
	                        <th colspan="2">Judul Artikel</th>
			                <th>Jumlah Komentar</th>
	                      </tr>
	                    </tfoot>
	                  </table>
	                </div>
	              </div>
	            </div> 
	          </div>
	        </div>
	        </div>
	        
	        <!-- End .powerwidget -->
	        
	        <!-- New Widget -->
	        
	        
	        
	        <!-- End .powerwidget --> 
	        
	        <!-- New widget ->
	        <div class="powerwidget cold-grey" id="vectormap-index" data-widget-editbutton="false">
	          <header>
	            <h2>Traffic Geo<small>Just Demo</small></h2>
	          </header>
	          <div class="inner-spacer">
	            <div class="row"> 
	              <!--Row->
	              <div class="col-md-6">
	                <div id="vmap" class="jqvmap"></div>
	              </div>
	              <div class="col-md-6">
	                <div class="table-responsive">
	                  <table class="table table-striped table-hover margin-0px airtable">
	                    <thead>
	                      <tr>
	                        <th colspan="2">Country</th>
	                        <th >Source/Data</th>
	                        <th >Part</th>
	                        <th >Dinamic</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                      <tr>
	                        <td><span class="num">1</span></td>
	                        <td><h5>United States</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:20</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">1,3,2,5,3</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>
	                      </tr>
	                      <tr>
	                        <td ><span class="num">2</span></td>
	                        <td><h5>China</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:21</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>82871 <span class="text-green">(99143)</span></strong> <i class="fa fa-caret-up"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">3,3,2,1,5</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">9,3,3,2,4,5,6,2,8,4,9,0</span></td>
	                      </tr>
	                      <tr>
	                        <td ><span class="num">3</span></td>
	                        <td><h5>Germany</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:22</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>589666 <span class="text-red">(542313)</span></strong> <i class="fa fa-caret-down"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">1,3,2,3,1</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,8,4,6,7</span></td>
	                      </tr>
	                      <tr>
	                        <td ><span class="num">4</span></td>
	                        <td><h5>Russian Federation</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:23</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>589788 <span class="text-green">(508113)</span></strong> <i class="fa fa-caret-down"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">4,3,2,2,5</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,9,2,4,6,7</span></td>
	                      </tr>
	                      <tr>
	                        <td ><span class="num">5</span></td>
	                        <td><h5>India</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:24</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>589871 <span class="text-blue">(567113)</span></strong> <i class="fa fa-caret-down"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">3,3,2,5,7</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">5,6,7,9,9,5,3,2,2,4,6,7</span></td>
	                      </tr>
	                      <tr>
	                        <td ><span class="num">6</span></td>
	                        <td><h5>Antarctica</h5>
	                          <small><i class="fa fa-clock-o"></i> Last Update</small> <span class="badge">12:25</span></td>
	                        <td><h5>Google<small class="text-muted">Google Analytics</small></h5>
	                          <small><strong>589871 <span class="text-green">(567113)</span></strong> <i class="fa fa-caret-up"></i></small></td>
	                        <td class="text-center"><span class="table-sparkline-pie2">2,3,2,2,6</span></td>
	                        <td class="text-center"><span class="table-sparkline-lines">5,6,7,2,4,5,1,2,8,4,6,9</span></td>
	                      </tr>
	                    </tbody>
	                    <tfoot>
	                      <tr>
	                        <th colspan="2">Country</th>
	                        <th>Source</th>
	                        <th>Part</th>
	                        <th>Dynamic</th>
	                      </tr>
	                    </tfoot>
	                  </table>
	                </div>
	              </div>
	            </div>
	            <!--/Row-> 
	          </div>
	        </div>
	        <!-- End Widget -> 
	        
	      </div>
	      <!-- /Inner Row Col-md-12 ->
	      
	      <div class="col-md-4 bootstrap-grid"> 
	        <!-- New widget ->
	        <div class="powerwidget powerwidget-as-portlet-green" id="chartjsgraphs-index-1" data-widget-editbutton="false">
	          <header>
	            <h2>Rates</h2>
	          </header>
	          <div class="inner-spacer">
	            <div class="row">
	              <div class="col-md-12 text-center">
	                <canvas id="chartjs-doughnut2" height="170" width="170"></canvas>
	                <h1 class="colossal"><i class="fa fa-dollar"></i>8290<i class="fa fa-caret-up"></i></h1>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="text-white margin-0px margin-top">2584</h3>
	                <span class="label bg-dark-cold-grey"><strong>Excellent</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="text-white margin-0px margin-top">1823</h3>
	                <span class="label bg-white"><strong>Good</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="text-white margin-0px margin-top">1823</h3>
	                <span class="label bg-yellow"><strong>Bad !#%**</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="text-white margin-0px margin-top">2345</h3>
	                <span class="label bg-cold-grey"><strong>Terrible</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	            </div>
	          </div>
	        </div>
	        <!-- /New widget -> 
	      </div>
	      <!-- /Inner Row Col-md-4 ->
	      
	      <div class="col-md-4 bootstrap-grid"> 
	        <!-- New widget ->
	        <div class="powerwidget powerwidget-as-portlet-white" id="chartjsgraphs-index-2" data-widget-editbutton="false">
	          <header>
	            <h2>Auditory</h2>
	          </header>
	          <div class="inner-spacer">
	            <div class="row">
	              <div class="col-md-12 text-center">
	                <canvas id="chartjs-doughnut3" height="170" width="170"></canvas>
	                <h1 class="colossal"><i class="fa fa-female"></i>3258<i class="fa fa-caret-down"></i></h1>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="margin-0px margin-top">1143</h3>
	                <span class="label bg-orange"><strong>Male</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="margin-0px margin-top">5523</h3>
	                <span class="label bg-purple"><strong>Female</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="margin-0px margin-top">8943</h3>
	                <span class="label bg-red"><strong>Aliens</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="text-white margin-0px margin-top">2064</h3>
	                <span class="label bg-pink"><strong>Predators</strong> <i class="fa fa-chevron-circle-up"></i></span> </div>
	            </div>
	          </div>
	        </div>
	        <!-- /New widget -> 
	      </div>
	      <!-- /Inner Row Col-md-4 ->
	      
	      <div class="col-md-4 bootstrap-grid"> 
	        <!-- New widget ->
	        <div class="powerwidget powerwidget-as-portlet-orange" id="chartjsgraphs-index-3" data-widget-editbutton="false">
	          <header>
	            <h2>Sales</h2>
	          </header>
	          <div class="inner-spacer">
	            <div class="row">
	              <div class="col-md-12 text-center">
	                <canvas id="chartjs-doughnut4" height="170" width="170"></canvas>
	                <h1 class="colossal"><i class="fa fa-android"></i>55232<i class="fa fa-caret-down"></i></h1>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="text-white margin-0px margin-top">2584</h3>
	                <span class="label bg-white"><strong>Windows</strong> <i class="fa fa-windows"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center">
	                <h3 class="text-white margin-0px margin-top">1823</h3>
	                <span class="label bg-dark-red"><strong>Android</strong> <i class="fa fa-android"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="text-white margin-0px margin-top">1823</h3>
	                <span class="label bg-blue"><strong>iOS</strong> <i class="fa fa-apple"></i></span> </div>
	              <div class="col-md-6 col-sm-6 col-xs-6 text-center margin-bottom-20px">
	                <h3 class="text-white margin-0px margin-top">2345</h3>
	                <span class="label bg-cold-grey"><strong>Linux</strong> <i class="fa fa-linux"></i></span> </div>
	            </div>
	          </div>
	        </div>
	        <!-- /New widget -> 
	      </div>
	      <!-- /Inner Row Col-md-4 ->
	      
	      <div class="col-md-12 bootstrap-grid"> 
	        
	        <!-- New widget ->
	        <div class="powerwidget blue" id="calendar-widget-index" data-widget-editbutton="false">
	          <header>
	            <h2>Calendar<small>Events</small></h2>
	          </header>
	          <div class="inner-spacer">
	            <div id='calendar2'></div>
	          </div>
	        </div>
	        <!-- End Widget -> 
	        
	        <!-- New widget ->
	        <div class="powerwidget cold-grey" id="chatz-index" data-widget-editbutton="false">
	          <header>
	            <h2>Chat<small>Yeah!</small></h2>
	          </header>
	          <div class="inner-spacer">
	            <div class="chat-container">
	              <div class="top-buttons clearfix">
	                <h2 class="margin-0px pull-left">Chat</h2>
	                <span class="badge">25</span>
	                <div class="btn-group btn-group-sm pull-right"> <a class="btn btn-default"><i class="fa fa-thumbs-down"></i> <span class="hidden-xs">Leave</span></a> <a class="btn btn-default"><i class="fa fa-times-circle"></i> <span class="hidden-xs">Clear</span></a></div>
	              </div>
	              <nav class="chat-users-menu"> 
	                
	                <!--Adding Some Scroll->
	                <div class="nano">
	                  <div class="nano-content">
	                    <div class="menu-header"><a class="btn btn-default chat-toggler"><i class="fa fa-user"></i> <i class="fa fa-arrow-down"></i></a></div>
	                    <ul>
	                      <li><a href="#"><span class="chat-name">Gluck Dorris</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span><span class="badge">5</span></a></li>
	                      <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
	                      <li><a href="#"><span class="chat-name">Mr. Joker</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Chewbacca</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">The Piggy</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
	                      <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                      <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
	                    </ul>
	                  </div>
	                </div>
	              </nav>
	              <div class="chat-container">
	                <div class="chat-pusher">
	                  <div class="chat-content"><!-- this is the wrapper for the content ->
	                    <div class="nano"><!-- this is the nanoscroller ->
	                      <div class="nano-content">
	                        <div class="chat-content-inner"><!-- extra div for emulating position:fixed of the menu --> 
	                          
	                          <!-- Top Navigation ->
	                          
	                          <div class="clearfix">
	                            <div class="chat-messages chat-messages-with-sidebar">
	                              <ul>
	                                <li class="left clearfix">
	                                  <div class="user-img pull-left"> <img src="http://placehold.it/150x150" alt="User Avatar" /> </div>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Gluck Dorris</span><span class="name"></span> <span class="badge"><i class="fa fa-clock-o"></i>14 mins ago</span></div>
	                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porttitor nulla vitae interdum fermentum. Ut in vulputate neque. Praesent luctus lacus a dolor tempus pellentesque. Cras sit amet urna eu augue suscipit eleifend. Mauris mollis pharetra faucibus. Phasellus eu massa quam. Nunc id metus placerat neque feugiat commodo. </p>
	                                  </div>
	                                </li>
	                                <li class="right clearfix"><span class="user-img pull-right"> <img src="http://placehold.it/150x150" alt="User Avatar" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Anton Durant</span><span class=" badge"><i class="fa fa-clock-o"></i>13 mins ago</span> </div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                                <li class="left clearfix"><span class="user-img pull-left"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Spiderman</span> <span class="badge"><i class="fa fa-clock-o"></i>14 mins ago</span> </div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                                <li class="right clearfix"><span class="user-img pull-right"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"><span class="name">Muchu</span><small class="badge"><i class="fa fa-clock-o"></i>15 mins ago</small></div>
	                                    <p>Nunc ipsum dui, tempus id sagittis eu, rutrum ac libero. Morbi non enim a tortor pulvinar feugiat at consectetur nunc. Curabitur pulvinar tincidunt nisi id bibendum. Nulla ut diam iaculis, venenatis velit hendrerit, fringilla arcu. Mauris accumsan pulvinar augue, non blandit justo vestibulum a. Proin non eros semper, accumsan nisl in, imperdiet justo. Pellentesque convallis commodo porttitor. Nam feugiat dignissim felis sed tempor. Sed pretium eros nec mi semper aliquam. Phasellus eget accumsan felis. Nulla varius risus quis dapibus porta. Donec vel magna viverra, semper velit eu, adipiscing arcu. Integer sollicitudin elementum est eget ullamcorper. Mauris eget sollicitudin erat. Nullam et lacinia nibh, a aliquam nunc. Curabitur ullamcorper metus ac purus commodo, sit amet mattis arcu mollis. </p>
	                                  </div>
	                                </li>
	                                <li class="left clearfix"><span class="user-img pull-left"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Gluck Dorris</span> <span class="badge"><i class="fa fa-clock-o"></i>14 mins ago</span></div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                                <li class="right clearfix"><span class="user-img pull-right"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Anton Durant</span><span class=" badge"><i class="fa fa-clock-o"></i>13 mins ago</span> </div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                                <li class="left clearfix"><span class="user-img pull-left"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Spiderman</span> <span class="badge"><i class="fa fa-clock-o"></i>14 mins ago</span> </div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                                <li class="right clearfix"><span class="user-img pull-right"> <img src="http://placehold.it/150x150" alt="User Avatar" class="img-circle" /> </span>
	                                  <div class="chat-body clearfix">
	                                    <div class="header"> <span class="name">Spiderman</span> <span class="badge"><i class="fa fa-clock-o"></i>14 mins ago</span></div>
	                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
	                                      dolor, quis ullamcorper ligula sodales. </p>
	                                  </div>
	                                </li>
	                              </ul>
	                            </div>
	                          </div>
	                        </div>
	                        
	                        <!-- /chat-content-inner -> 
	                      </div>
	                    </div>
	                    
	                    <!-- /nano -> 
	                    
	                  </div>
	                  
	                  <!-- /chat-content -> 
	                </div>
	                <!-- /chat-pusher -> 
	              </div>
	            </div>
	            <!-- /chat-container--> 
	            <!--Chat-form ->
	            <div class="chat-message-form">
	              <div class="row">
	                <div class="col-md-12">
	                  <textarea placeholder="Write Your Message Here" class="form-control margin-bottom" rows="2"></textarea>
	                </div>
	                <div class="col-md-8 col-sm-8 col-xs-8">
	                  <div class="btn-group">
	                    <button class=" btn btn-default"><i class="fa fa-location-arrow"></i></button>
	                    <button class=" btn btn-default"><i class="fa fa-camera"></i></button>
	                    <button class=" btn btn-default"><i class="fa fa-music"></i></button>
	                    <button class=" btn btn-default"><i class="fa fa-file"></i></button>
	                  </div>
	                </div>
	                <div class="col-md-4 col-sm-4 col-xs-4">
	                  <button class="btn btn-info pull-right" type="button">Chat!</button>
	                </div>
	              </div>
	            </div>
	            
	            <!-- /Chat-form -> 
	            
	          </div>
	        </div>
	        <!-- End Widget --> 
	        
	      </div>
	      <!-- /Inner Row Col-md-12 --> 
	    </div>
	    <!-- /Widgets Row End Grid--> 
	  </div>
</div>