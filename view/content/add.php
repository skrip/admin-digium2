<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Content</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Content<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post" enctype='multipart/form-data'>
                  <fieldset>
                  	<section>
                      <label class="label">Content Type</label>
                      <label class="select">
                        <select name="contenttype" id="contenttype">
                        	<?php
                        		$m='<option value="Post">Post</option>';
            					if ($contenttype=="Post")
									$m='<option value="Post" selected="selected">Post</option>';
								$mm .= $m;
								
								$m='<option value="Kutipan">Kutipan</option>';
            					if ($contenttype=="Kutipan")
									$m='<option value="Kutipan" selected="selected">Kutipan</option>';
								$mm .= $m;
								
								$m='<option value="Video">Video</option>';
            					if ($contenttype=="Video")
									$m='<option value="Video" selected="selected">Video</option>';
								$mm .= $m;
								
								$m='<option value="Audio">Audio</option>';
            					if ($contenttype=="Audio")
									$m='<option value="Audio" selected="selected">Audio</option>';
								$mm .= $m;
								
								echo $mm;
                        	?>
                        </select>
                      </label>
                    </section>
                    <div id="kutipan" style="display: none">
                    	<section>
	                      <label class="label">Kutipan (Arabic)</label>
	                      <?php
	                    	if(isset($error['quotearab']))
	                    		echo '<label class="textarea textarea-resizable state-error">';
							else
								echo '<label class="textarea textarea-resizable">';
						  ?>
	                        <textarea rows="3" id="quotearab" name="quotearab"><?php echo $quotearab; ?></textarea>
	                      </label>
	                      <?php
		                    if(isset($error['quotearab']))
		                    {
		                    	foreach($error['quotearab'] as $err)
								{
									echo '<div class="note note-error">'.$err.'</div>';
								}
		                    }
						  ?>
	                    </section>
	                    
	                    <section>
	                      <label class="label">Kutipan (Latin)</label>
	                      <?php
	                    	if(isset($error['quotelatin']))
	                    		echo '<label class="textarea textarea-resizable state-error">';
							else
								echo '<label class="textarea textarea-resizable">';
						  ?>
	                        <textarea rows="3" id="quotelatin" name="quotelatin"><?php echo $quotelatin; ?></textarea>
	                      </label>
	                      <?php
		                    if(isset($error['quotelatin']))
		                    {
		                    	foreach($error['quotelatin'] as $err)
								{
									echo '<div class="note note-error">'.$err.'</div>';
								}
		                    }
						  ?>
	                    </section>
	                    
	                    <section>
	                    	<label class="label">Quote (Nama)</label>
	                      <?php
	                    	if(isset($error['quotename']))
	                    		echo '<label class="input state-error">';
							else
								echo '<label class="input">';
						  ?>
	                        <input type="text" name="quotename" value="<?php echo $quotename; ?>">
	                      </label>
	                      <?php
		                    if(isset($error['quotename']))
		                    {
		                    	foreach($error['quotename'] as $err)
								{
									echo '<div class="note note-error">'.$err.'</div>';
								}
		                    }
						  ?>
	                    </section>
                    	
                    </div>
                    <div id="audiovideo" style="display: none">
                    	<section>
	                    	<label class="label">Video/Audio URL</label>
	                      <?php
	                    	if(isset($error['url']))
	                    		echo '<label class="input state-error">';
							else
								echo '<label class="input">';
						  ?>
	                        <input type="text" name="url" value="<?php echo $url; ?>">
	                      </label>
	                      <?php
		                    if(isset($error['url']))
		                    {
		                    	foreach($error['url'] as $err)
								{
									echo '<div class="note note-error">'.$err.'</div>';
								}
		                    }
						  ?>
	                    </section>
                    </div>
              		<section>
                      <label class="label">Category</label>
                      <label class="select select-multiple">
                        <select name="category[]" multiple="multiple">
                          <?php
                          $db = Db::init();
						  $c = $db->categories;
						  $cat = $c->find();
						  foreach($cat as $cc)
						  {
						  	$ada = false;
							foreach($category as $dcc)
							{
								if(trim($dcc['category_id']) == trim($cc['_id']))
								{
									$ada = true;
									echo '<option value="'.trim($cc['_id']).'" selected>'.$cc['name'].'</option>';
								}									
							}
							if(!$ada)
								echo '<option value="'.trim($cc['_id']).'">'.$cc['name'].'</option>';
						  }
                        ?>
                        </select>
                      </label>
                    </section>
                    
                    <script type="text/javascript">
                    	$(document).ready(function($) {

								$('#tag').tagit({
								   allowSpaces: true,
							       availableTags: [
							       <?php
							       		foreach ($atag as $key) {
											echo '"'. $key['name'] .'",';
										}
							       ?>
							       ]
							    });
							});

                    	
                    </script>
                    
                    <section>
                    	<label class="label">Tag</label>
                      <?php
                    	if(isset($error['tag']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
						
						$vtag = '';				
						if(isset($tag))
						{
							if(count($tag) > 0)
							{								
								$dtg = $db->tags;
								foreach($tag as $ttg)
								{
									$mtg = $dtg->findOne(array('_id' => new MongoId($ttg['tag_id'])));
									$vtag .= $mtg['name'].', ';
								}
							}
						}
					  ?>					  
                        <input type="text" name="tag" id="tag" value="<?php echo $vtag; ?>">
                      </label>
                      <?php
	                    if(isset($error['tag']))
	                    {
	                    	foreach($error['tag'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	<label class="label">Title</label>
                      <?php
                    	if(isset($error['title']))
                    		echo '<label class="input state-error">';
						else
							echo '<label class="input">';
					  ?>
                        <input type="text" name="title" value="<?php echo $title; ?>">
                      </label>
                      <?php
	                    if(isset($error['title']))
	                    {
	                    	foreach($error['title'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                    	<label class="label">Tanggal Kegiatan</label>
						<label class="input"> <i class="icon-append fa fa-calendar"></i>
                    		<input type="text" name="event_date" id="date" value="<?php echo $event_date; ?>">
                       	</label>
                    </section>
                    
                    
                    
                    <section>
                      <label class="label">Short description</label>
                      <?php
                    	if(isset($error['description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                      <textarea rows="3" name="short_description" class="editor"><?php echo $short_description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['short_description']))
	                    {
	                    	foreach($error['short_description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
                      <label class="label">Description</label>
                      <?php
                    	if(isset($error['description']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" id="description" name="description" class="editor"><?php echo $description; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['description']))
	                    {
	                    	foreach($error['description'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
	                    <label class="label">Publish</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
	                    	<?php
	                    		if(!empty($publish))
								{
									if(trim($publish) == 'yes')
										echo '<input type="checkbox" name="publish" checked>';
									else
										echo '<input type="checkbox" name="publish" >';
								}
								else
									echo '<input type="checkbox" name="publish" checked>';
	                    	?>
	                    	<i></i>Publish</label>		                    			                    
	                    </div>
                    </section>
                    
                    <section>
	                    <label class="label">Slide Show</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
	                    	<?php
	                    		if(!empty($slide))
								{
									if(trim($slide) == 'yes')
										echo '<input type="checkbox" name="slide" checked>';
									else
										echo '<input type="checkbox" name="slide" >';
								}
								else
									echo '<input type="checkbox" name="slide">';
	                    	?>
	                    	<i></i>Yes</label>		                    			                    
	                    </div>
                    </section>  
                    
                    <section>
                      <label class="label">File Image</label>
                      <?php
	                    if(strlen(trim($fileimage)) > 0)
						{
				      ?>
                      <div class="col-md-3 col-sm-6 thumbnail">
                      	<?php
	                        $path_parts = pathinfo($fileimage);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".f300x300.".$ext;
							$url2 = $f.".f450x450.".$ext;
                        ?>
                        <div class='hover-fader'><a href="#image-1"><img class="img-rounded img-responsive" src="<?php echo IMAGE_URL.'image_content/'.$url; ?>" alt="image01"><span class='zoom'><i class='fa fa-search-plus'></i></span></a></div>                        
                        <div class="gal-overlay" id="image-1"><img src="<?php echo IMAGE_URL.'image_content/'.$url2; ?>" alt="image01" />
                          <a href="#die" class="gal-close"><i class="fa fa-times-circle"></i></a> </div>
                      </div>
                      <?php } ?>
                      
                      <div class="col-md-8">
                        <input id="filebutton" name="fileimage" class="input-file" type="file">
                      </div>
                      <!--div class="note note-error">File size must be less than 3Mb.</div-->
                    </section>					
                      
                  </fieldset>
	
				  	
                  <footer>					  
                  <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 
