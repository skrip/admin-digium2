<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Content</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <div class="big-icons-buttons pull-right" ><a href="/content/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>	
          <h1>Content<small> list Content</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="6">Content
                        <form action="/content/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:10px 3px;">Search</button>
                      	</form>
                      </th>
                    </tr>
                    <tr>
                      <th></th>
                      <th></th>
                      <th>Tipe</th>
                      <th>Judul</th>
                      <th>Terpilih</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$no = $idx;
					$db = Db::init();
					$usr = $db->users;
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						
						$created_by = 'admin';
						if(isset($dt['created_by']))
						{
							$mu = $usr->findone(array('_id' => new MongoId($dt['created_by'])));
							$created_by = $mu['name'];
						}
						if(!empty($dt['image']))
						{
							
							$path_parts = pathinfo($dt['image']);
							$f = $path_parts['filename'];
							$ext = $path_parts['extension'];
							$url = $f.".f50x50.".$ext;
							echo '<td width="7%"><img src="'.IMAGE_URL.'image_content/'.$url.'"></td>';
						}
						else 
						{
							echo '<td width="7%"></td>';
						}
						
						$contenttype = 'Post';
						if(isset($dt['contenttype']))
							$contenttype = $dt['contenttype'];
						echo '<td><h5>'.$contenttype.'</h5></td>';
                      	echo '<td><h5>'.$dt['title'].' ( '.$created_by.' - '.date('d-m-Y H:i:s )', $dt['time_created']).'</h5>';
						echo '<small>'.helper::limitString($dt['description'], 200).'</small></td>';
						if (isset($dt['terpilih']) && $dt['terpilih']=='yes')
                      		echo '<td><input class="content-checkbox" data-val="'.trim($dt['_id']).'" type="checkbox" checked="checked" /></td>';
						else
							echo '<td><input class="content-checkbox" data-val="'.trim($dt['_id']).'" type="checkbox" /></td>';
                        echo '<td class="text-center" style="width:30px">';
                      	echo '<a href="/content/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/content/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="5">Content</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Konten"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 
      
<script type="text/javascript">
	$(function(){
		$('.content-checkbox').bind('change', function() {
		var id = $(this).attr('data-val');
        if ($(this).prop('checked')) {
        	$.get('/content/terpilih?set=yes&id='+id, function(data){
	        		
	        });
        }
        else 
        {
             $.get('/content/terpilih?set=no&id='+id, function(data){
	        		
	        });
        }
    });
	})
</script>