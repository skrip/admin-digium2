<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/forum/index">Forum thread</a></li>
            <li class="active">Forum thread</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
        	<div class="big-icons-buttons pull-right" ><a href="/forum/add" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add</a></div>
          <h1>Forum thread<small> list Forum thread</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="powerwidget powerwidget-as-portlet-white" id="tablestyled" data-widget-editbutton="false">
              <div class="inner-spacer">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0">
                  <thead>
                    <tr>
                      <th width="100%" colspan="3">Forum thread
						<form action="/forum/index" name="form-search" method="post" class="orb-form" style="float: right">
                      		<input type="text" name="search" id="search" value="<?php echo $search; ?>" style="color:#000000">
                      		<button type="submit" class="btn btn-default" style="padding:3px 10px;">Search</button>
                      	</form>
					  </th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$katserver = new kategoriserver();
					$db = Db::init();
					$usr = $db->users;
		
                  	$no = $idx;					
                  	foreach($data as $dt)
					{
						echo '<tr>';
                      	echo '<td width="1%"><span class="num">'.$no.'</span></td>';
						
						$ppname = "";
						$parent = $dt['forumcategory'];
						$par = $katserver->getAllParent($parent);
						foreach($par as $pp)
						{
							$ppname = "/" . $ppname;
							$ppname = $pp['name'] . $ppname;
						}
						$u = $usr->findone(array('_id' => new MongoId($dt['created_by'])));
						$ppname .= ', '.date('d-m-Y H:i:s', $dt['time_created']).', '.$u['email'];
						
                      	echo '<td><h5>'.$dt['title'].'</h5>';
                      		
                        echo '<small>'.$ppname.'</small></td>';
                      	echo '<td class="text-center">';
                      	echo '<a href="/forum/edit?id='.trim($dt['_id']).'" title="edit"><i class="fa fa-edit"></i></a>';
						echo '&nbsp;&nbsp;<a href="#" ref="/forum/delete?id='.trim($dt['_id']).'" class="mmmodal" title="hapus"><i class="fa fa-trash-o"></i></a>';
                    	echo '</td></tr>';
						$no++;
					}
                  	?>                  	
                  </tbody>
                  <tfoot>
                    <tr>
                      <th width="90%" colspan="2">Category</th>
                      <th width="10%"></th>
                    </tr>
                  </tfoot>
                </table>
                <?php echo $pagination;?>
              </div>
            </div>
            <?php echo Modal::Show("Delete Forum thread"); ?>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 