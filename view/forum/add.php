<div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/forum/index">Forum threads</a></li>
            <li class="active">Forum threads</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Forum threads<small> Forms</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          
          <!-- New widget -->
          <div class="col-md-12  bootstrap-grid">
            <div class="powerwidget green" id="most-form-elements" data-widget-editbutton="false">
              <header>
                <h2>Form Elements<small>Normal State</small></h2>
              </header>
              <div class="inner-spacer">
              	<?php
              	if(isset($info))
				{
					foreach($info as $inf)
					{
              			echo '<div class="callout callout-info">';
                  		echo '<h4>'.$inf['title'].'</h4>';
                  		echo '<p>'.$inf['info'].'</p>';
                		echo '</div>';
					}
                }
                ?>
                
                <form action="<?php echo $link; ?>" class="orb-form" method="post">
                  <fieldset>
                  	<section>
		                <label class="label">Forum Category</label>
		                <label class="select">
		                    <select name="forumcategory">
		                      	<?php
		                      		$katserver = new kategoriserver();	
									$d = $katserver->getAll(0, 0);								

									echo '<option value="0">-- Pilih Kategori --</option>';
									foreach($d as $dd)
									{
										$space = "";
										for($i=0;$i<$dd['level']-1;$i++)
											$space .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										if($parent == $dd['id'])
											echo '<option value="'.trim($dd['id']).'" selected>'.$space.$dd['name'].'</option>';
										else
											echo '<option value="'.trim($dd['id']).'" >'.$space.$dd['name'].'</option>';
									}
		                      	?>
		                    </select>
		                <i></i> </label>
                    </section>
                    
                    <section>
                      <label class="label">threads</label>
                      <?php
                    	if(isset($error['title']))
                    		echo '<label class="textarea textarea-resizable state-error">';
						else
							echo '<label class="textarea textarea-resizable">';
					  ?>
                        <textarea rows="3" name="title"><?php echo $title; ?></textarea>
                      </label>
                      <?php
	                    if(isset($error['title']))
	                    {
	                    	foreach($error['title'] as $err)
							{
								echo '<div class="note note-error">'.$err.'</div>';
							}
	                    }
					  ?>
                    </section>
                    
                    <section>
	                    <label class="label">Status</label>
	                    <div class="inline-group">
		                    <label class="checkbox">
		                    	<?php
		                    		if(!empty($status))
									{
										if(trim($status) == 'active')
											echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
										else
											echo '<input type="checkbox" name="status" > <i></i>Active</label>';
									}
									else
										echo '<input type="checkbox" name="status" checked> <i></i>Active</label>';
		                    	?>		                    			                    
	                    </div>
                    </section>
                    
                  </fieldset>
	
				  	
                  <footer>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </footer>
                </form>
              </div>
            </div>
          </div>
          
          <!-- End .powerwidget -->
         
          
        </div>
        <!-- /Inner Row Col-md-12 --> 
      </div>
      <!-- /Widgets Row End Grid--> 