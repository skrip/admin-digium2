<!--Content Wrapper-->
      <div class="content-wrapper"> 
        <!--Horisontal Dropdown-->
        <?php echo $shorcut; ?>
        
        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li><a href="/forum/index">Forum</a></li>
            <li class="active">Forum Diskusi</li>
          </ul>
        </div>
        <!--/Breadcrumb-->
        
        <div class="page-header">
          <h1>Forum<small>Forum</small></h1>
        </div>
        
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          <div class="col-md-12 bootstrap-grid"> 
            
            <!-- New widget -->
            <div class="powerwidget dark-blue" id="forum" data-widget-editbutton="false">
              <header>
                <h2>Forum<small>Forum</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="forum">
                  <?php
                  	$katserver = new kategoriserver();	
					$d = $katserver->getAll(0, 0);								

					foreach($d as $dd)
					{
						for($i=0;$i<$dd['level']-1;$i++)
						{
							if($dd['level'] == 2)	
							{
								echo '<div class="header">'.$dd['name'].'</div>';
								echo '<ul>';
								$pr = $katserver->getAll(0,0);
								//print_r($pr); die;
								foreach($pr as $dpr)
								{
									if(($dpr['level'] == 3) && ($dpr['parent'] == $dd['id']))	
									{
										echo '<li>';
				                      	echo '<div class="col-lg-8 col-md-6 col-sm-8">';
				                        echo '<div class="main-details"> <i class="fa fa-comments"></i>';
				                        echo '<h3><a href="/forum/detail?forumcategory='.$dpr['id'].'">'.$dpr['name'].'</a></h3>';
				                        echo '<span class="description">'.$dpr['description'].'</span> </div>';
				                      	echo '</div>';
				                      	echo '<div class="col-lg-2 col-md-3 visible-lg visible-md">';
				                        echo '<div class="nums-container">';
				                        echo '<ul class="nums">';
										
										$quer = array('forumcategory' => $dpr['id']);
										$db = Db::init();
										$usr = $db->users;
										
										$forum = $db->forum_threads;
										$count = $forum->count($quer);
										$satu = $forum->findone($quer);
				                        echo '<li><strong>'.$count.'</strong> Topics</li>';
				                        echo '<li><strong>0</strong> Replies</li>';
				                        echo '</ul>';
				                        echo '</div>';
				                      	echo '</div>';
				                      	$isi = '';
										$sejak = '';
										$oleh = '';
										if($count > 0)
										{
											$isi = $satu['title'];
											$sejak = helper::time_elapsed_string($satu['time_created']);
											$uusr = $usr->findone(array('_id' => new MongoId($satu['created_by'])));
											$oleh = $uusr['name'];
										}
				                      	echo '<div class="col-lg-2 col-md-3 col-sm-4 hidden-xs"><span class="last-post"><a href="#">'.helper::limitString($isi, 40).'</a></span><span class="author">by <a href="#">'.$oleh.'</a></span> <span class="badge">'.$sejak.'</span></div>';
				                      	echo '<div class="clearfix"></div>';
				                    	echo '</li>';
									}
								}
								echo '</ul>';
							}
						}
					}
                  ?>
              </div>
            </div>
            <!--/Forum Block--> 
            
          </div>
        </div>
        <!-- End Widget --> 
        
      </div>
      <!-- /Inner Row Col-md-12 --> 
    </div>
    <!-- /Widgets Row End Grid--> 
  </div>
  <!-- / Content Wrapper --> 